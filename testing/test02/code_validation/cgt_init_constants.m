%-----------------
% define constants
%-----------------
  growth_max_dia  = 1.3     ; % maximum growth rate of diatoms [1/day]
  no3_min_dia     = 1.0E-6  ; % diatoms half-saturation constant for DIN [mol/kg]
  po4_min_dia     = 6.25E-8 ; % diatoms half-saturation constant for DIP [mol/kg]
  light_min_dia   = 35.0    ; % minimum light for diatoms growth [W/m**2]
  resp_rate_dia   = 0.05    ; % respiration rate of diatoms [1/day]
  mort_rate_dia   = 0.02    ; % mortality rate of diatoms [1/day]
  piston_vel_oxy  = 5.0     ; % piston velocity for oxygen surface flux[m/d]
