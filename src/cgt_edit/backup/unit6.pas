unit unit6;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, erg_code, Buttons;

type

  { TForm6 }

  TForm6 = class(TForm)
    BitBtn3: TBitBtn;
    Button1: TButton;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation

{$R *.lfm}

procedure TForm6.FormShow(Sender: TObject);
begin
  Label1.Caption:='CGT Editor '+erg_code.codegenVersion;
end;

procedure TForm6.Button1Click(Sender: TObject);
begin
  ShowMessage('CGT_EDIT - Advanced editor for formal descriptions of ecosystem models.'+chr(13)+
              'Copyright (C) 2013 Hagen Radtke (hagen.radtke@io-warnemuende.de)'+chr(13)+chr(13)+
              'This program was developed at Leibniz Institute for Baltic Sea Research Warnemuende.'+chr(13)+
              'Source code can be downloaded from http://www.ergom.net'+chr(13)+chr(13)+
              'This program is free software: you can redistribute it and/or modify'+chr(13)+
              'it under the terms of the GNU General Public License as published by'+chr(13)+
              'the Free Software Foundation, either version 3 of the License, or'+chr(13)+
              '(at your option) any later version.'+chr(13)+chr(13)+
              'This program is distributed in the hope that it will be useful,'+chr(13)+
              'but WITHOUT ANY WARRANTY; without even the implied warranty of'+chr(13)+
              'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the'+chr(13)+
              'GNU General Public License for more details.'+chr(13)+chr(13)+
              'You should have received a copy of the GNU General Public License'+chr(13)+
              'along with this program.  If not, see <http://www.gnu.org/licenses/>.');
end;

procedure TForm6.BitBtn3Click(Sender: TObject);
begin
  Form6.ModalResult:=4;
end;

procedure TForm6.BitBtn1Click(Sender: TObject);
begin
  Form6.ModalResult:=1;
end;

procedure TForm6.BitBtn2Click(Sender: TObject);
begin
  Form6.ModalResult:=3;
end;

end.
