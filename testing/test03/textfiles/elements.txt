! BioCGT elements file
! ********************
! properties of elements:
!   name=           internal name used, e.g. "N"
!   description=    e.g. "nitrogen"
!   comment=        any comments
! *************************************************************************************
name        = N
description = nitrogen
***********************
name        = P
description = phosphorus
***********************
name        = O
description = oxygen
***********************
name        = C
description = carbon
***********************
name        = H
description = hydrogen
***********************
name        = ch
description = electrical charge
***********************
