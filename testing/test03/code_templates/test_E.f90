
! Test rateCondition
<tracers>
  <name>
  <timetendencies vertloc=WAT OR vertloc=SUR>
    <timeTendency> ! <description>
  </timetendencies>

</tracers>

! Test ctCondition
<celements>
  <containingtracers vertLoc=SED OR vertLoc=SUR>
    <ctName> ! <ctStandardname>
  </containingtracers>
</celements>

! ~~~~~~~~~~~~~~~~~ Test constants ~~~~~~~~~~~~~~~~~
  <numConstants> ! < numConstants >
  <numConstants name=NOR> ! < numConstants name=NOR >
  <numConstants name=NOR OR name=resp_rate_dia OR name=growth_max_dia> ! < numConstants name=NOR OR name=resp_rate_dia OR name=growth_max_dia >
  <numConstants+2 name=NOR OR name=resp_rate_dia> ! < numConstants+2 name=NOR OR name=resp_rate_dia >
<constants name=NOR OR name=resp_rate_dia OR name=growth_max_dia>
  <name> = <value> ! <description>
</constants>

! ~~~~~~~~~~~~~~~~~ Test elements ~~~~~~~~~~~~~~~~~
  <numElements> ! < numElements > 
  <numElements hascoloredcopies=1> ! < numElements hascoloredcopies=1 >
  <numElements hascoloredcopies=1 OR element=p> ! < numElements hascoloredcopies=1 OR element=p > 
  <numElements+2 hascoloredcopies=1 OR element=p> ! < numElements+2 hascoloredcopies=1 OR element=p >

<tracers childof=none>
  ! <name>
  !  all elements
  <elements>
    <element>
  </elements>
  !  only P and elements with colored copies
  <elements hascoloredcopies=1 OR element=p>
    <element>, <name>, <elementDescription>, <description>
  </elements>
  
</tracers>

! ~~~~~~~~~~~~~~~~~ Test tracers ~~~~~~~~~~~~~~~~~
!  ~~~ TEST 1: numTracers ~~~
  <numTracers> ! < numTracers >
  <numTracers+1> ! < numTracers+1 >
  <numTracers-1> ! < numTracers-1 >
  <numTracers vertLoc=SED OR vertLoc=SUR> ! < numTracers vertLoc=SED OR vertLoc=SUR >
  <numTracers vertLoc=SED;childOf=none OR vertLoc=SUR;childOf=none> ! < numTracers vertLoc=SED;childOf=none OR vertLoc=SUR;childOf=none >

  <numFlatTracers> ! < numFlatTracers >
  <num3DTracers> ! < num3DTracers >
  <numWatTracers> ! < numWatTracers >
  <numWatTracers childOf=none> ! < numWatTracers childOf=none >
  <numSedTracers> ! < numSedTracers >
  <numTracers vertLoc=SED> ! < numTracers vertLoc=SED >
  <numSurTracers> ! < numSurTracers >
  <numMovingTracers> ! < numMovingTracers >
  <numRiverDepTracers> ! < numRiverDepTracers >

  <numFlatTracers+2> ! < numFlatTracers+2 >
  <numFlatTracers childOf=none> ! < numFlatTracers childOf=none >
  <numFlatTracers vertLoc=SED;childOf=none OR vertLoc=SUR;childOf=none> ! < numFlatTracers vertLoc=SED;childOf=none OR vertLoc=SUR;childOf=none >
  <numTracers> + <numTracers+2> + <numFlatTracers childOf=none> + <numSedTracers+4> ! several <numXXXTracers ...> in a row

! ~~~ TEST 2: tracers loop
<tracers vertLoc=SED;childOf=none OR vertLoc=SUR;childOf=none>
  <name>
</tracers>

! ~~~ TEST 3a: numTracer and idxTracer, general ~~~
<tracers>
  <name> = <idxTracer>  ! = < numTracer >
                  = <idxFlat>  ! idxFlat
                  = <numFlat>  ! numFlat
                  = <numWat>  ! numWat
                  = <numTracer+2>  ! numTracer+2
                  = <idxFlat childof=none>  ! idxFlat childOf=none
</tracers>

! ~~~ TEST 3b: numTracer and idxTracer, conditions in tracers-loop ~~~
<tracers vertLoc=SED>
 <name>:
  <idxTracer> ! idxTracer
  <idxTracer vertLoc=SED> ! idxTracer vertLoc=SED
  <idxTracer loopConditions> ! idxTracer loopConditions
  <idxSed> ! idxSed
  <idxTracer loopConditions;childOf=none> ! idxTracer loopConditions;childOf=none
  <idxTracer loopConditions;childOf/=none> ! idxTracer loopConditions;childOf/=none
  <idxTracer+1 loopConditions> ! idxTracer+1 loopConditions
  <idxTracer loopCondition OR childOf=none> ! idxTracer loopCondition OR childOf=none

</tracers>

! ~~~ TEST 3c: detailed test of 'loopConditions' ~~~
<tracers vertLoc=SED OR vertLoc=SUR>
 <name>:
  <idxTracer loopConditions> ! idxTracer loopConditions
  <idxSed loopConditions>  ! idxSed loopConditions => only SED
  <idxWat loopConditions>  ! idxWat loopConditions => nothing
  <idxTracer childOf=none OR color=grey> ! idxTracer childOf=none OR color=grey
  <idxTracer childOf=none;loopConditions OR color=grey;LoopConditions> ! idxTracer childOf=none;loopConditions OR color=grey;loopConditions
  <idxTracer childOf=none OR color=grey;loopConditions> ! idxTracer childOf=none OR color=grey;loopConditions

</tracers


! ~~~~~~~~~~~~~~~~~ test auxiliaries ~~~~~~~~~~~~~~~~~
  <numAuxiliaries> ! < numAuxiliaries >
  <numAuxiliaries vertLoc=SED> ! < numAuxiliaries vertLoc=SED >
  <numAuxiliaries vertLoc=SED or name=oxysat> ! < numAuxiliaries vertLoc=SED or name=oxysat >
  <numAuxiliaries+2 vertLoc=SED or name=oxysat> ! < numAuxiliaries+2 vertLoc=SED or name=oxysat >
  
<auxiliaries vertLoc=SED or name=oxysat>
  <name> = <formula> ! blub
</auxiliaries>

! ~~~~~~~~~~~~~~~~~ test processes ~~~~~~~~~~~~~~~~~
  <numProcesses> ! < numProcesses >
  <numProcesses vertLoc=SED> ! < numProcesses vertLoc=SED >
  <numProcesses vertLoc=SED OR vertLoc=SUR> ! < numProcesses vertLoc=SED OR vertLoc=SUR >
  <numProcesses+2 vertLoc=SED OR vertLoc=SUR> ! < numProcesses+2 vertLoc=SED OR vertLoc=SUR >

<processes vertLoc=SED OR vertLoc=SUR>
  <name> = <turnover> ! <description>
</processes>

! ~~~~~~~~~~~~~~~~~ test celements ~~~~~~~~~~~~~~~~~
<celements color=darkgreen OR element=O OR color=pink>
  <element>, <color> ! <description>
</celements>
