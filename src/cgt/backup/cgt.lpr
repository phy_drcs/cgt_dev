program cgt;

{$MODE Delphi}

uses
  Forms, Interfaces, Dialogs, Classes, SysUtils,
  Unit1 in 'Unit1.pas' {Form1},
  erg_base in 'erg_base.pas',
  erg_code in 'erg_code.pas',
  erg_color in 'erg_color.pas',
  erg_types in 'erg_types.pas',
  erg_vector in 'erg_vector.pas',
  fortwrap in 'fortwrap.pas',
  Unix2Dos in 'Unix2Dos.pas',
  parser in 'parser.pas';


{$R *.res}

var
  i: Integer;
  requestedHelp: Boolean;
  sl: TStringList;
  tpath, opath, errstring: String;
begin
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  if Application.ParamCount > 0 then
  begin
    // if "-h" or "--help" is given, write usage text
    requestedHelp:=false;
    for i:=0 to Application.ParamCount-1 do
      if (lowercase(Application.Params[i+1])='-h') or (lowercase(Application.Params[i+1])='--help') then
        requestedHelp:=true;
    if requestedHelp then
    begin
      showMessage('Usage: cgt    (running without parameters will start up graphical user interface)'+chr(13)+
                  'Usage: cgt [-t /path/to/template/dir] [-o /path/to/output/dir] '+chr(13)+
                  '           /base/model/modelinfos.txt [/first/add/on/modelinfos.txt [/second/add/on/modelinfos.txt [...]]]'+chr(13)+
                  'Options:'+chr(13)+
                  '  -t     Path to code template directory (override default in modelinfos.txt)'+chr(13)+
                  '  -o     Path to output directory        (override default in modelinfos.txt)'+chr(13)+
                  'If more than one input file "modelinfos.txt" is given, the later ones are loaded as add-on.');
    end
    else
    begin
      if application.ParamCount>=1 then
      begin
        // initialize parameters
        i:=1;
        sl:=TStringList.create;
        tpath:='';
        opath:='';
        errstring:='';
        // loop over all parameters
        while i<=Application.ParamCount do
        begin
          if (lowercase(Application.Params[i])='-t') then
          begin
            if (i+1 <= Application.ParamCount) then
            begin
              tpath:=Application.Params[i+1]; i:=i+1;
            end
            else
              errstring:=errstring+'The command line parameter -t must be followed by a code template path.'+chr(13);
          end
          else if (lowercase(Application.Params[i])='-o') then
          begin
            if (i+1 <= Application.ParamCount) then
            begin
              opath:=Application.Params[i+1]; i:=i+1;
            end
            else
              errstring:=errstring+'The command line parameter -o must be followed by an output path.'+chr(13);
          end
          else
            sl.Add(ExtractFilePath(Application.Params[i]));
          i:=i+1;
        end;
        if sl.Count=0 then errstring:=errstring+'No file modelinfos.txt was given on the command line.';
        if errstring<>'' then
        begin
          showMessage(errstring);
          halt;
        end
        else
        begin
          // run cgt in command line mode and then exit
          modelinfos:=TErgModelinfo.Create();
          CallLoadModelInfos(sl);
          CallGenerateIndexes(modelinfos.autoSortMoving, modelinfos.autoMassClassProp);
          if tpath<>'' then modelinfos.templatePath:=tpath;
          if (copy(modelinfos.templatePath,length(modelinfos.templatePath),1)<>'/') and
             (copy(modelinfos.templatePath,length(modelinfos.templatePath),1)<>'/') then
            modelinfos.templatePath:=modelinfos.templatePath+'/';
          if opath<>'' then modelinfos.outputPath:=opath;
          if (copy(modelinfos.outputPath,length(modelinfos.outputPath),1)<>'/') and
             (copy(modelinfos.outputPath,length(modelinfos.outputPath),1)<>'/') then
            modelinfos.outputPath:=modelinfos.outputPath+'/';
          CallGenerateCode(modelinfos.autoLimitProcesses, modelinfos.debugMode,
                           modelinfos.autoSplitColors, modelinfos.autoSortMoving,
                           modelinfos.templatePath, modelinfos.outputPath,
                           modelinfos.RealSuffixF90, modelinfos.autoWrapF,
                           modelinfos.autoWrapF90, modelinfos.autoUnixOutput);
          halt;
        end;
      end;
    end;
  end
  else
    Application.Run;
end.
