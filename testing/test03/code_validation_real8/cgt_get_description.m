function [ description ] = cgt_get_description( name )
%CGT_GET_DESCRIPTION get the description for a cgt output variable name
  
  description = '';
    if strcmpi(strtrim(name),strtrim('no3            '))
        description = 'nitrate [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('po4            '))
        description = 'phosphate [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('o2             '))
        description = 'oxygen [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('dia            '))
        description = 'diatoms [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('det            '))
        description = 'detritus [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('no3_with_green_O'))
        description = 'nitrate; containing  [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('po4_with_green_O'))
        description = 'phosphate; containing  [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('o2_with_green_O'))
        description = 'oxygen; containing  [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('dia_with_green_O'))
        description = 'diatoms; containing  [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('det_with_green_O'))
        description = 'detritus; containing  [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('no3_with_green_N'))
        description = 'nitrate; containing nitrogen bound by diatomes [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('dia_with_green_N'))
        description = 'diatoms; containing nitrogen bound by diatomes [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('det_with_green_N'))
        description = 'detritus; containing nitrogen bound by diatomes [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('no3_with_red_N '))
        description = 'nitrate; containing nitrogen formerly bound by diatoms [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('dia_with_red_N '))
        description = 'diatoms; containing nitrogen formerly bound by diatoms [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('det_with_red_N '))
        description = 'detritus; containing nitrogen formerly bound by diatoms [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('no3_with_darkgreen_N'))
        description = 'nitrate; containing nitrogen bound by diatoms a second time [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('dia_with_darkgreen_N'))
        description = 'diatoms; containing nitrogen bound by diatoms a second time [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('det_with_darkgreen_N'))
        description = 'detritus; containing nitrogen bound by diatoms a second time [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('no3_with_pink_N'))
        description = 'nitrate; containing nitrogen formerly bound by diatoms a second time [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('dia_with_pink_N'))
        description = 'diatoms; containing nitrogen formerly bound by diatoms a second time [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('det_with_pink_N'))
        description = 'detritus; containing nitrogen formerly bound by diatoms a second time [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('no3_with_blue_N'))
        description = 'nitrate; containing nitrogen bound by diatoms a third or more times [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('dia_with_blue_N'))
        description = 'diatoms; containing nitrogen bound by diatoms a third or more times [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('det_with_blue_N'))
        description = 'detritus; containing nitrogen bound by diatoms a third or more times [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('no3_with_grey_N'))
        description = 'nitrate; containing  [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('dia_with_grey_N'))
        description = 'diatoms; containing  [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('det_with_grey_N'))
        description = 'detritus; containing  [mol/kg]';
    end
    if strcmpi(strtrim(name),strtrim('nitr           '))
        description = 'nitrogen in sediment [mol/m2]';
    end
    if strcmpi(strtrim(name),strtrim('nitr_with_green_N'))
        description = 'nitrogen in sediment; containing nitrogen bound by diatomes [mol/m2]';
    end
    if strcmpi(strtrim(name),strtrim('nitr_with_red_N'))
        description = 'nitrogen in sediment; containing nitrogen formerly bound by diatoms [mol/m2]';
    end
    if strcmpi(strtrim(name),strtrim('nitr_with_darkgreen_N'))
        description = 'nitrogen in sediment; containing nitrogen bound by diatoms a second time [mol/m2]';
    end
    if strcmpi(strtrim(name),strtrim('nitr_with_pink_N'))
        description = 'nitrogen in sediment; containing nitrogen formerly bound by diatoms a second time [mol/m2]';
    end
    if strcmpi(strtrim(name),strtrim('nitr_with_blue_N'))
        description = 'nitrogen in sediment; containing nitrogen bound by diatoms a third or more times [mol/m2]';
    end
    if strcmpi(strtrim(name),strtrim('nitr_with_grey_N'))
        description = 'nitrogen in sediment; containing  [mol/m2]';
    end
end

