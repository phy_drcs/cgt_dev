#!/bin/bash

# name='erg_code'

for name in 'erg_code' 'Unit1' 'erg_color' 'erg_pseudo3d' 'erg_base'; do
  pas2dox ../src/cgt/${name}.pas > ${name}.cpp
done
