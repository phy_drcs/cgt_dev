unit Unit7;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type

  { TForm7 }

  TForm7 = class(TForm)
    Label2: TLabel;
    ListBox1: TListBox;
    Label3: TLabel;
    Label4: TLabel;
    ListBox2: TListBox;
    ListBox3: TListBox;
    ListBox4: TListBox;
    ListBox5: TListBox;
    ListBox6: TListBox;
    ListBox7: TListBox;
    ListBox8: TListBox;
    Label5: TLabel;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox2DblClick(Sender: TObject);
    procedure ListBox3DblClick(Sender: TObject);
    procedure ListBox4DblClick(Sender: TObject);
    procedure ListBox5DblClick(Sender: TObject);
    procedure ListBox6DblClick(Sender: TObject);
    procedure ListBox7DblClick(Sender: TObject);
    procedure ListBox8DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

uses Unit1;

{$R *.lfm}

procedure TForm7.FormShow(Sender: TObject);
begin
  ListBox1.Color:=Form1.RadioButton1.Color; ListBox2.Color:=Form1.RadioButton1.Color;
  ListBox3.Color:=Form1.RadioButton2.Color; ListBox4.Color:=Form1.RadioButton2.Color;
  ListBox5.Color:=Form1.RadioButton3.Color; ListBox6.Color:=Form1.RadioButton3.Color;
  ListBox7.Color:=Form1.RadioButton4.Color; ListBox8.Color:=Form1.RadioButton4.Color;
end;

procedure TForm7.BitBtn1Click(Sender: TObject);
begin
  Form7.Close;
end;

procedure TForm7.ListBox1DblClick(Sender: TObject);
begin
  GoToExpression(ListBox1.Items[ListBox1.ItemIndex]);
  Form7.Close;
end;

procedure TForm7.ListBox2DblClick(Sender: TObject);
begin
  GoToExpression(ListBox2.Items[ListBox2.ItemIndex]);
  Form7.Close;
end;

procedure TForm7.ListBox3DblClick(Sender: TObject);
begin
  GoToExpression(ListBox3.Items[ListBox3.ItemIndex]);
  Form7.Close;
end;

procedure TForm7.ListBox4DblClick(Sender: TObject);
begin
  GoToExpression(ListBox4.Items[ListBox4.ItemIndex]);
  Form7.Close;
end;

procedure TForm7.ListBox5DblClick(Sender: TObject);
begin
  GoToExpression(ListBox5.Items[ListBox5.ItemIndex]);
  Form7.Close;
end;

procedure TForm7.ListBox6DblClick(Sender: TObject);
begin
  GoToExpression(ListBox6.Items[ListBox6.ItemIndex]);
  Form7.Close;
end;

procedure TForm7.ListBox7DblClick(Sender: TObject);
begin
  GoToExpression(ListBox7.Items[ListBox7.ItemIndex]);
  Form7.Close;
end;

procedure TForm7.ListBox8DblClick(Sender: TObject);
begin
  GoToExpression(ListBox8.Items[ListBox8.ItemIndex]);
  Form7.Close;
end;

end.
