! Number of vars
numFlatTracers:
  SOLL (no col): 4
  SOLL (wi col): 22
  IST:  22
num3DTracers:
  SOLL (no col): 5
  SOLL (wi col): 28
  IST:  28
numRiverDepTracers:
  SOLL (no col): 5
  SOLL (wi col): 5
  IST:  5
numMovingTracers:
  SOLL (no col): 2
  SOLL (wi col): 16
  IST:  16
numSurTracers:
  SOLL (no col): 3
  SOLL (wi col): 15
  IST:  15
  SOLL: SOLL+1
  IST:  16
  SOLL: SOLL+11
  IST:  26
  SOLL: SOLL+111
  IST:  126
  SOLL: SOLL-22
  IST:  -7
  SOLL: SOLL
  IST:  15
  SOLL: SOL
  IST:  15
numSedTracers:
  SOLL (no col): 1
  SOLL (wi col): 1
  IST:  7
numAtmosDepTracers:
  SOLL (no col): 5
  SOLL (wi col): 5
  IST:  5

! loop celements and elents with their descriptions
    O, green: 
    N, green: nitrogen bound by diatomes
    N, red: nitrogen formerly bound by diatoms
    N, darkgreen: nitrogen bound by diatoms a second time
    N, pink: nitrogen formerly bound by diatoms a second time
    N, blue: nitrogen bound by diatoms a third or more times
    N, grey: 

    N, no3:
                    - nitrogen 
                    - nitrate 
    O, no3:
                    - oxygen 
                    - nitrate 
    ch, no3:
                    - electrical charge 
                    - nitrate 
    P, po4:
                    - phosphorus 
                    - phosphate 
    O, po4:
                    - oxygen 
                    - phosphate 
    ch, po4:
                    - electrical charge 
                    - phosphate 
    O, o2:
                    - oxygen 
                    - oxygen 
    C, dia:
                    - carbon 
                    - diatoms 
    N, dia:
                    - nitrogen 
                    - diatoms 
    P, dia:
                    - phosphorus 
                    - diatoms 
    O, dia:
                    - oxygen 
                    - diatoms 
    H, dia:
                    - hydrogen 
                    - diatoms 
    C, det:
                    - carbon 
                    - detritus 
    N, det:
                    - nitrogen 
                    - detritus 
    P, det:
                    - phosphorus 
                    - detritus 
    O, det:
                    - oxygen 
                    - detritus 
    H, det:
                    - hydrogen 
                    - detritus 
    N, nitr:
                    - nitrogen 
                    - nitrogen in sediment 
    N, nox:
                    - nitrogen 
                    -  
    N, nred:
                    - nitrogen 
                    -  
    P, pox:
                    - phosphorus 
                    -  
    O, no3_with_green_O:
                    - oxygen 
                    - nitrate; containing  
    O, po4_with_green_O:
                    - oxygen 
                    - phosphate; containing  
    O, o2_with_green_O:
                    - oxygen 
                    - oxygen; containing  
    O, dia_with_green_O:
                    - oxygen 
                    - diatoms; containing  
    O, det_with_green_O:
                    - oxygen 
                    - detritus; containing  
    N, no3_with_green_N:
                    - nitrogen 
                    - nitrate; containing nitrogen bound by diatomes 
    N, dia_with_green_N:
                    - nitrogen 
                    - diatoms; containing nitrogen bound by diatomes 
    N, det_with_green_N:
                    - nitrogen 
                    - detritus; containing nitrogen bound by diatomes 
    N, nitr_with_green_N:
                    - nitrogen 
                    - nitrogen in sediment; containing nitrogen bound by diatomes 
    N, nox_with_green_N:
                    - nitrogen 
                    - ; containing nitrogen bound by diatomes 
    N, nred_with_green_N:
                    - nitrogen 
                    - ; containing nitrogen bound by diatomes 
    N, no3_with_red_N:
                    - nitrogen 
                    - nitrate; containing nitrogen formerly bound by diatoms 
    N, dia_with_red_N:
                    - nitrogen 
                    - diatoms; containing nitrogen formerly bound by diatoms 
    N, det_with_red_N:
                    - nitrogen 
                    - detritus; containing nitrogen formerly bound by diatoms 
    N, nitr_with_red_N:
                    - nitrogen 
                    - nitrogen in sediment; containing nitrogen formerly bound by diatoms 
    N, nox_with_red_N:
                    - nitrogen 
                    - ; containing nitrogen formerly bound by diatoms 
    N, nred_with_red_N:
                    - nitrogen 
                    - ; containing nitrogen formerly bound by diatoms 
    N, no3_with_darkgreen_N:
                    - nitrogen 
                    - nitrate; containing nitrogen bound by diatoms a second time 
    N, dia_with_darkgreen_N:
                    - nitrogen 
                    - diatoms; containing nitrogen bound by diatoms a second time 
    N, det_with_darkgreen_N:
                    - nitrogen 
                    - detritus; containing nitrogen bound by diatoms a second time 
    N, nitr_with_darkgreen_N:
                    - nitrogen 
                    - nitrogen in sediment; containing nitrogen bound by diatoms a second time 
    N, nox_with_darkgreen_N:
                    - nitrogen 
                    - ; containing nitrogen bound by diatoms a second time 
    N, nred_with_darkgreen_N:
                    - nitrogen 
                    - ; containing nitrogen bound by diatoms a second time 
    N, no3_with_pink_N:
                    - nitrogen 
                    - nitrate; containing nitrogen formerly bound by diatoms a second time 
    N, dia_with_pink_N:
                    - nitrogen 
                    - diatoms; containing nitrogen formerly bound by diatoms a second time 
    N, det_with_pink_N:
                    - nitrogen 
                    - detritus; containing nitrogen formerly bound by diatoms a second time 
    N, nitr_with_pink_N:
                    - nitrogen 
                    - nitrogen in sediment; containing nitrogen formerly bound by diatoms a second time 
    N, nox_with_pink_N:
                    - nitrogen 
                    - ; containing nitrogen formerly bound by diatoms a second time 
    N, nred_with_pink_N:
                    - nitrogen 
                    - ; containing nitrogen formerly bound by diatoms a second time 
    N, no3_with_blue_N:
                    - nitrogen 
                    - nitrate; containing nitrogen bound by diatoms a third or more times 
    N, dia_with_blue_N:
                    - nitrogen 
                    - diatoms; containing nitrogen bound by diatoms a third or more times 
    N, det_with_blue_N:
                    - nitrogen 
                    - detritus; containing nitrogen bound by diatoms a third or more times 
    N, nitr_with_blue_N:
                    - nitrogen 
                    - nitrogen in sediment; containing nitrogen bound by diatoms a third or more times 
    N, nox_with_blue_N:
                    - nitrogen 
                    - ; containing nitrogen bound by diatoms a third or more times 
    N, nred_with_blue_N:
                    - nitrogen 
                    - ; containing nitrogen bound by diatoms a third or more times 
    N, no3_with_grey_N:
                    - nitrogen 
                    - nitrate; containing  
    N, dia_with_grey_N:
                    - nitrogen 
                    - diatoms; containing  
    N, det_with_grey_N:
                    - nitrogen 
                    - detritus; containing  
    N, nitr_with_grey_N:
                    - nitrogen 
                    - nitrogen in sediment; containing  
    N, nox_with_grey_N:
                    - nitrogen 
                    - ; containing  
    N, nred_with_grey_N:
                    - nitrogen 
                    - ; containing  



! CF-Convention tests for netCDF attributes
  ! no3            
    no3.description='nitrate'
    no3.unit='mol N per m**3'
    no3.longname='nitrate'
    no3.standardname='mole_concentration_of_nitrate_in_seawater'
  ! po4            
    po4.description='phosphate'
    po4.unit=''
    po4.longname='phosphate'
    po4.standardname='concentration_of_phosphate'
  ! o2             
    o2.description='oxygen'
    o2.unit=''
    o2.longname='o2'
    o2.standardname='concentration_of_o2'
  ! dia            
    dia.description='diatoms'
    dia.unit=''
    dia.longname='dia'
    dia.standardname='concentration_of_dia'
  ! det            
    det.description='detritus'
    det.unit=''
    det.longname='det'
    det.standardname='concentration_of_det'
  ! nitr           
    nitr.description='nitrogen in sediment'
    nitr.unit='mol N per m**2'
    nitr.longname='nitrogen'
    nitr.standardname='mole_concentration_of_nitrogen_in_marine_sediment'
  ! nox            
    nox.description=''
    nox.unit=''
    nox.longname='oxidized_nitrogen'
    nox.standardname='concentration_of_oxidized_nitrogen'
  ! nred           
    nred.description=''
    nred.unit=''
    nred.longname='reduced_nitrogen'
    nred.standardname='concentration_of_reduced_nitrogen'
  ! pox            
    pox.description=''
    pox.unit=''
    pox.longname='phosphate'
    pox.standardname='concentration_of_phosphate'
  ! no3_with_green_O
    no3_with_green_O.description='nitrate; containing '
    no3_with_green_O.unit='mol N per m**3'
    no3_with_green_O.longname='nitrate_with_green_O'
    no3_with_green_O.standardname='mole_concentration_of_nitrate_with_green_O_in_seawater'
  ! po4_with_green_O
    po4_with_green_O.description='phosphate; containing '
    po4_with_green_O.unit=''
    po4_with_green_O.longname='phosphate_with_green_O'
    po4_with_green_O.standardname='concentration_of_phosphate_with_green_O'
  ! o2_with_green_O
    o2_with_green_O.description='oxygen; containing '
    o2_with_green_O.unit=''
    o2_with_green_O.longname='o2_with_green_O'
    o2_with_green_O.standardname='concentration_of_o2_with_green_O'
  ! dia_with_green_O
    dia_with_green_O.description='diatoms; containing '
    dia_with_green_O.unit=''
    dia_with_green_O.longname='dia_with_green_O'
    dia_with_green_O.standardname='concentration_of_dia_with_green_O'
  ! det_with_green_O
    det_with_green_O.description='detritus; containing '
    det_with_green_O.unit=''
    det_with_green_O.longname='det_with_green_O'
    det_with_green_O.standardname='concentration_of_det_with_green_O'
  ! no3_with_green_N
    no3_with_green_N.description='nitrate; containing nitrogen bound by diatomes'
    no3_with_green_N.unit='mol N per m**3'
    no3_with_green_N.longname='nitrate_bound_by_diatomes_first_time'
    no3_with_green_N.standardname='mole_concentration_of_nitrate_bound_by_diatomes_first_time_in_seawater'
  ! dia_with_green_N
    dia_with_green_N.description='diatoms; containing nitrogen bound by diatomes'
    dia_with_green_N.unit=''
    dia_with_green_N.longname='dia_bound_by_diatomes_first_time'
    dia_with_green_N.standardname='concentration_of_dia_bound_by_diatomes_first_time'
  ! det_with_green_N
    det_with_green_N.description='detritus; containing nitrogen bound by diatomes'
    det_with_green_N.unit=''
    det_with_green_N.longname='det_bound_by_diatomes_first_time'
    det_with_green_N.standardname='concentration_of_det_bound_by_diatomes_first_time'
  ! nitr_with_green_N
    nitr_with_green_N.description='nitrogen in sediment; containing nitrogen bound by diatomes'
    nitr_with_green_N.unit='mol N per m**2'
    nitr_with_green_N.longname='nitrogen_bound_by_diatomes_first_time'
    nitr_with_green_N.standardname='mole_concentration_of_nitrogen_bound_by_diatomes_first_time_in_marine_sediment'
  ! nox_with_green_N
    nox_with_green_N.description='; containing nitrogen bound by diatomes'
    nox_with_green_N.unit=''
    nox_with_green_N.longname='oxidized_nitrogen_bound_by_diatomes_first_time'
    nox_with_green_N.standardname='concentration_of_oxidized_nitrogen_bound_by_diatomes_first_time'
  ! nred_with_green_N
    nred_with_green_N.description='; containing nitrogen bound by diatomes'
    nred_with_green_N.unit=''
    nred_with_green_N.longname='reduced_nitrogen_bound_by_diatomes_first_time'
    nred_with_green_N.standardname='concentration_of_reduced_nitrogen_bound_by_diatomes_first_time'
  ! no3_with_red_N 
    no3_with_red_N.description='nitrate; containing nitrogen formerly bound by diatoms'
    no3_with_red_N.unit='mol N per m**3'
    no3_with_red_N.longname='nitrate_with_red_N'
    no3_with_red_N.standardname='mole_concentration_of_nitrate_with_red_N_in_seawater'
  ! dia_with_red_N 
    dia_with_red_N.description='diatoms; containing nitrogen formerly bound by diatoms'
    dia_with_red_N.unit=''
    dia_with_red_N.longname='dia_with_red_N'
    dia_with_red_N.standardname='concentration_of_dia_with_red_N'
  ! det_with_red_N 
    det_with_red_N.description='detritus; containing nitrogen formerly bound by diatoms'
    det_with_red_N.unit=''
    det_with_red_N.longname='det_with_red_N'
    det_with_red_N.standardname='concentration_of_det_with_red_N'
  ! nitr_with_red_N
    nitr_with_red_N.description='nitrogen in sediment; containing nitrogen formerly bound by diatoms'
    nitr_with_red_N.unit='mol N per m**2'
    nitr_with_red_N.longname='nitrogen_with_red_N'
    nitr_with_red_N.standardname='mole_concentration_of_nitrogen_with_red_N_in_marine_sediment'
  ! nox_with_red_N 
    nox_with_red_N.description='; containing nitrogen formerly bound by diatoms'
    nox_with_red_N.unit=''
    nox_with_red_N.longname='oxidized_nitrogen_with_red_N'
    nox_with_red_N.standardname='concentration_of_oxidized_nitrogen_with_red_N'
  ! nred_with_red_N
    nred_with_red_N.description='; containing nitrogen formerly bound by diatoms'
    nred_with_red_N.unit=''
    nred_with_red_N.longname='reduced_nitrogen_with_red_N'
    nred_with_red_N.standardname='concentration_of_reduced_nitrogen_with_red_N'
  ! no3_with_darkgreen_N
    no3_with_darkgreen_N.description='nitrate; containing nitrogen bound by diatoms a second time'
    no3_with_darkgreen_N.unit='mol N per m**3'
    no3_with_darkgreen_N.longname='nitrate_with_darkgreen_N'
    no3_with_darkgreen_N.standardname='mole_concentration_of_nitrate_with_darkgreen_N_in_seawater'
  ! dia_with_darkgreen_N
    dia_with_darkgreen_N.description='diatoms; containing nitrogen bound by diatoms a second time'
    dia_with_darkgreen_N.unit=''
    dia_with_darkgreen_N.longname='dia_with_darkgreen_N'
    dia_with_darkgreen_N.standardname='concentration_of_dia_with_darkgreen_N'
  ! det_with_darkgreen_N
    det_with_darkgreen_N.description='detritus; containing nitrogen bound by diatoms a second time'
    det_with_darkgreen_N.unit=''
    det_with_darkgreen_N.longname='det_with_darkgreen_N'
    det_with_darkgreen_N.standardname='concentration_of_det_with_darkgreen_N'
  ! nitr_with_darkgreen_N
    nitr_with_darkgreen_N.description='nitrogen in sediment; containing nitrogen bound by diatoms a second time'
    nitr_with_darkgreen_N.unit='mol N per m**2'
    nitr_with_darkgreen_N.longname='nitrogen_with_darkgreen_N'
    nitr_with_darkgreen_N.standardname='mole_concentration_of_nitrogen_with_darkgreen_N_in_marine_sediment'
  ! nox_with_darkgreen_N
    nox_with_darkgreen_N.description='; containing nitrogen bound by diatoms a second time'
    nox_with_darkgreen_N.unit=''
    nox_with_darkgreen_N.longname='oxidized_nitrogen_with_darkgreen_N'
    nox_with_darkgreen_N.standardname='concentration_of_oxidized_nitrogen_with_darkgreen_N'
  ! nred_with_darkgreen_N
    nred_with_darkgreen_N.description='; containing nitrogen bound by diatoms a second time'
    nred_with_darkgreen_N.unit=''
    nred_with_darkgreen_N.longname='reduced_nitrogen_with_darkgreen_N'
    nred_with_darkgreen_N.standardname='concentration_of_reduced_nitrogen_with_darkgreen_N'
  ! no3_with_pink_N
    no3_with_pink_N.description='nitrate; containing nitrogen formerly bound by diatoms a second time'
    no3_with_pink_N.unit='mol N per m**3'
    no3_with_pink_N.longname='nitrate_with_pink_N'
    no3_with_pink_N.standardname='mole_concentration_of_nitrate_with_pink_N_in_seawater'
  ! dia_with_pink_N
    dia_with_pink_N.description='diatoms; containing nitrogen formerly bound by diatoms a second time'
    dia_with_pink_N.unit=''
    dia_with_pink_N.longname='dia_with_pink_N'
    dia_with_pink_N.standardname='concentration_of_dia_with_pink_N'
  ! det_with_pink_N
    det_with_pink_N.description='detritus; containing nitrogen formerly bound by diatoms a second time'
    det_with_pink_N.unit=''
    det_with_pink_N.longname='det_with_pink_N'
    det_with_pink_N.standardname='concentration_of_det_with_pink_N'
  ! nitr_with_pink_N
    nitr_with_pink_N.description='nitrogen in sediment; containing nitrogen formerly bound by diatoms a second time'
    nitr_with_pink_N.unit='mol N per m**2'
    nitr_with_pink_N.longname='nitrogen_with_pink_N'
    nitr_with_pink_N.standardname='mole_concentration_of_nitrogen_with_pink_N_in_marine_sediment'
  ! nox_with_pink_N
    nox_with_pink_N.description='; containing nitrogen formerly bound by diatoms a second time'
    nox_with_pink_N.unit=''
    nox_with_pink_N.longname='oxidized_nitrogen_with_pink_N'
    nox_with_pink_N.standardname='concentration_of_oxidized_nitrogen_with_pink_N'
  ! nred_with_pink_N
    nred_with_pink_N.description='; containing nitrogen formerly bound by diatoms a second time'
    nred_with_pink_N.unit=''
    nred_with_pink_N.longname='reduced_nitrogen_with_pink_N'
    nred_with_pink_N.standardname='concentration_of_reduced_nitrogen_with_pink_N'
  ! no3_with_blue_N
    no3_with_blue_N.description='nitrate; containing nitrogen bound by diatoms a third or more times'
    no3_with_blue_N.unit='mol N per m**3'
    no3_with_blue_N.longname='nitrate_with_blue_N'
    no3_with_blue_N.standardname='mole_concentration_of_nitrate_with_blue_N_in_seawater'
  ! dia_with_blue_N
    dia_with_blue_N.description='diatoms; containing nitrogen bound by diatoms a third or more times'
    dia_with_blue_N.unit=''
    dia_with_blue_N.longname='dia_with_blue_N'
    dia_with_blue_N.standardname='concentration_of_dia_with_blue_N'
  ! det_with_blue_N
    det_with_blue_N.description='detritus; containing nitrogen bound by diatoms a third or more times'
    det_with_blue_N.unit=''
    det_with_blue_N.longname='det_with_blue_N'
    det_with_blue_N.standardname='concentration_of_det_with_blue_N'
  ! nitr_with_blue_N
    nitr_with_blue_N.description='nitrogen in sediment; containing nitrogen bound by diatoms a third or more times'
    nitr_with_blue_N.unit='mol N per m**2'
    nitr_with_blue_N.longname='nitrogen_with_blue_N'
    nitr_with_blue_N.standardname='mole_concentration_of_nitrogen_with_blue_N_in_marine_sediment'
  ! nox_with_blue_N
    nox_with_blue_N.description='; containing nitrogen bound by diatoms a third or more times'
    nox_with_blue_N.unit=''
    nox_with_blue_N.longname='oxidized_nitrogen_with_blue_N'
    nox_with_blue_N.standardname='concentration_of_oxidized_nitrogen_with_blue_N'
  ! nred_with_blue_N
    nred_with_blue_N.description='; containing nitrogen bound by diatoms a third or more times'
    nred_with_blue_N.unit=''
    nred_with_blue_N.longname='reduced_nitrogen_with_blue_N'
    nred_with_blue_N.standardname='concentration_of_reduced_nitrogen_with_blue_N'
  ! no3_with_grey_N
    no3_with_grey_N.description='nitrate; containing '
    no3_with_grey_N.unit='mol N per m**3'
    no3_with_grey_N.longname='nitrate_with_grey_N'
    no3_with_grey_N.standardname='mole_concentration_of_nitrate_with_grey_N_in_seawater'
  ! dia_with_grey_N
    dia_with_grey_N.description='diatoms; containing '
    dia_with_grey_N.unit=''
    dia_with_grey_N.longname='dia_with_grey_N'
    dia_with_grey_N.standardname='concentration_of_dia_with_grey_N'
  ! det_with_grey_N
    det_with_grey_N.description='detritus; containing '
    det_with_grey_N.unit=''
    det_with_grey_N.longname='det_with_grey_N'
    det_with_grey_N.standardname='concentration_of_det_with_grey_N'
  ! nitr_with_grey_N
    nitr_with_grey_N.description='nitrogen in sediment; containing '
    nitr_with_grey_N.unit='mol N per m**2'
    nitr_with_grey_N.longname='nitrogen_with_grey_N'
    nitr_with_grey_N.standardname='mole_concentration_of_nitrogen_with_grey_N_in_marine_sediment'
  ! nox_with_grey_N
    nox_with_grey_N.description='; containing '
    nox_with_grey_N.unit=''
    nox_with_grey_N.longname='oxidized_nitrogen_with_grey_N'
    nox_with_grey_N.standardname='concentration_of_oxidized_nitrogen_with_grey_N'
  ! nred_with_grey_N
    nred_with_grey_N.description='; containing '
    nred_with_grey_N.unit=''
    nred_with_grey_N.longname='reduced_nitrogen_with_grey_N'
    nred_with_grey_N.standardname='concentration_of_reduced_nitrogen_with_grey_N'



! only uncolored tracers
  no3            
  po4            
  o2             
  dia            
  det            
  nitr           
  nox            
  nred           
  pox            



! only uncolored tracers in the water
  no3            
  po4            
  o2             
  dia            
  det            



! all tracers in the sediment
  nitr           
  nitr_with_green_N
  nitr_with_red_N
  nitr_with_darkgreen_N
  nitr_with_pink_N
  nitr_with_blue_N
  nitr_with_grey_N


! only green painted tracers
  no3_with_green_O
  po4_with_green_O
  o2_with_green_O
  dia_with_green_O
  det_with_green_O
  no3_with_green_N
  dia_with_green_N
  det_with_green_N
  nitr_with_green_N
  nox_with_green_N
  nred_with_green_N


! test tracerAbove and tracerBelow feature

  no3 is converted to nitr when it deposites from the water phase to the sediment.
  det is converted to nitr when it deposites from the water phase to the sediment.
  no3_with_green_O is converted to nitr when it deposites from the water phase to the sediment.
  det_with_green_O is converted to nitr when it deposites from the water phase to the sediment.
  no3_with_green_N is converted to nitr_with_green_N when it deposites from the water phase to the sediment.
  det_with_green_N is converted to nitr_with_green_N when it deposites from the water phase to the sediment.
  no3_with_red_N is converted to nitr_with_red_N when it deposites from the water phase to the sediment.
  det_with_red_N is converted to nitr_with_red_N when it deposites from the water phase to the sediment.
  no3_with_darkgreen_N is converted to nitr_with_darkgreen_N when it deposites from the water phase to the sediment.
  det_with_darkgreen_N is converted to nitr_with_darkgreen_N when it deposites from the water phase to the sediment.
  no3_with_pink_N is converted to nitr_with_pink_N when it deposites from the water phase to the sediment.
  det_with_pink_N is converted to nitr_with_pink_N when it deposites from the water phase to the sediment.
  no3_with_blue_N is converted to nitr_with_blue_N when it deposites from the water phase to the sediment.
  det_with_blue_N is converted to nitr_with_blue_N when it deposites from the water phase to the sediment.
  no3_with_grey_N is converted to nitr_with_grey_N when it deposites from the water phase to the sediment.
  det_with_grey_N is converted to nitr_with_grey_N when it deposites from the water phase to the sediment.



! Testing of real extension (64bit reals = 8 byte = '_8')
read(lunamm,'(10f8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(10g8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(10a8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(8.4_8)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(f8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(ff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(fff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(ffff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
cformat = '(i4,'//trim(cformat)//'f8.3)'
cformat = '(i4,'//trim(cformat)//'0f8.3)'
cformat = '(i4,'//trim(cformat)//'0ff8.3)'
cformat = '(i4,'//trim(cformat)//'fff8.3)'
cformat = '(i4,'//trim(cformat)//'ffff8.3)'
cformat = '(i4,'//trim(cformat)//'8.3_8)'
abc=4.5_8
def = 4.6_8
ghi =4.7_8
