
% iterate all tracers
<tracers>
  % <description>
  <name>
  <childOf>
  
</tracers>

% iterate un-colored tracers
<tracers childof=none>
  % description
  "<name>"
  "<trimName>"
  <childOf>
  
  % now iterate all colored elemets:
  <elements hasColoredCopies=1>
    Colored <name> is made of colored <element>!
  </elements>
  <elements hasColoredCopies=0>
    Colored <name> is made of uncolored <element>!
  </elements>
  
  % time tendency
  <timetendencies>
    <timeTendency> 
  </timetendencies>
  
  % now iterate all elemets:
  <elements>
    <name> is made of <element>!
  </elements>
  
</tracers>
 
% end!