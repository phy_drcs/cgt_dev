! normal
  no3: + respiration_dia             
  no3: - growth_dia                  
  no3: - (mineralization_nitr_sed)*(ldn_N_sed)/(cgt_cellheight*cgt_density)
  dia: + growth_dia                  
  dia: - respiration_dia             
  dia: - mortality_dia               

! no invertSign
  no3: + respiration_dia             
  no3: - growth_dia                  
  no3: - (mineralization_nitr_sed)*(ldn_N_sed)/(cgt_cellheight*cgt_density)
  dia: + growth_dia                  
  dia: - respiration_dia             
  dia: - mortality_dia               

! inverted sign
  no3: - respiration_dia             
  no3: + growth_dia                  
  no3: + (mineralization_nitr_sed)*(ldn_N_sed)/(cgt_cellheight*cgt_density)
  dia: - growth_dia                  
  dia: + respiration_dia             
  dia: + mortality_dia               

! only LHS with positive sign
  no3: <timeTendency invertSign>
  no3: <timeTendency invertSign>
  dia: <timeTendency invertSign>
  dia: <timeTendency invertSign>

! only RHS
  no3: + respiration_dia             
  dia: + growth_dia                  

! none 1

! none 2

! default 1
  no3: + respiration_dia             
  no3: - growth_dia                  
  no3: - (mineralization_nitr_sed)*(ldn_N_sed)/(cgt_cellheight*cgt_density)
  dia: + growth_dia                  
  dia: - respiration_dia             
  dia: - mortality_dia               

! default 2
  no3: + respiration_dia             
  no3: - growth_dia                  
  no3: - (mineralization_nitr_sed)*(ldn_N_sed)/(cgt_cellheight*cgt_density)
  dia: + growth_dia                  
  dia: - respiration_dia             
  dia: - mortality_dia               
