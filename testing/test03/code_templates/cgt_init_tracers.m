%--------------------------------
% load initial values for tracers
%--------------------------------

% some need to be loaded from files
<tracers isFlat=0; useInitValue=0>
  tempstring = strtrim('<name>');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_vector_<name> = load(tempstring);

</tracers>
<tracers isFlat=1; useInitValue=0>
  tempstring = strtrim('<name>');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_scalar_<name> = load(tempstring);

</tracers>
<tracers isFlat=2; useInitValue=0>
  tempstring = strtrim('<name>');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_scalar_<name> = load(tempstring);

</tracers>

% others are initialized as constant
<tracers isFlat=0; useInitValue=1>
  tracer_vector_<name> = ones(kmax,1)*<initValue>;
</tracers>
<tracers isFlat=1; useInitValue=1>
  tracer_scalar_<name> = <initValue>;
</tracers>
<tracers isFlat=2; useInitValue=1>
  tracer_scalar_<name> = <initValue>;
</tracers>

% some tracers have vertical movement
<tracers isFlat=0; vertSpeed/=0>
  vertical_speed_of_<name> = zeros(kmax,1);
  vertical_diffusivity_of_<name> = zeros(kmax,1);
</tracers>

% auxiliaries which communicate data from the last time step are set to 0
<auxiliaries isFlat=0; isUsedElsewhere=1>
  auxiliary_vector_<name> = zeros(kmax,1);
</auxiliaries>
<auxiliaries isFlat=1; isUsedElsewhere=1>
  auxiliary_scalar_<name> = 0.0;
</auxiliaries>
<auxiliaries isFlat=2; isUsedElsewhere=1>
  auxiliary_scalar_<name> = 0.0;
</auxiliaries>
