%--------------------------------
% load initial values for tracers
%--------------------------------

% some need to be loaded from files
  tempstring = strtrim('nh4            ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_vector_nh4             = load(tempstring);

  tempstring = strtrim('no3            ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_vector_no3             = load(tempstring);

  tempstring = strtrim('po4            ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_vector_po4             = load(tempstring);

  tempstring = strtrim('o2             ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_vector_o2              = load(tempstring);

  tempstring = strtrim('dia            ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_vector_dia             = load(tempstring);

  tempstring = strtrim('det            ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_vector_det             = load(tempstring);

  tempstring = strtrim('nitr           ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_scalar_nitr            = load(tempstring);

  tempstring = strtrim('nox            ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_scalar_nox             = load(tempstring);

  tempstring = strtrim('nred           ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_scalar_nred            = load(tempstring);

  tempstring = strtrim('pox            ');
  tempstring = strcat('init/',tempstring,'.txt');
  tracer_scalar_pox             = load(tempstring);


% others are initialized as constant
  tracer_vector_no3_with_green_O = ones(kmax,1)*0.0;
  tracer_vector_po4_with_green_O = ones(kmax,1)*0.0;
  tracer_vector_o2_with_green_O = ones(kmax,1)*0.0;
  tracer_vector_dia_with_green_O = ones(kmax,1)*0.0;
  tracer_vector_det_with_green_O = ones(kmax,1)*0.0;
  tracer_vector_nh4_with_green_N = ones(kmax,1)*0.0;
  tracer_vector_no3_with_green_N = ones(kmax,1)*0.0;
  tracer_vector_dia_with_green_N = ones(kmax,1)*0.0;
  tracer_vector_det_with_green_N = ones(kmax,1)*0.0;
  tracer_vector_nh4_with_red_N  = ones(kmax,1)*0.0;
  tracer_vector_no3_with_red_N  = ones(kmax,1)*0.0;
  tracer_vector_dia_with_red_N  = ones(kmax,1)*0.0;
  tracer_vector_det_with_red_N  = ones(kmax,1)*0.0;
  tracer_vector_nh4_with_darkgreen_N = ones(kmax,1)*0.0;
  tracer_vector_no3_with_darkgreen_N = ones(kmax,1)*0.0;
  tracer_vector_dia_with_darkgreen_N = ones(kmax,1)*0.0;
  tracer_vector_det_with_darkgreen_N = ones(kmax,1)*0.0;
  tracer_vector_nh4_with_pink_N = ones(kmax,1)*0.0;
  tracer_vector_no3_with_pink_N = ones(kmax,1)*0.0;
  tracer_vector_dia_with_pink_N = ones(kmax,1)*0.0;
  tracer_vector_det_with_pink_N = ones(kmax,1)*0.0;
  tracer_vector_nh4_with_blue_N = ones(kmax,1)*0.0;
  tracer_vector_no3_with_blue_N = ones(kmax,1)*0.0;
  tracer_vector_dia_with_blue_N = ones(kmax,1)*0.0;
  tracer_vector_det_with_blue_N = ones(kmax,1)*0.0;
  tracer_vector_nh4_with_grey_N = ones(kmax,1)*0.0;
  tracer_vector_no3_with_grey_N = ones(kmax,1)*0.0;
  tracer_vector_dia_with_grey_N = ones(kmax,1)*0.0;
  tracer_vector_det_with_grey_N = ones(kmax,1)*0.0;
  tracer_scalar_nitr_with_green_N = 0.0;
  tracer_scalar_nitr_with_red_N = 0.0;
  tracer_scalar_nitr_with_darkgreen_N = 0.0;
  tracer_scalar_nitr_with_pink_N = 0.0;
  tracer_scalar_nitr_with_blue_N = 0.0;
  tracer_scalar_nitr_with_grey_N = 0.0;
  tracer_scalar_nox_with_green_N = 0.0;
  tracer_scalar_nred_with_green_N = 0.0;
  tracer_scalar_nox_with_red_N  = 0.0;
  tracer_scalar_nred_with_red_N = 0.0;
  tracer_scalar_nox_with_darkgreen_N = 0.0;
  tracer_scalar_nred_with_darkgreen_N = 0.0;
  tracer_scalar_nox_with_pink_N = 0.0;
  tracer_scalar_nred_with_pink_N = 0.0;
  tracer_scalar_nox_with_blue_N = 0.0;
  tracer_scalar_nred_with_blue_N = 0.0;
  tracer_scalar_nox_with_grey_N = 0.0;
  tracer_scalar_nred_with_grey_N = 0.0;

% some tracers have vertical movement
  vertical_speed_of_dia             = zeros(kmax,1);
  vertical_diffusivity_of_dia             = zeros(kmax,1);
  vertical_speed_of_det             = zeros(kmax,1);
  vertical_diffusivity_of_det             = zeros(kmax,1);
  vertical_speed_of_dia_with_green_O = zeros(kmax,1);
  vertical_diffusivity_of_dia_with_green_O = zeros(kmax,1);
  vertical_speed_of_det_with_green_O = zeros(kmax,1);
  vertical_diffusivity_of_det_with_green_O = zeros(kmax,1);
  vertical_speed_of_dia_with_green_N = zeros(kmax,1);
  vertical_diffusivity_of_dia_with_green_N = zeros(kmax,1);
  vertical_speed_of_det_with_green_N = zeros(kmax,1);
  vertical_diffusivity_of_det_with_green_N = zeros(kmax,1);
  vertical_speed_of_dia_with_red_N  = zeros(kmax,1);
  vertical_diffusivity_of_dia_with_red_N  = zeros(kmax,1);
  vertical_speed_of_det_with_red_N  = zeros(kmax,1);
  vertical_diffusivity_of_det_with_red_N  = zeros(kmax,1);
  vertical_speed_of_dia_with_darkgreen_N = zeros(kmax,1);
  vertical_diffusivity_of_dia_with_darkgreen_N = zeros(kmax,1);
  vertical_speed_of_det_with_darkgreen_N = zeros(kmax,1);
  vertical_diffusivity_of_det_with_darkgreen_N = zeros(kmax,1);
  vertical_speed_of_dia_with_pink_N = zeros(kmax,1);
  vertical_diffusivity_of_dia_with_pink_N = zeros(kmax,1);
  vertical_speed_of_det_with_pink_N = zeros(kmax,1);
  vertical_diffusivity_of_det_with_pink_N = zeros(kmax,1);
  vertical_speed_of_dia_with_blue_N = zeros(kmax,1);
  vertical_diffusivity_of_dia_with_blue_N = zeros(kmax,1);
  vertical_speed_of_det_with_blue_N = zeros(kmax,1);
  vertical_diffusivity_of_det_with_blue_N = zeros(kmax,1);
  vertical_speed_of_dia_with_grey_N = zeros(kmax,1);
  vertical_diffusivity_of_dia_with_grey_N = zeros(kmax,1);
  vertical_speed_of_det_with_grey_N = zeros(kmax,1);
  vertical_diffusivity_of_det_with_grey_N = zeros(kmax,1);

% auxiliaries which communicate data from the last time step are set to 0
