! BioCGT constants file
! *********************
! properties of constants:
!   name=           code name of constant
!   description=    description including unit, default=""
!   value=          value(s) separated by ";"
!   dependsOn=      whether this constant varies in time and space, default="none", other possible values: "xyz"
!   minval=         minimum value for variation (for inverse modelling)
!   maxval=         maximum value for variation (for inverse modelling)
!   varphase=       phase in which a parameter is allowed to vary (for inverse modelling) (default=1)
!   comment=        comment, e.g. how certain this value is, literature,..., default=""
! *************************************************************************************
name        = growth_max_dia
value       = 1.3
description = maximum growth rate of diatoms [1/day]
***********************
name        = no3_min_dia
value       = 1.0e-6
description = diatoms half-saturation constant for DIN [mol/kg]
***********************
name        = po4_min_dia
value       = 6.25e-8
description = diatoms half-saturation constant for DIP [mol/kg]
***********************
name        = light_min_dia
value       = 35.0
description = minimum light for diatoms growth [W/m**2]
***********************
name        = resp_rate_dia
value       = 0.05
description = respiration rate of diatoms [1/day]
***********************
name        = mort_rate_dia
value       = 0.02
description = mortality rate of diatoms [1/day]
***********************
name        = piston_vel_oxy
value       = 5.0
description = piston velocity for oxygen surface flux[m/d]
***********************
