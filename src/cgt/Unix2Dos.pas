unit Unix2Dos;

{$MODE Delphi}

interface

uses LCLIntf, LCLType, LMessages, FileUtil, LazFileUtils;

procedure ConvertDosToUnix(ifile: String; ofile: String='');

implementation

procedure ConvertDosToUnix(ifile: String; ofile: String='');
var Q, Z: TextFile;
    s: String;
begin
  AssignFile(Q, ifile);
  reset(Q);
  if ofile='' then
    AssignFile(Z, ifile+'__unix__')
  else
    AssignFile(Z, ofile);
  rewrite(Z);
  while not EOF(Q) do
  begin
    readln(Q,s);
    write(Z,s);
    write(Z,chr(10));
  end;
  closefile(Q);
  closefile(Z);
  if ofile='' then
  begin
    CopyFile(pchar(ifile+'__unix__'),pchar(ifile),false);
    DeleteFileUTF8(pchar(ifile+'__unix__')); { *Converted from DeleteFile*  }
  end;
end;

end.
