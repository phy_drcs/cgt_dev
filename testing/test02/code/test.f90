! loop celements and elents with their descriptions

    O, green: 
    N, green: nitrogen bound by diatomes
    N, red: nitrogen formerly bound by diatoms
    N, darkgreen: nitrogen bound by diatoms a second time
    N, pink: nitrogen formerly bound by diatoms a second time
    N, blue: nitrogen bound by diatoms a third or more times
    N, grey: 

    N, no3:
                    - nitrogen 
                    - nitrate 
    O, no3:
                    - oxygen 
                    - nitrate 
    ch, no3:
                    - electrical charge 
                    - nitrate 
    P, po4:
                    - phosphorus 
                    - phosphate 
    O, po4:
                    - oxygen 
                    - phosphate 
    ch, po4:
                    - electrical charge 
                    - phosphate 
    O, o2:
                    - oxygen 
                    - oxygen 
    C, dia:
                    - carbon 
                    - diatoms 
    N, dia:
                    - nitrogen 
                    - diatoms 
    P, dia:
                    - phosphorus 
                    - diatoms 
    O, dia:
                    - oxygen 
                    - diatoms 
    H, dia:
                    - hydrogen 
                    - diatoms 
    C, det:
                    - carbon 
                    - detritus 
    N, det:
                    - nitrogen 
                    - detritus 
    P, det:
                    - phosphorus 
                    - detritus 
    O, det:
                    - oxygen 
                    - detritus 
    H, det:
                    - hydrogen 
                    - detritus 
    O, no3_with_green_O:
                    - oxygen 
                    - nitrate; containing  
    O, po4_with_green_O:
                    - oxygen 
                    - phosphate; containing  
    O, o2_with_green_O:
                    - oxygen 
                    - oxygen; containing  
    O, dia_with_green_O:
                    - oxygen 
                    - diatoms; containing  
    O, det_with_green_O:
                    - oxygen 
                    - detritus; containing  
    N, no3_with_green_N:
                    - nitrogen 
                    - nitrate; containing nitrogen bound by diatomes 
    N, dia_with_green_N:
                    - nitrogen 
                    - diatoms; containing nitrogen bound by diatomes 
    N, det_with_green_N:
                    - nitrogen 
                    - detritus; containing nitrogen bound by diatomes 
    N, no3_with_red_N:
                    - nitrogen 
                    - nitrate; containing nitrogen formerly bound by diatoms 
    N, dia_with_red_N:
                    - nitrogen 
                    - diatoms; containing nitrogen formerly bound by diatoms 
    N, det_with_red_N:
                    - nitrogen 
                    - detritus; containing nitrogen formerly bound by diatoms 
    N, no3_with_darkgreen_N:
                    - nitrogen 
                    - nitrate; containing nitrogen bound by diatoms a second time 
    N, dia_with_darkgreen_N:
                    - nitrogen 
                    - diatoms; containing nitrogen bound by diatoms a second time 
    N, det_with_darkgreen_N:
                    - nitrogen 
                    - detritus; containing nitrogen bound by diatoms a second time 
    N, no3_with_pink_N:
                    - nitrogen 
                    - nitrate; containing nitrogen formerly bound by diatoms a second time 
    N, dia_with_pink_N:
                    - nitrogen 
                    - diatoms; containing nitrogen formerly bound by diatoms a second time 
    N, det_with_pink_N:
                    - nitrogen 
                    - detritus; containing nitrogen formerly bound by diatoms a second time 
    N, no3_with_blue_N:
                    - nitrogen 
                    - nitrate; containing nitrogen bound by diatoms a third or more times 
    N, dia_with_blue_N:
                    - nitrogen 
                    - diatoms; containing nitrogen bound by diatoms a third or more times 
    N, det_with_blue_N:
                    - nitrogen 
                    - detritus; containing nitrogen bound by diatoms a third or more times 
    N, no3_with_grey_N:
                    - nitrogen 
                    - nitrate; containing  
    N, dia_with_grey_N:
                    - nitrogen 
                    - diatoms; containing  
    N, det_with_grey_N:
                    - nitrogen 
                    - detritus; containing  

! CF-Convention tests for netCDF attributes

no3            .description='nitrate'
no3            .unit=''
no3            .longname='no3'
no3            .standardname='concentration_of_no3'
po4            .description='phosphate'
po4            .unit=''
po4            .longname='po4'
po4            .standardname='concentration_of_po4'
o2             .description='oxygen'
o2             .unit=''
o2             .longname='o2'
o2             .standardname='concentration_of_o2'
dia            .description='diatoms'
dia            .unit=''
dia            .longname='dia'
dia            .standardname='concentration_of_dia'
det            .description='detritus'
det            .unit=''
det            .longname='det'
det            .standardname='concentration_of_det'
no3_with_green_O.description='nitrate; containing '
no3_with_green_O.unit=''
no3_with_green_O.longname='no3_with_green_O'
no3_with_green_O.standardname='concentration_of_no3_with_green_O'
po4_with_green_O.description='phosphate; containing '
po4_with_green_O.unit=''
po4_with_green_O.longname='po4_with_green_O'
po4_with_green_O.standardname='concentration_of_po4_with_green_O'
o2_with_green_O.description='oxygen; containing '
o2_with_green_O.unit=''
o2_with_green_O.longname='o2_with_green_O'
o2_with_green_O.standardname='concentration_of_o2_with_green_O'
dia_with_green_O.description='diatoms; containing '
dia_with_green_O.unit=''
dia_with_green_O.longname='dia_with_green_O'
dia_with_green_O.standardname='concentration_of_dia_with_green_O'
det_with_green_O.description='detritus; containing '
det_with_green_O.unit=''
det_with_green_O.longname='det_with_green_O'
det_with_green_O.standardname='concentration_of_det_with_green_O'
no3_with_green_N.description='nitrate; containing nitrogen bound by diatomes'
no3_with_green_N.unit=''
no3_with_green_N.longname='no3_with_green_N'
no3_with_green_N.standardname='concentration_of_no3_with_green_N'
dia_with_green_N.description='diatoms; containing nitrogen bound by diatomes'
dia_with_green_N.unit=''
dia_with_green_N.longname='dia_with_green_N'
dia_with_green_N.standardname='concentration_of_dia_with_green_N'
det_with_green_N.description='detritus; containing nitrogen bound by diatomes'
det_with_green_N.unit=''
det_with_green_N.longname='det_with_green_N'
det_with_green_N.standardname='concentration_of_det_with_green_N'
no3_with_red_N .description='nitrate; containing nitrogen formerly bound by diatoms'
no3_with_red_N .unit=''
no3_with_red_N .longname='no3_with_red_N'
no3_with_red_N .standardname='concentration_of_no3_with_red_N'
dia_with_red_N .description='diatoms; containing nitrogen formerly bound by diatoms'
dia_with_red_N .unit=''
dia_with_red_N .longname='dia_with_red_N'
dia_with_red_N .standardname='concentration_of_dia_with_red_N'
det_with_red_N .description='detritus; containing nitrogen formerly bound by diatoms'
det_with_red_N .unit=''
det_with_red_N .longname='det_with_red_N'
det_with_red_N .standardname='concentration_of_det_with_red_N'
no3_with_darkgreen_N.description='nitrate; containing nitrogen bound by diatoms a second time'
no3_with_darkgreen_N.unit=''
no3_with_darkgreen_N.longname='no3_with_darkgreen_N'
no3_with_darkgreen_N.standardname='concentration_of_no3_with_darkgreen_N'
dia_with_darkgreen_N.description='diatoms; containing nitrogen bound by diatoms a second time'
dia_with_darkgreen_N.unit=''
dia_with_darkgreen_N.longname='dia_with_darkgreen_N'
dia_with_darkgreen_N.standardname='concentration_of_dia_with_darkgreen_N'
det_with_darkgreen_N.description='detritus; containing nitrogen bound by diatoms a second time'
det_with_darkgreen_N.unit=''
det_with_darkgreen_N.longname='det_with_darkgreen_N'
det_with_darkgreen_N.standardname='concentration_of_det_with_darkgreen_N'
no3_with_pink_N.description='nitrate; containing nitrogen formerly bound by diatoms a second time'
no3_with_pink_N.unit=''
no3_with_pink_N.longname='no3_with_pink_N'
no3_with_pink_N.standardname='concentration_of_no3_with_pink_N'
dia_with_pink_N.description='diatoms; containing nitrogen formerly bound by diatoms a second time'
dia_with_pink_N.unit=''
dia_with_pink_N.longname='dia_with_pink_N'
dia_with_pink_N.standardname='concentration_of_dia_with_pink_N'
det_with_pink_N.description='detritus; containing nitrogen formerly bound by diatoms a second time'
det_with_pink_N.unit=''
det_with_pink_N.longname='det_with_pink_N'
det_with_pink_N.standardname='concentration_of_det_with_pink_N'
no3_with_blue_N.description='nitrate; containing nitrogen bound by diatoms a third or more times'
no3_with_blue_N.unit=''
no3_with_blue_N.longname='no3_with_blue_N'
no3_with_blue_N.standardname='concentration_of_no3_with_blue_N'
dia_with_blue_N.description='diatoms; containing nitrogen bound by diatoms a third or more times'
dia_with_blue_N.unit=''
dia_with_blue_N.longname='dia_with_blue_N'
dia_with_blue_N.standardname='concentration_of_dia_with_blue_N'
det_with_blue_N.description='detritus; containing nitrogen bound by diatoms a third or more times'
det_with_blue_N.unit=''
det_with_blue_N.longname='det_with_blue_N'
det_with_blue_N.standardname='concentration_of_det_with_blue_N'
no3_with_grey_N.description='nitrate; containing '
no3_with_grey_N.unit=''
no3_with_grey_N.longname='no3_with_grey_N'
no3_with_grey_N.standardname='concentration_of_no3_with_grey_N'
dia_with_grey_N.description='diatoms; containing '
dia_with_grey_N.unit=''
dia_with_grey_N.longname='dia_with_grey_N'
dia_with_grey_N.standardname='concentration_of_dia_with_grey_N'
det_with_grey_N.description='detritus; containing '
det_with_grey_N.unit=''
det_with_grey_N.longname='det_with_grey_N'
det_with_grey_N.standardname='concentration_of_det_with_grey_N'



! Testing of real extension (64bit reals = 8 byte = '_8')
read(lunamm,'(10f8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(10g8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(10a8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(f8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(ff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(fff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(ffff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
cformat = '(i4,'//trim(cformat)//'f8.3)'
cformat = '(i4,'//trim(cformat)//'0f8.3)'
cformat = '(i4,'//trim(cformat)//'0ff8.3)'
cformat = '(i4,'//trim(cformat)//'fff8.3)'
cformat = '(i4,'//trim(cformat)//'ffff8.3)'
cformat = '(i4,'//trim(cformat)//'8.3)'
abc=4.5
def = 4.6
ghi =4.7
