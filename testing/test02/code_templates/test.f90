! loop celements and elents with their descriptions

<celements>
    <element>, <color>: <description>
</celements>

<tracers>
  <elements>
    <element>, <trimName>:
                    - <elementDescription> 
                    - <description> 
  </elements>
</tracers>

! CF-Convention tests for netCDF attributes

<tracers>
<name>.description='<description>'
<name>.unit='<unit>'
<name>.longname='<longname>'
<name>.standardname='<standardname>'
</tracers>



! Testing of real extension (64bit reals = 8 byte = '_8')
read(lunamm,'(10f8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(10g8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(10a8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(f8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(ff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(fff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(ffff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
cformat = '(i4,'//trim(cformat)//'f8.3)'
cformat = '(i4,'//trim(cformat)//'0f8.3)'
cformat = '(i4,'//trim(cformat)//'0ff8.3)'
cformat = '(i4,'//trim(cformat)//'fff8.3)'
cformat = '(i4,'//trim(cformat)//'ffff8.3)'
cformat = '(i4,'//trim(cformat)//'8.3)'
abc=4.5
def = 4.6
ghi =4.7