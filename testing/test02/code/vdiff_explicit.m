function [ field ] = vdiff_explicit( diff, old_field, numerator, denominator, dzt, dt )
%VDIFF_EXPLICIT apply vertical diffusivity to particulate tracers
% diff        = vector(1:kmax) of diffusivity [m2/s]
% old_field   = vector(1:kmax) of initial concentration [mol/kg]
% numerator   = vector(1:kmax): if only a part of the tracer shall move, the numerator of the moving fraction
% denominator = vector(1:kmax): if only a part of the tracer shall move, the denominator of the moving fraction
% dzt         = vector(1:kmax) of cell heights [m]
% dt          = timestep [s]
% field       = output vector(1:kmax) of final concentration [kg/m3]

    kmt   = size(old_field,1);
    field = old_field;
    
    ft1  = 0.0;                    % upward transport through upper boundary of the cell [mol*m/kg/s]
    for k = 1:kmt-1      
        kp1 = k+1;
        diffusivity = 0.5*diff(k)+0.5*diff(kp1);
        speed=diffusivity/(0.5*(dzt(k)+dzt(kp1))); % speed of exchange [m/s]
        mixed_height=speed*dt;                     % height of mixed water column which would be mixed if the gradient would remain the same
        % now limit it by 1/4 of the cell
        actual_mixed_height = (0.5*(dzt(k)+dzt(kp1)))*0.25*(1 - exp(- 4 * (mixed_height/ (0.5*(dzt(k)+dzt(kp1)))*0.25))); 
        actual_speed = actual_mixed_height/dt;
        
        if numerator(k) == denominator(k)
           ft2 = (max(field(kp1),0.0)-max(field(k),0.0))*actual_speed;  
                                    % upward transport through lower boundary of the cell [mol*m/kg/s]
        else
           ft2 = (max(field(kp1),0.0)*max(numerator(kp1),0.0)/max(denominator(kp1),1e-20) ... 
                 -max(field(k  ),0.0)*max(numerator(k  ),0.0)/max(denominator(k  ),1e-20)) ...
                      *actual_speed; 
                                    % upward transport through lower boundary of the cell [mol*m/kg/s]
        end
        % limit the downward flux by the amount which fits in 10% of the smaller of the two cells
        %ft2=max(ft2,-field(k)*(min(dzt(k),dzt(k+1))*0.1/dt));
        % limit the upward flux by the amount which fits in 10% of the smaller of the two cells
        %ft2=min(ft2,field(k+1)*(min(dzt(k),dzt(k+1))*0.1/dt));
        field(k) = field(k) - dt*(ft1-ft2)/dzt(k);  % change in the cell due to transports through lower and upper boundary [mol/kg]
        ft1 = ft2;
    end
    k = kmt;
    field(k) = field(k) - dt*ft1/dzt(k);
end

