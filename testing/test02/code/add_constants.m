% additional constants


% test in cgt_bio_timestep to initialize the grey N in NO3-
firstBioTimestep = false;


% test against current_date
% detail_date_start = datenum(1964,02,20);
detail_date_start = datenum(1964,07,02); % doy: 184 -> everything still ok
detail_date_stop = datenum(1964,07,03); % doy: 185 -> first negativ values in dia_with_green_N
% detail_date_stop = datenum(1964,02,21);


i_count_l = 1;
i_count_s = 1;

detail_change_of_no3_uncolored = zeros(kmax,max_output_index,100)/0.0;
detail_tracer_vector_no3_uncolored = zeros(kmax,max_output_index,100)/0.0;
detail_change_of_dia_uncolored = zeros(kmax,max_output_index,100)/0.0;
detail_tracer_vector_dia_uncolored = zeros(kmax,max_output_index,100)/0.0;
loop_count = zeros(kmax,max_output_index,24)/0.0;