#!/bin/bash
VERSION=`grep "const codegenVersion" ../src/cgt/erg_code.pas | awk -F"'" '/codegenVersion=/{print $2}'`
mkdir $VERSION
mkdir $VERSION/linux_executables
zip $VERSION/linux_executables/cgt_bin_linux64_$VERSION.zip ../src/cgt/cgt
zip $VERSION/linux_executables/cgt_bin_linux64_$VERSION.zip ../src/cgt_edit/cgt_edit
zip $VERSION/linux_executables/cgt_bin_linux64_$VERSION.zip ../README.txt
zip $VERSION/linux_executables/cgt_bin_linux64_$VERSION.zip ../LICENSE.txt
mkdir $VERSION/windows_executables
zip $VERSION/windows_executables/cgt_bin_windows_$VERSION.zip ../src/cgt/cgt.exe
zip $VERSION/windows_executables/cgt_bin_windows_$VERSION.zip ../src/cgt_edit/cgt_edit.exe
zip $VERSION/windows_executables/cgt_bin_windows_$VERSION.zip ../README.txt
zip $VERSION/windows_executables/cgt_bin_windows_$VERSION.zip ../LICENSE.txt
mkdir $VERSION/src
zip -r $VERSION/src/cgt_src_$VERSION.zip ../src
zip $VERSION/src/cgt_src_$VERSION.zip ../README.txt
zip $VERSION/src/cgt_src_$VERSION.zip ../LICENSE.txt
