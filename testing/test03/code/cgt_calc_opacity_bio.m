%-----------------------------------------------------------------
% fill the vector opacity_bio with the opacity caused by POM [1/m]
%-----------------------------------------------------------------
forcing_vector_opacity_bio = zeros(1,kmax);

% water column tracers
% calculate opacity contribution [1/m] as product of
% opacity [m2/mol] * concentration [mol/kg] * water density [kg/m3]
for k=1:kmax
end

% surface tracers (only in uppermost cell)
% calculate opacity contribution [1/m] as product of
% opacity [m2/mol] * concentration [mol/m2] / cell height [m]
