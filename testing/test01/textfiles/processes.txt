! BioCGT processes file
! *********************
! properties of processes:
! name=           fortran variable name for the rate
! description=    e.g. "grazing of zooplankton"
! turnover=       fortran formula for calculating the process turnover [mol/kg or mol/m2]
! equation=       equation which, like a chemical equation, lists reaction agents and products of this process.
!                   example: t_no3 + 1/16*t_po4 -> t_lpp
!                   tracers to the left of the "->" are consumed, tracers to the right of the "->" are produced by this process.
! feedingEfficiency= name of an auxiliary variable (values 0..1) which tells how much of the food in a certain depth is accessible for the predator with vertLoc=FIS. Relevant for vertLoc=FIS only. Default="1.0"
! isActive=       1=active (default); 0=process is switched off
! isOutput=       1=occurs as output in model results; 0=internal use only (default)
! limitation=     TYPE tracer > value else otherProcess
! limitation=     TYPE tracer < value else otherProcess
!                   TYPE = HARD (theta function), MM (Michaelis-Menten), MMQ (quadratic Michaelis-Menten), IV (Ivlev), IVQ (quadratic Ivlev), LIN (linear), TANH (tangens hyperbolicus)
!                   tracer = name of tracer that needs to be present
!                   value = value that needs to be exceeded, may also be a constant or auxiliary
!                   otherProcess = process that takes place instead if this process gets hampered by the availability of "tracer"
!                 several of these lines may exist, the order of them may be relevant for the rate of "otherProcess".
! processType=    type of process, e.g. "propagation", default="standard"
! repaint=        number n of repainting actions to be done by the process, default=0
!                 This line is followed by n lines of this kind:
!   <oldColor> <element> = <newColor>    e.g.: "all  N   = blue "
!                                              "blue P   = none "
!                                              "red  all = green"
! vertLoc=        WAT=z-dependent (default), SED=in the sediment only, SUR=in the surface only, FIS=fish-type behaviour
! comment=        comment, default=""
!
! Process rates are calculated in the given order.
! *************************************************************************************
name        = growth_dia
description = growth of diatoms
turnover    = growth_max_dia * dia * min(light_lim_dia,min(no3_lim_dia,po4_lim_dia))
equation    = 1.1875*h3oplus + 6.625*co2 + 6.4375*h2o + 1/16*po4 + no3 -> dia + 8.625*o2
repaint     = 4
  all O = green
  none N = green
  red N = darkgreen
  pink N = blue
***********************
name        = respiration_dia
description = respiration of diatoms
turnover    = dia * resp_rate_dia
equation    = 8.625*o2 + dia -> no3 + 1/16*po4 + 1.1875*h3oplus + 6.625*co2 + 6.4375*h2o
repaint     = 2
  green N = red
  darkgreen N = pink
***********************
name        = mortality_dia
description = mortality of diatoms
turnover    = mort_rate_dia * dia
equation    = dia -> det
***********************
name        = surf_flux_o2_down
description = downward oxygen flux through the surface
turnover    = piston_vel_oxy*oxysat*cgt_density
comment     = relaxation of oxygen concentration against saturation at surface using piston-\\velocity, \\downward oxygen flow
equation    =  -> o2
vertLoc     = SUR
***********************
name        = surf_flux_o2_up
description = upward oxygen flux through the surface
turnover    = piston_vel_oxy*o2*cgt_density
comment     = relaxation of oxygen concentration against saturation at surface using piston-\\velocity, upward oxygen flow
equation    = o2 -> 
vertLoc     = SUR
***********************
