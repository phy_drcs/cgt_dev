unit Unit8;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls;

type

  { TForm8 }

  TForm8 = class(TForm)
    TreeView1: TTreeView;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.lfm}

procedure TForm8.FormShow(Sender: TObject);
begin
  TreeView1.SetFocus;
end;

procedure TForm8.BitBtn1Click(Sender: TObject);
begin
  Form8.Tag:=1; //success
  Form8.Close;
end;

procedure TForm8.BitBtn2Click(Sender: TObject);
begin
  Form8.Tag:=0; //no success
  Form8.Close;
end;

end.
