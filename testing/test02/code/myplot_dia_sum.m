%function [ output_plot ] = myplot( input_name )
% plot 1d or 2d variables
figure
input_matrix=output_dia - (output_dia_with_green_N + output_dia_with_red_N + output_dia_with_pink_N + output_dia_with_darkgreen_N + output_dia_with_blue_N);

if size(input_matrix,1)==size(depths,1)
  if sum(mylevels<0)
    contourf(time_axis',depths',input_matrix);
  else
    contourf(time_axis',depths',input_matrix,mylevels);
  end
  shading flat;
  colorbar;
  set(gca,'YDir','reverse');
  set(gca,'xtick',time_axis);
  datetick('x','keeplimits');
  xlabel('time');
  ylabel('depth [m]');
  title('total N in det');
end

if size(input_matrix,1)==1
  plot(time_axis',input_matrix);
  datetick('x');
  xlabel('time');
  title('total N in det');
end