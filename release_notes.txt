

1.3.6:
  - for users:
   -- added <elements> loop to cgt, which is a sub-loop to the <tracers> loop 
        and can contain a <children> loop
   -- <timetendencies> and <limitations> can also be called from within
        <elements>
  - for programmers:
   -- introduced doxygen documentation
   -- added elementLoop and elementCondition functions to erg_code.pas to 
        provide the functionality for the <elements> loop
   -- modification in childLoop: when called as sub-loop of elementLoop
        only children of the tracer in the far outer loop and containing
        the element of the previous outer loop are considered

1.3.6.1:
  - for programmers:
   -- moved changes from cgt to cgt_edit
   -- bug-fixes in initErgElement and copyErgElement
   -- corrections in version numbers

1.3.7:
  - for users:
   -- new tracer attributes and tags: <tracerAbove> and <tracerBelow>
   -- new tags work in <tracers> and <children> loops
  - for programmers:
   -- update of the doxygen documentation
   -- modifications in erg_types.pas (TErgTracer class, initErgTracer function,
        copyErgTracer function)
   -- modifications in erg_color::SplitTracers()
   -- modifications in erg_code::TracerLoop()

1.3.7.1:
  - resolved an issue in fortwrap.pas: previously, 
      "read(unit,'(10f8.4)')" was converted to 
      "read(unit,'(10.f8.4_SUFFIX)')"

1.3.7.2:
  - resolved an issue in fortwrap.pas: previously, 
      "cformat = '(i4,'//trim(SOME_LETTER)//'8.3)'" was converted to 
      "...//'8.3_SUFFIX)'" 

1.3.7.3:
  - added release notes
  - for users: 
   -- new output tags of <celements> loop: <description>, <element> and <name>
   -- new output tags of <elements> loop: <description> (of tracer) and <elementDescription>
  - for programmers
   -- minor in erg_code.pas
   -- added a new test file: test.f90
   -- modified test02 project description

1.3.7.4:
   - for users:
    -- the conditions hasBelow, hasAbove, tracerBelow and tracerAbove in tracer loops did not work in 1.3.7.3; fixed
    -- tracerAbove and tracerBelow are colored during the coloring process of the tracer but in some situations they should not; in 1.3.7.3 these situations were not always recognized; fixed

1.3.8:
   - The user has new tags in tracer loops in order to generate attributes for CF-Conventions conformal netCDF files. These new tags are:
    -- <longname>
    -- <unit>
    -- <standardname>
     The values of longname and unit are set via the tracer attributes 'longname' and 'outputUnit', respectively, in the cgt textfiles. Standardname is generated from the 'longname' and two new attributes 'stdname_prefix' and 'stdname_suffix' (standardname = stdname_prefix + longname + stdname_suffix).
     The value of unit/outputUnit is not affected by the coloring of elements. The
value of longname is treated like name ('_with_COLOR_ELEMENT' is appended).

1.3.8.1:
   - for users:
    -- new conditions for tracer loops: 'color';
        usage example: <tracers atmosDep=1; color=green>
    -- new attributes for celements: longname_prefix and longname_suffix;
        reason: Previously, the longname of painted tracers was set to
         [longname_unpainted_tracer]_with_[COLOR]_[ELEMENT]. However, since
         the standardname (for netCDF access) is constructed with the longname
         and since 'nitrogen_with_green_N' is no CF-convention standardname
         the user might want to define the suffix of the longname of painted
         tracers. 'longname_prefix' was just added for completness.
   - bugfixes:
    -- in the absence of 'longname' it should be set to 'name'; this was not
        done previously

1.3.9:
   - for users:
    -- new output tags in <containingtracers> sub-loop of the <celements> loop 
        the print out information on colored tracers:
      --- longname: <ctLongname>
      --- tracerAbove: <ctAbove>
      --- tracerBelow: <ctBelow>
      --- description: <ctDescription>
      --- name (untrimmed): <ctName>
      --- trimName: <ctTrimName>
      --- unit: <ctUnit>
      --- standardname: <ctStandardname>
      --- childOf: <ctChildof>
    -- new condition in <celements> loop:
      --- color=COLOR
      --- element=ELEMENT
      --- atmosDep=ATMOSDEP
      --- riverDep=RIVERDEP
    -- new output tags in the non-loop environment:
      --- numb. of tracers with vertLoc=SED: <numSedTracers>, <numSedTracers+X>
            and <numSedTracers-X> (X an integer)
      --- numb. of tracers with vertLoc=SUR: <numSurTracers>, <numSurTracers+X> 
            and <numSurTracers-X> (X an integer)
      --- numb. of tracers with vertLoc=FIS: <numFisTracers>, <numFisTracers+X> 
            and <numFisTracers-X> (X an integer)
      --- numb. of tracers with atmosDep=1: <numAtmosDepTracers>, 
            <numAtmosDepTracers+X> and <numAtmosDepTracers-X> (X an integer)
      --- <numWatTracers> = <num3DTracers>
      --- X can be an arbitrary number
    -- new output tags in "tracers" loop environment (similar to non-loop env.)
      --- generalized <numFlat>, <num3D>, <numRiverDep>, <numMoving> and 
            <numTracers>
      --- new: <numWat> (=<num3D>), <numSed>, <numSur>, <numFis> and 
            <numAtmosDep>

   - for developers:
    -- new function: StringReplaceTagPlusMinus
      --- does the generalized +/-X  
      --- can be applied on each tag
      --- call: "s:=StringReplaceTagPlusMinus(s,'<TAG',myNum);"
    
1.3.10
   - for users:
    -- cgt_edit: the variable "cgt_diffusivity" is now highlighted in grey in 
                  the editor
    -- the tag <numTracers> was added/repaired and works again (outside of any
         loop); now this tag exists outside of any loop and in the <tracers>-
         loop; the tags for getting further tracer numbers (e.g. flat) are
         differently called outsite of loops and in the <tracer>-loop
    -- When a tracer is colored by adding a suffix ('_with_COLOR_ELEMENT'), the
        same suffix is appended to tracerAbove and tracerBelow. In a second 
        step - after all tracers are colored - it is tested whether the newly
        constructed tracerAbove and tracerBelow actually exist. If not, the
        two attributes are set to none.
       Previously, the attributes were set to the tracer's parent's values
        for tracerAbove and tracerBelow. However, this might lead to 
        inconsistencies if mass flows from tracers to tracerBelow. Then, too
        much mass flow flow into the parent's tracerBelow.

        
1.3.10.1
   - for users:
    -- the condition 'color/=' did not work in the <tracers ...> loop
    -- modification of colored elements are now properly saved in the linux
        version; in Windows it worked; the GUI is constructed differently in
        Windows and Linux

1.3.11.0
   - for users:
    -- new tags (outside of any loops)
        These tags print out the number of uncolored tracers in each category:
          <numChildOf=none>  (of all tracers)
          <numFlatChildOf=none>  (of flat tracers)
          <num3DChildOf=none>
          <numWatChildOf=none>
          <numSurChildOf=none>
          <numSedChildOf=none>
          <numFisChildOf=none>
        The output of these tags can also be incremented and decremented. As
        follows:
          <numXXX=none+Y>
          <numXXX=none-Y>
        The tag <numChildOf=none> accepts not only 'none' but also all tracer 
        names. E.g.:
          <numChildOf=dia>  (number of children whose parent is dia)
          <numChildOf=nit>  (number of children whose parent is nit)

1.3.12.0
   - for users:
    -- <numTracers [CONDITIONS][+|-N]>
        print out the number of tracers
         +N = increment by N
         -N = decrement by N
         CONDITIONS = conditions applicable to the <tracers ...> loop-tag
         several conditions need to be separated by ';'
         conditions and +|-N need to be separated by ';'
    -- <numProcesses [CONDITIONS][+|-N]>
        print out the number of processes
    -- <numAuxiliaries [CONDITIONS][+|-N]>
        print out the number of auxiliaries
    -- <numElements [CONDITIONS][+|-N]>
        print out the number of elements
    -- <numConstants [CONDITIONS][+|-N]>
        print out the number of constants
    -- <idxTracer [CONDITIONS][+|-N]>
        print out the index of a tracer (considering the given conditions)
        Example:
         We have three tracers:
          - dia (vertLoc=WAT)
          - no3 (vertLoc=WAT)
          - nit (vertLoc=SED)
         The code
          <tracers vertLoc=SED>
            index_<trimName> = <idxTracer>
          </tracers>
         prints out
            index_nit = 3

         In contrast, the code
          <tracers vertLoc=SED>
            index_<trimName> = <idxTracer verLoc=SED>
          </tracers>
         prints out
            index_nit = 1

         The code
          <tracers>
            index_<trimName> = <idxTracer vertLoc=SED>
          </tracers>
         prints out
            index_dia = 0
            index_no3 = 0    
   - for developers:
    -- bugfix in ConstantConditions()
    -- new functions:
         stringReplaceNumTracers(s: String)
         StringReplaceIndexTracers(s: String; jTracer: Integer) 
         stringReplaceNumConstants(s: String)
         stringReplaceNumElements(s: String)
         stringReplaceNumAuxiliaries(s: String)
         stringReplaceNumProcesses(s: String)

1.3.13.0
   - for users:
    -- removed tags from version 1.3.11.0
    -- old <numXXxTracers> tags are now deprecated but still work
    -- use <idxTracer loopConditions> to get the same conditions as
        in the <tracers> loop
   - for developers:
    -- new functions from version 1.3.11.0 were removed
    -- A dummy function dummyFunForDoxygen() was introduced. The Pascal
        code is converted into C code by a tool called pas2dox in order
        to create a Doxygen documentation. However, the pas2dox parser
        has some problems with variables defined on the scope of the
        erg_code Unit and "eats" the first succeeding function.
    -- Updated Test files (test_D.f90) and Documentation

1.3.14.0
   - for users:
    -- the <numTracer> tag now works again in the <tracers> loop; in the
        previous release, only <numTracers> worked
    -- the traditional <numXXX+1> did not always work properly and was fixed

1.3.15.0
   - for users:
    -- some changes from version 1.3.12.0 were reverted:
       --- in the '<numXXX' and '<idxXXX' tags, the +/-Y needs to be directly
            behind the numXXX: '<numXXX+Y CONDITIONS>'
    -- 'loopConditions' condition in <numTracer>-tag in <tracers>-loop works
        properly (support for 'loopConditions' was added in v1.3.13.0)
    -- A logical 'OR' is supported in all situations with conditions. It is
        case-insensitive.
        Examples:
          <tracers vertLoc=SED OR vertLoc=SUR>
            <name>
          </tracers>
          
          <numTracers vertLoc=SED OR vertLoc=SUR> == <numFlatTracers>
          
          <tracers childOf=none OR childOf=oxy>
            idx_<name> = <numTracer loopConditions> 
              ! 'loopConditions' is replaced by 'childOf=none OR childOf=oxy'
          </tracers>
   - for developers:
    -- several new functions and procedures:
       --- SplitOR(): split condition string into sub-conditions:
             String: 'abc OR def OR ghi' => TStringList: ['abc', 'def', 'ghi']
       --- JoinConditions(): join two conditions strings (add ';' if both <>'')
       --- MergeConditionPrefix(): prepend a conditions string to an existing 
             conditions string; if the latter consists of several OR-separated 
             sub-conditions, the new condition is prepended each sub-condition
       --- ReplaceSubCondition(): Replace a specified part of a condition 
             string by a new conditions string. If the newly inserted condition
             string contains an 'OR', the original conditions is duplicated to
             keep the correctness of the logical expression (=> it is not just
             find&replace!)
       --- NumTagPlusMinus(): read a '+/-'XYZ in the begining of a tag-string
       --- CheckTag(): search for first appearance of a tag in a string s and
             does some more stuff (=> see detailed doc of that function!)
       --- SubstituteNumTracersTag(): look for first occurance of a 
             '<numXXXTracers' tag in a string, replace it by '<numTracers' and
             print out the conditions for XXX.
       --- SubstituteIdxTracersTag(): same like SubstituteNumTracersTag but for
             '<numTracer' or '<idxTracer'
    -- strongly modified functions:
       --- StringReplaceNumTracers(): treatment of new 'OR' conditions; do the
             merging of actual conditions and conditions from '<numXXXTracers'
             properly; remove '+/-XYZ' treatment (shifted to NumTagPlusMinus())
       --- StringReplaceIndexTracers(): like StringReplaceNumTracers(); have a
             special tag called 'loopConditions', which inserts the conditions
             of the <tracers>-loop
    -- moderatly modified functions:
       --- deal with 'OR': 
             RateCondition(), ctCondition(), ConstantCondition(), 
             ElementCondition(), AuxiliaryCondition(), ProcessCondition(), 
             cElementCondition(), TracerCondition()
       --- deal with 'OR' and recognize in-/decrement (+/-X) in tag:
             StringReplaceNumConstants(), StringReplaceNumElements(),
             StringReplaceNumAuxiliaries(), StringReplaceNumProcesses()
       --- remove special treatment of some count and number tags for tracers
             (<idxXXX, <numXXX, <numXXXTracers): TracerLoop(), generateCode()
    -- functions/procedures not used anymore:
       --- StringReplaceTagPlusMinus()


1.4.0
   - many new features ...

1.4.1
   - for users:
    -- bug fix: splitting conditions by 'OR' did not work in 1.4.0

1.4.2
   - for users:
    -- new: <unit> tag in <constants> und <auxiliaries> loops
    -- new: <name> and <trimName> tag in <timeTendencies> loop
    -- new: 'revertSign=1' (or 0; 0 is default) condition in <timeTendency> 
              tag (switch '-' to '+' and the other way around)
    -- new: 'isProduct=0' and 'isEduct=0' (or 1; 1 is default) conditions
              in <timeTendencies> loop. If 'isProduct' is set to zero, no
              time tendencies of tracers, which are products of this process 
              are inserted. If 'isEduct' is set to zero, no time tendencies 
              of tracers, which are products of this process are inserted. If 
              a tracer is on both sides of the process (product and educt), 
              only its production time tendencies are printed if 'isEduct=0'. 
              If a tracer is on both sides of the process (product and educt),
              only its desctruction time tendencies are printed if
              'isProduct=0'.
   - for developers:
    -- some updated comments and parts of the documentation

1.4.5
   - for users:
    -- new: Added the option to use "custom tags" both in cgt_edit and
              correspondingly in the code templates (invent own keywords)
   - for developers:
    -- changed data type of tracers etc. from record to object, so they 
              need initialization now

1.4.7
   - for users
    -- new: option to run from the command line (run "cgt --help" to see 
              how it works
    -- fixed: Will not crash if a plus sign is given in front of the value
              of "contents" of a tracer
