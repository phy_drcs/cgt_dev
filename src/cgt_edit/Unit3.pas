unit Unit3;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TForm3 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    ListBox1: TListBox;
    Label3: TLabel;
    ListBox2: TListBox;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox2DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses Unit1;

{$R *.lfm}

procedure TForm3.ListBox1DblClick(Sender: TObject);
begin
  if pos(' ',ListBox1.Items[ListBox1.ItemIndex])>0 then
    GoToExpression(copy(ListBox1.Items[ListBox1.ItemIndex],1,pos(' ',ListBox1.Items[ListBox1.ItemIndex])-1))
  else
    GoToExpression(ListBox1.Items[ListBox1.ItemIndex]);
  Form3.Close;
end;

procedure TForm3.ListBox2DblClick(Sender: TObject);
begin
  if pos(' ',ListBox2.Items[ListBox2.ItemIndex])>0 then
    GoToExpression(copy(ListBox2.Items[ListBox2.ItemIndex],1,pos(' ',ListBox2.Items[ListBox2.ItemIndex])-1))
  else
    GoToExpression(ListBox2.Items[ListBox2.ItemIndex]);
  Form3.Close;
end;

end.
