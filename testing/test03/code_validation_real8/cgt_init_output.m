%-----------------------------
% initialize the output arrays
%-----------------------------

% auxiliary variables

% tracers
  % nitrate :
    output_no3             = zeros(kmax,max_output_index)/0.0; 
    output_vector_no3             = zeros(1,kmax);
  % phosphate :
    output_po4             = zeros(kmax,max_output_index)/0.0; 
    output_vector_po4             = zeros(1,kmax);
  % oxygen :
    output_o2              = zeros(kmax,max_output_index)/0.0; 
    output_vector_o2              = zeros(1,kmax);
  % diatoms :
    output_dia             = zeros(kmax,max_output_index)/0.0; 
    output_vector_dia             = zeros(1,kmax);
  % detritus :
    output_det             = zeros(kmax,max_output_index)/0.0; 
    output_vector_det             = zeros(1,kmax);
  % nitrate; containing  :
    output_no3_with_green_O = zeros(kmax,max_output_index)/0.0; 
    output_vector_no3_with_green_O = zeros(1,kmax);
  % phosphate; containing  :
    output_po4_with_green_O = zeros(kmax,max_output_index)/0.0; 
    output_vector_po4_with_green_O = zeros(1,kmax);
  % oxygen; containing  :
    output_o2_with_green_O = zeros(kmax,max_output_index)/0.0; 
    output_vector_o2_with_green_O = zeros(1,kmax);
  % diatoms; containing  :
    output_dia_with_green_O = zeros(kmax,max_output_index)/0.0; 
    output_vector_dia_with_green_O = zeros(1,kmax);
  % detritus; containing  :
    output_det_with_green_O = zeros(kmax,max_output_index)/0.0; 
    output_vector_det_with_green_O = zeros(1,kmax);
  % nitrate; containing nitrogen bound by diatomes :
    output_no3_with_green_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_no3_with_green_N = zeros(1,kmax);
  % diatoms; containing nitrogen bound by diatomes :
    output_dia_with_green_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_dia_with_green_N = zeros(1,kmax);
  % detritus; containing nitrogen bound by diatomes :
    output_det_with_green_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_det_with_green_N = zeros(1,kmax);
  % nitrate; containing nitrogen formerly bound by diatoms :
    output_no3_with_red_N  = zeros(kmax,max_output_index)/0.0; 
    output_vector_no3_with_red_N  = zeros(1,kmax);
  % diatoms; containing nitrogen formerly bound by diatoms :
    output_dia_with_red_N  = zeros(kmax,max_output_index)/0.0; 
    output_vector_dia_with_red_N  = zeros(1,kmax);
  % detritus; containing nitrogen formerly bound by diatoms :
    output_det_with_red_N  = zeros(kmax,max_output_index)/0.0; 
    output_vector_det_with_red_N  = zeros(1,kmax);
  % nitrate; containing nitrogen bound by diatoms a second time :
    output_no3_with_darkgreen_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_no3_with_darkgreen_N = zeros(1,kmax);
  % diatoms; containing nitrogen bound by diatoms a second time :
    output_dia_with_darkgreen_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_dia_with_darkgreen_N = zeros(1,kmax);
  % detritus; containing nitrogen bound by diatoms a second time :
    output_det_with_darkgreen_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_det_with_darkgreen_N = zeros(1,kmax);
  % nitrate; containing nitrogen formerly bound by diatoms a second time :
    output_no3_with_pink_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_no3_with_pink_N = zeros(1,kmax);
  % diatoms; containing nitrogen formerly bound by diatoms a second time :
    output_dia_with_pink_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_dia_with_pink_N = zeros(1,kmax);
  % detritus; containing nitrogen formerly bound by diatoms a second time :
    output_det_with_pink_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_det_with_pink_N = zeros(1,kmax);
  % nitrate; containing nitrogen bound by diatoms a third or more times :
    output_no3_with_blue_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_no3_with_blue_N = zeros(1,kmax);
  % diatoms; containing nitrogen bound by diatoms a third or more times :
    output_dia_with_blue_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_dia_with_blue_N = zeros(1,kmax);
  % detritus; containing nitrogen bound by diatoms a third or more times :
    output_det_with_blue_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_det_with_blue_N = zeros(1,kmax);
  % nitrate; containing  :
    output_no3_with_grey_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_no3_with_grey_N = zeros(1,kmax);
  % diatoms; containing  :
    output_dia_with_grey_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_dia_with_grey_N = zeros(1,kmax);
  % detritus; containing  :
    output_det_with_grey_N = zeros(kmax,max_output_index)/0.0; 
    output_vector_det_with_grey_N = zeros(1,kmax);
  % nitrogen in sediment :
    output_nitr            = zeros(1   ,max_output_index)/0.0;
    output_scalar_nitr            = 0.0;
  % nitrogen in sediment; containing nitrogen bound by diatomes :
    output_nitr_with_green_N = zeros(1   ,max_output_index)/0.0;
    output_scalar_nitr_with_green_N = 0.0;
  % nitrogen in sediment; containing nitrogen formerly bound by diatoms :
    output_nitr_with_red_N = zeros(1   ,max_output_index)/0.0;
    output_scalar_nitr_with_red_N = 0.0;
  % nitrogen in sediment; containing nitrogen bound by diatoms a second time :
    output_nitr_with_darkgreen_N = zeros(1   ,max_output_index)/0.0;
    output_scalar_nitr_with_darkgreen_N = 0.0;
  % nitrogen in sediment; containing nitrogen formerly bound by diatoms a second time :
    output_nitr_with_pink_N = zeros(1   ,max_output_index)/0.0;
    output_scalar_nitr_with_pink_N = 0.0;
  % nitrogen in sediment; containing nitrogen bound by diatoms a third or more times :
    output_nitr_with_blue_N = zeros(1   ,max_output_index)/0.0;
    output_scalar_nitr_with_blue_N = 0.0;
  % nitrogen in sediment; containing  :
    output_nitr_with_grey_N = zeros(1   ,max_output_index)/0.0;
    output_scalar_nitr_with_grey_N = 0.0;

% processes
