! for testing new tags in version 1.3.12.0


! +--------------------------------------------------------------------------+
! | TEST 1: numTracers Tag                                                   |
! +--------------------------------------------------------------------------+
Tag: < numTracers >
  IST-Wert:           <numTracers>
  SOLL-Wert (col):    50
  SOLL-Wert (un-col):  9

Tag: < numTracers childOf=NONE>
  IST-Wert:           <numTracers childOf=NONE>
  SOLL-Wert (col):     9
  SOLL-Wert (un-col):  9

Tag: < numTracers childOf=no3>
  IST-Wert:           <numTracers childOf=no3>
  SOLL-Wert (col):     7
  SOLL-Wert (un-col):  0

Tag: < numTracers+2 >
  IST-Wert:           <numTracers+2>
  SOLL-Wert (col):    52
  SOLL-Wert (un-col): 11

Tag: < numTracers vertLoc=sur >
  IST-Wert:           <numTracers vertLoc=sur>
  SOLL-Wert (col):     15
  SOLL-Wert (un-col):   3

Tag: < numTracers-1 vertLoc=sur >
  IST-Wert:           <numTracers-1 vertLoc=sur>
  SOLL-Wert (col):     14
  SOLL-Wert (un-col):   2

Tag: < numTracers vertLoc=wat >
  IST-Wert:           <numTracers vertLoc=WAT>
  SOLL-Wert (col):    28
  SOLL-Wert (un-col):  9

Tag: < numTracers vertLoc=wat; childOf=NONE >
  IST-Wert:           <numTracers vertLoc=WAT; childOf=NONE>
  SOLL-Wert (col):     5
  SOLL-Wert (un-col):  5

Tag: < numTracers vertLoc=sed > + < numTracers vertLoc=sur > 
  IST-Wert:           <numTracers vertLoc=sed> + <numTracers vertLoc=sur>
  SOLL-Wert (col):    7 + 15
  SOLL-Wert (un-col): 1 + 3


! +--------------------------------------------------------------------------+
! | TEST 2: Backward Compatibility                                           |
! +--------------------------------------------------------------------------+
Tag: < numFlatTracers >
  Old tag: <numFlatTracers>
  New tag: <numTracers vertLoc/=FIS; vertLoc/=WAT> 
  
Tag: < numFlatTracers+2 >
  Old tag: <numFlatTracers+2>
  New tag: <numTracers+2 vertLoc/=FIS; vertLoc/=WAT> 
  
Tag: < num3DTracers  >
  Old tag: <num3DTracers>
  New tag: <numTracers vertLoc=WAT> 
  
Tag: < numWatTracers  >
  Old tag: <numWatTracers>
  New tag: <numTracers vertLoc=WAT> 
  
Tag: < numSurTracers  >
  Old tag: <numSurTracers>
  New tag: <numTracers vertLoc=SUR> 
  
Tag: < numSedTracers  >
  Old tag: <numSedTracers>
  New tag: <numTracers vertLoc=SED> 
  
Tag: < numFisTracers  >
  Old tag: <numFisTracers>
  New tag: <numTracers vertLoc=FIS> 
  
Tag: < numRiverDepTracers  >
  Old tag: <numRiverDepTracers>
  New tag: <numTracers riverDep=1 > 
  
Tag: < numAtmosDepTracers  >
  Old tag: <numAtmosDepTracers>
  New tag: <numTracers atmosDep=1 > 
  
Tag: < numMovingTracers  >
  Old tag: <numMovingTracers>
  New tag: <numTracers vertSpeed/=0 > 
  
Tag: < numTracers+1 >
  IST-Wert:           <numTracers+1>
  SOLL-Wert (col):    51
  SOLL-Wert (un-col): 10
  



! +--------------------------------------------------------------------------+
! | TEST 3: idxTracers Tag                                                   |
! +--------------------------------------------------------------------------+
  po4            
  NAME            | iT | iT | iC | iNO3| iWAT| iWAT+1| iWAT | iT+1
<tracers>
  <name> |  <numTracers> |  <idxTracer> |  <idxTracer childOf=none> |   <idxTracer childOf=no3> |   <num3D> |   <num3D+1> |   <idxTracer vertLoc=WAT> |  <numTracer+1>
</tracers>


! +--------------------------------------------------------------------------+
! | TEST 4: idxTracers Tag with tracersConditions                            |
! +--------------------------------------------------------------------------+
  Name (vertLoc=WAT) | < idxTracer > | < idxTracer tracersConditions >
<tracers vertLoc=WAT>
  <name>    |       <idxTracer>       |      <idxTracer tracersConditions>
</tracers>


! +--------------------------------------------------------------------------+
! | TEST 5: numConstants Tag                                                 |
! +--------------------------------------------------------------------------+
Tag: < numConstants >
  IST-Wert:   <numConstants>
  SOLL-Wert:  8

Tag: < numConstants variation=0 >
  IST-Wert:   <numConstants variation=0>
  SOLL-Wert:  7


! +--------------------------------------------------------------------------+
! | TEST 6: numElements Tag                                                  |
! +--------------------------------------------------------------------------+
Tag: < numElements >
  IST-Wert:           <numElements>
  SOLL-Wert (col):    13
  SOLL-Wert (un-col):  6

Tag: < numElements hascoloredcopies=1 >
  IST-Wert:            <numElements hascoloredcopies=1>
  SOLL-Wert (col):     9
  SOLL-Wert (un-col):  0

Tag: < numElements+1 hascoloredcopies=1 >
  IST-Wert:            <numElements+1 hascoloredcopies=1>
  SOLL-Wert (col):     10
  SOLL-Wert (un-col):  1


! +--------------------------------------------------------------------------+
! | TEST 7: numAuxiliaries Tag                                               |
! +--------------------------------------------------------------------------+
Tag: < numAuxiliaries >
  IST-Wert:   <numAuxiliaries>
  SOLL-Wert:  7

Tag: < numAuxiliaries vertLoc=WAT >
  IST-Wert:   <numAuxiliaries vertLoc=WAT>
  SOLL-Wert:  5

Tag: < numAuxiliaries iterations/=0 >
  IST-Wert:   <numAuxiliaries iterations/=0>
  SOLL-Wert:  2

Tag: < numAuxiliaries+5 iterations/=0; vertLoc=SED >
  IST-Wert:   <numAuxiliaries+5 iterations/=0; vertLoc=SED>
  SOLL-Wert:  6



! +--------------------------------------------------------------------------+
! | TEST 8: numProcesses Tag                                                 |
! +--------------------------------------------------------------------------+
Tag: < numProcesses >
  IST-Wert:            <numProcesses>
  SOLL-Wert (col):     26
  SOLL-Wert (un-col):   5

Tag: < numProcesses vertLoc=WAT >
  IST-Wert:            <numProcesses vertLoc=WAT>
  SOLL-Wert (col):     23
  SOLL-Wert (un-col):   3

Tag: < numProcesses+9 vertLoc=SED >
  IST-Wert:            <numProcesses+9 vertLoc=SUR>
  SOLL-Wert (col):     12
  SOLL-Wert (un-col):  11


