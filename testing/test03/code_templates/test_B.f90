!~~~~~~~~~~~~~~ TEST A ~~~~~~~~~~~~~~
! only N
<celements element=N>
  <color>, <element>
  <containingtracers>
    <ct>
  </containingtracers>
  
</celements>

! only green
<celements color=green>
  <color>, <element>
  <containingtracers>
    <ct>
  </containingtracers>
  
</celements>

! only atmosDep
<celements atmosDep=1>
  <color>, <element>
  <containingtracers>
    <ct>
  </containingtracers>
  
</celements>

! only riverDep
<celements riverDep=1>
  <color>, <element>
  <containingtracers>
    <ct>
  </containingtracers>
  
</celements>

! all
<celements>
  <color>, <element>
  <containingtracers>
    <ct>
  </containingtracers>
  
</celements>


!~~~~~~~~~~~~~~ TEST B ~~~~~~~~~~~~~~
! only N
<celements element=N>
  <color>, <element>
  <containingtracers>
    ! <trimName>
    longname: <ctLongname>
    tracerAbove: <ctAbove>
    tracerBelow: <ctBelow>
    description: <ctDescription>
    name: <ctName>
    name (ct): <ct>
    trimName: <ctTrimName>
    unit: <ctUnit>
    standardname: <ctStandardname>
    childOf: <ctChildof>
    
  </containingtracers>
  
</celements>
