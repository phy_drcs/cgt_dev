#!/bin/bash
VERSION=`grep "const codegenVersion" ../src/cgt/erg_code.pas | awk -F"'" '/codegenVersion=/{print $2}'`
mkdir $VERSION
cp ../src/cgt/erg_*.pas ../src/cgt_edit/.
lazbuild ../src/cgt/cgt.lpi
lazbuild ../src/cgt_edit/cgt_edit.lpi
echo "Now build cgt and cgt_edit in Lazarus under Windows and then run step 2"
