%--------------------------------------------------------------------------
% save tracers, auxiliary variables and process rates to final output array
%--------------------------------------------------------------------------
% in the water column
for k=1:kmax
  <tracers isFlat=0; isOutput=1>
    output_<name>(k,current_output_index)=output_vector_<name>(k)/output_count;
  </tracers>
  <auxiliaries isFlat=0; isOutput=1>
    output_<name>(k,current_output_index)=output_vector_<name>(k)/output_count;
  </auxiliaries>
  <processes isFlat=0; isOutput=1>
    output_<name>(k,current_output_index)=output_vector_<name>(k)/output_count;
  </processes>
end
% in the sediment
<tracers isFlat=1; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</tracers>
<auxiliaries isFlat=1; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</auxiliaries>
<processes isFlat=1; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</processes>
% at the sea surface
<tracers isFlat=2; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</tracers>
<auxiliaries isFlat=2; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</auxiliaries>
<processes isFlat=2; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</processes>

% now, set the temporary output vector to zero
<tracers isFlat=0; isOutput=1>
  output_vector_<name> = zeros(1,kmax);
</tracers>
<auxiliaries isFlat=0; isOutput=1>
  output_vector_<name> = zeros(1,kmax);
</auxiliaries>
<processes isFlat=0; isOutput=1>
  output_vector_<name> = zeros(1,kmax);
</processes>
<tracers isFlat=1; isOutput=1>
  output_scalar_<name> = 0.0;        
</tracers>
<auxiliaries isFlat=1; isOutput=1>
  output_scalar_<name> = 0.0;        
</auxiliaries>
<processes isFlat=1; isOutput=1>
  output_scalar_<name> = 0.0;        
</processes>
<tracers isFlat=2; isOutput=1>
  output_scalar_<name> = 0.0;        
</tracers>
<auxiliaries isFlat=2; isOutput=1>
  output_scalar_<name> = 0.0;        
</auxiliaries>
<processes isFlat=2; isOutput=1>
  output_scalar_<name> = 0.0;        
</processes>