% NEW: calculate sum of all colored tracers
<tracers childof=none>
    % doing <trimName>
 <elements hascoloredcopies=1>
    sum_<trimName>_with_colored_<trimElement> = ...
  <children>
      tracer_vector_<childName> + ...
  </children>
      0.0
 </elements>
    
</tracers>
    
% NEW: reduce colored tracer concentraions if necessary    
    for k = 1:kmax 
<tracers childof=none>
 <elements hascoloredcopies=1>
        if(sum_<trimName>_with_colored_<trimElement>(k) > tracer_vector_<trimName>(k))
  <children>
            tracer_vector_<childName>(k) = tracer_vector_<trimName>(k) * tracer_vector_<childName>(k) / sum_<trimName>_with_colored_<trimElement>(k);
  </children>
            disp(strcat('corrected <trimName>: ', num2str(sum_<trimName>_with_colored_<trimElement>(k) - tracer_vector_<trimName>(k)), ', k=', num2str(k)))
        end
    
 </elements>
</tracers>
        
    % calculate total element concentrations in the water column
    for k = 1:kmax
          <celements>
             tracer_vector_<total>(k) = ...
             <containingTracers isFlat=0>
                max(0.0,tracer_vector_<ct>(k))*<ctAmount> + ...
             </containingTracers>
                0.0;
          </celements>   
    end

    % calculate total colored element concentrations at bottom
       <celements>
          tracer_scalar_<totalBottom> = ...
          <containingTracers isFlat=1>
             max(0.0,tracer_scalar_<ct>)*<ctAmount> + ...
          </containingTracers>
             0.0;
       </celements>    

    % Water column processes

    cgt_bottomdepth = 0.0;
    for k = 1:kmax
       cgt_bottomdepth = cgt_bottomdepth + cellheights(k);
          
             %------------------------------------
             % STEP 1: prepare abiotic parameters
             %------------------------------------
             cgt_temp       = forcing_vector_temperature(k);           % potential temperature     [Celsius]
             cgt_sali       = forcing_vector_salinity(k);              % salinity                  [g/kg]
             cgt_light      = forcing_vector_light(k);                 % light intensity           [W/m2]
             cgt_cellheight = cellheights(k);                          % cell height               [m]
             cgt_density    = density_water;                           % density                   [kg/m3]
             cgt_timestep   = timestep;                                % timestep                  [days]
             cgt_latitude   = location.latitude;                       % geographic latitude       [deg]
             if k == kmax 
                cgt_current_wave_stress=forcing_scalar_bottom_stress;  % bottom stress             [N/m2]
             end                     
             
             %------------------------------------
             % STEP 2: load tracer values
             %------------------------------------
<tracers isFlat=0>
             <name> = tracer_vector_<name>(k); % <description>
             if k == 1
                above_<name> = tracer_vector_<name>(k);
             else
                above_<name> = tracer_vector_<name>(k-1);
             end
</tracers>             
<auxiliaries isFlat=0; isUsedElsewhere=1>
             <name> = auxiliary_vector_<name>(k); % <description>
</auxiliaries>

<tracers isFlat=0; isPositive=1>
             <name>       = max(<name>,0.0);
             above_<name> = max(above_<name>,0.0);
</tracers>

             if k == kmax
<tracers isFlat=1>
                <name> = tracer_scalar_<name>; % <description>
</tracers>

<tracers isFlat=1; isPositive=1>
                <name> = max(<name>,0.0);
</tracers>
             end

             %------------------------------------
             % STEP 4.1: calculate auxiliaries
             %------------------------------------
<auxiliaries isFlat=0; calcAfterProcesses=0; isZGradient=1>
             % <description> :
             <name> = (above_<formula>-<formula>)/depths(k);

</auxiliaries>
<auxiliaries isFlat=0; calcAfterProcesses=0; isZGradient=0>
             % <description> :
             temp1  = <temp1>;
             temp2  = <temp2>;
             temp3  = <temp3>;
             temp4  = <temp4>;
             temp5  = <temp5>;
             temp6  = <temp6>;
             temp7  = <temp7>;
             temp8  = <temp8>;
             temp9  = <temp9>;
             <name> = <formula>;
             
</auxiliaries>

             if k == kmax
<auxiliaries isFlat=1; calcAfterProcesses=0>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;
                <name> = <formula> ;
                
</auxiliaries>
             end
             
             if k == 1
<auxiliaries isFlat=2; calcAfterProcesses=0>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;                
                <name> = <formula> ;
                
</auxiliaries>
             end
             
             %------------------------------------
             % STEP 4.2: output of auxiliaries
             %------------------------------------
<auxiliaries isFlat=0; calcAfterProcesses=0; isOutput=1>             
             output_vector_<name>(k) = output_vector_<name>(k) + <name>;
</auxiliaries>

             if k == kmax
<auxiliaries isFlat=1; calcAfterProcesses=0; isOutput=1>
                   output_scalar_<name> = output_scalar_<name> + <name>;
</auxiliaries>
             end
             if k == 1
<auxiliaries isFlat=2; calcAfterProcesses=0; isOutput=1>
                   output_scalar_<name> = output_scalar_<name> + <name>;
</auxiliaries>
             end

             %------------------------------------
             % STEP 5: calculate process limitations
             %------------------------------------

<tracers isFlat=0>
  <limitations>
             <name> = <formula>;
  </limitations>
</tracers>

             if k == kmax
<tracers isFlat=1>
  <limitations>
                <name> = <formula>;
  </limitations>
</tracers>
             end

             if k == 1
<tracers isFlat=2>
  <limitations>
                <name> = <formula>;
  </limitations>
</tracers>
             end

             %------------------------------------
             %-- POSITIVE-DEFINITE SCHEME --------
             %-- means the following steps will be repeated as often as nessecary
             %------------------------------------

             fraction_of_total_timestep = 1.0;   % how much of the original timestep is remaining
<processes>
             total_rate_<name>          = 0.0;
</processes>
             number_of_loop = 1;

             while cgt_timestep > 0.0

                %------------------------------------
                % STEP 6.1: calculate process rates
                %------------------------------------
<processes isFlat=0>
                % <description> :
                <name> = <turnover>;
                <name> = max(<name>,0.0);

</processes>

                if k == kmax
<processes isFlat=1>
                   % <description> :
                   <name> = <turnover>;
                   <name> = max(<name>,0.0);
                
</processes>
                end
             
                if k == 1
<processes isFlat=2>
                   % <description> :
                   <name> = <turnover>;
                   <name> = max(<name>,0.0);
                
</processes>
                end

                %------------------------------------
                % STEP 6.2: calculate possible euler-forward change (in a full timestep)
                %------------------------------------

<tracers>
                change_of_<name> = 0.0;
</tracers>

<tracers isFlat=0; hasRatesFlat=0>
             
                change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                <timeTendencies isFlat=0>
                   <timeTendency> ... % <description>
                </timeTendencies>
                );
</tracers>

                if k == 1
<tracers isFlat=0; hasRatesFlat=2>

                   change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                   <timeTendencies isFlat=2>
                      <timeTendency> ... % <description>
                   </timeTendencies>
                   );
</tracers>
                end

                if k == kmax
<tracers isFlat=0; hasRatesFlat=1>

                   change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                   <timeTendencies isFlat=1>
                      <timeTendency> ... % <description>
                   </timeTendencies>
                   );
</tracers>
<tracers isFlat=1; hasRates>

                   change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                   <timeTendencies>
                      <timeTendency> ... % <description>
                   </timeTendencies>
                   );
</tracers>                         
                end           

                %------------------------------------
                % STEP 6.2.5: diagnostic output by Daniel Neumann
                %------------------------------------
                tracer_vector_no3_uncolored = tracer_vector_no3(k) - tracer_vector_no3_with_red_N(k) - tracer_vector_no3_with_pink_N(k) - tracer_vector_no3_with_blue_N(k);
                change_of_no3_uncolored = change_of_no3 - change_of_no3_with_red_N - change_of_no3_with_pink_N - change_of_no3_with_blue_N;
                tracer_vector_dia_uncolored = tracer_vector_dia(k) - tracer_vector_dia_with_green_N(k) - tracer_vector_dia_with_darkgreen_N(k) - tracer_vector_dia_with_blue_N(k);
                change_of_dia_uncolored = change_of_dia - change_of_dia_with_green_N - change_of_dia_with_darkgreen_N - change_of_dia_with_blue_N;
                
                if ( (k == 1) && (current_date >= detail_date_start) && (current_date <= detail_date_stop) )
                    disp('~~~~~~~~~~~ DIAGNOSTIC OUTPUT ~~~~~~~~~~~')
                    disp(strcat(' Date: ', datestr(current_date), ', timestep left: ', num2str(fraction_of_total_timestep), ', number_of_loop', num2str(number_of_loop), ', cgt_timestep:', num2str(cgt_timestep)))
%~ <tracers>
                    %~ tracer_name = strcat('<name>', '_with_noColor');
                    %~ if( (strcmp(tracer_name(1:3), 'no3') && (strcmp(tracer_name(10:12), 'noC') || strcmp(tracer_name(10:12), 'red') || strcmp(tracer_name(10:12), 'pin') || strcmp(tracer_name(10:12), 'blu'))) ...
                     %~ || (strcmp(tracer_name(1:3), 'dia') && (strcmp(tracer_name(10:12), 'noC') || strcmp(tracer_name(10:13), 'gree') || strcmp(tracer_name(10:12), 'dar') || strcmp(tracer_name(10:12), 'blu'))) )
                        %~ disp(' tracer: <trimName>')
                        %~ disp(strcat('  change_of_*: ', num2str(change_of_<trimName>(1))))
                        %~ disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_<name>(k))))
                    %~ end
%~ </tracers>
                    disp(' tracer: uncolored no3')
                    disp(strcat('  change_of_*: ', num2str(change_of_no3_uncolored)))
                    disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_no3_uncolored)))
                    disp(' tracer: uncolored dia')
                    disp(strcat('  change_of_*: ', num2str(change_of_dia_uncolored)))
                    disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_dia_uncolored)))
                end
                
                %------------------------------------
                % STEP 6.3: calculate maximum fraction of the timestep before some tracer gets exhausted
                %------------------------------------

                timestep_fraction = 1.0;
                which_tracer_exhausted = -1;

                % find the tracer which is exhausted after the shortest period of time

                % in the water column
<tracers vertLoc=WAT>
             
                % check if tracer <name> was exhausted from the beginning and is still consumed
                if (tracer_vector_<name>(k) <= 0.0) && (change_of_<name> < 0.0)
                   disp(strcat('~~ exhaused but consumded: <name>, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = <numTracer>;
                end
                % check if tracer <name> was present, but got exhausted
                if (tracer_vector_<name>(k) > 0.0) && (tracer_vector_<name>(k) + change_of_<name> < 0.0)
                   timestep_fraction_new = tracer_vector_<name>(k) / (0.0 - change_of_<name>);
                   disp(strcat('~~ present but got exhausted: <name>, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = <numTracer>;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
</tracers>
                
                if (k == 1) 
                    %~ disp(' tracer: uncolored no3')
                    %~ disp(strcat('  change_of_*: ', num2str(change_of_no3_uncolored)))
                    %~ disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_no3_uncolored)))
                    %~ disp(' tracer: uncolored dia')
                    %~ disp(strcat('  change_of_*: ', num2str(change_of_dia_uncolored)))
                    %~ disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_dia_uncolored)))
                    
                     detail_change_of_no3_uncolored(k,current_output_index,i_count_l) = change_of_no3_uncolored;
                     detail_tracer_vector_no3_uncolored(k,current_output_index,i_count_l) = tracer_vector_no3_uncolored;
                     detail_change_of_dia_uncolored(k,current_output_index,i_count_l) = change_of_dia_uncolored;
                     detail_tracer_vector_dia_uncolored(k,current_output_index,i_count_l) = change_of_dia_uncolored;
                     i_count_l = i_count_l + 1;
                end
                
                %~ % check if tracer <name> was exhausted from the beginning and is still consumed
                %~ if (tracer_vector_no3_uncolored <= 0.0) && (change_of_no3_uncolored < 0.0)
                   %~ disp(strcat('~~ exhaused but consumded: uncolored NO3-, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   %~ timestep_fraction = 0.0;
                %~ end
                %~ % check if tracer <name> was present, but got exhausted
                %~ if (tracer_vector_no3_uncolored > 0.0) && (tracer_vector_no3_uncolored + change_of_no3_uncolored < 0.0)
                   %~ timestep_fraction_new = tracer_vector_no3_uncolored / (0.0 - change_of_no3_uncolored);
                   %~ disp(strcat('~~ present but got exhausted: uncolored NO3-, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   %~ if timestep_fraction_new <= timestep_fraction
                      %~ disp('~~~ really exhausted')
                      %~ timestep_fraction = timestep_fraction_new;
                   %~ end
                %~ end
                
                %~ % check if tracer <name> was exhausted from the beginning and is still consumed
                %~ if (tracer_vector_dia_uncolored <= 0.0) && (change_of_dia_uncolored < 0.0)
                   %~ disp(strcat('~~ exhaused but consumded: uncolored dia, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   %~ timestep_fraction = 0.0;
                %~ end
                %~ % check if tracer <name> was present, but got exhausted
                %~ if (tracer_vector_dia_uncolored > 0.0) && (tracer_vector_dia_uncolored + change_of_dia_uncolored < 0.0)
                   %~ timestep_fraction_new = tracer_vector_dia_uncolored / (0.0 - change_of_dia_uncolored);
                   %~ disp(strcat('~~ present but got exhausted: uncolored dia, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   %~ if timestep_fraction_new <= timestep_fraction
                      %~ disp('~~~ really exhausted')
                      %~ timestep_fraction = timestep_fraction_new;
                   %~ end
                %~ end
          
                % in the bottom layer
                if k == kmax
<tracers vertLoc=SED; isPositive=1>

                   % check if tracer <name> was exhausted from the beginning and is still consumed
                   if (tracer_scalar_<name> <= 0.0) && (change_of_<name> < 0.0)
                      timestep_fraction = 0.0;
                      which_tracer_exhausted = <numTracer>;
                   end
                   % check if tracer <name> was present, but got exhausted
                   if (tracer_scalar_<name> > 0.0) && (tracer_scalar_<name> + change_of_<name> < 0.0)
                      timestep_fraction_new = tracer_scalar_<name> / (0.0 - change_of_<name>);
                      if timestep_fraction_new <= timestep_fraction
                         which_tracer_exhausted = <numTracer>;
                         timestep_fraction = timestep_fraction_new;
                      end
                   end
</tracers>                         
                end          

                % now, update the limitations: rates of the processes limited by this tracer become zero in the future

<tracers isPositive=1>
                if <numTracer> == which_tracer_exhausted
                  <limitations>
                   <name> = 0.0;
                  </limitations>
                end
</tracers>

                %------------------------------------
                % STEP 6.4: apply a Euler-forward timestep with the fraction of the time
                %------------------------------------ 

                % in the water column
<tracers isFlat=0>
             
                % tracer <name> (<description>):
                tracer_vector_<name>(k) = tracer_vector_<name>(k) + change_of_<name> * timestep_fraction;
</tracers>
          
                % in the bottom layer
                if k == kmax
<tracers isFlat=1>

                   % tracer <name> (<description>)
                   tracer_scalar_<name> = tracer_scalar_<name> + change_of_<name> * timestep_fraction;
</tracers>                         
                end           

                %------------------------------------
                % STEP 6.5: output of process rates
                %------------------------------------
<processes isFlat=0; isOutput=1>             
                output_vector_<name>(k) = output_vector_<name>(k) + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                if k == kmax
<processes isFlat=1; isOutput=1>
                   output_scalar_<name> = output_scalar_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                end
                if k == 1
<processes isFlat=2; isOutput=1>
                   output_scalar_<name> = output_scalar_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                end
             
                %------------------------------------
                % STEP 6.6: set timestep to remaining timestep only
                %------------------------------------

                cgt_timestep = cgt_timestep * (1.0 - timestep_fraction);                         % remaining timestep
                fraction_of_total_timestep = fraction_of_total_timestep * (1.0 - timestep_fraction); % how much of the original timestep is remaining


                if number_of_loop > 100
                   error('aborted positive-definite scheme: more than 100 iterations');
                end
                number_of_loop=number_of_loop+1;

             end
             
             loop_count(k,current_output_index,i_count_s) = number_of_loop;
             i_count_s = i_count_s + 1;
             %------------------------------------
             %-- END OF POSITIVE-DEFINITE SCHEME -
             %------------------------------------  
             
             %------------------------------------
             % STEP 7.1: output of new tracer concentrations
             %------------------------------------
<tracers isFlat=0; isOutput=1>
             output_vector_<name>(k) = output_vector_<name>(k) + <name>;
</tracers>
             if k == kmax
<tracers isFlat=1; isOutput=1>
               output_scalar_<name> = output_scalar_<name> + <name>;
</tracers>
             end
             if k==1
<tracers isFlat=2; isOutput=1>
               output_scalar_<name> = output_scalar_<name> + <name>;
</tracers>
             end
             
             %------------------------------------
             % STEP 7.2: calculate "late" auxiliaries
             %------------------------------------
<auxiliaries isFlat=0; calcAfterProcesses=1>
             % <description> :
             temp1  = <temp1>;
             temp2  = <temp2>;
             temp3  = <temp3>;
             temp4  = <temp4>;
             temp5  = <temp5>;
             temp6  = <temp6>;
             temp7  = <temp7>;
             temp8  = <temp8>;
             temp9  = <temp9>;
             <name> = <formula>;
             
</auxiliaries>

             if k == kmax
<auxiliaries isFlat=1; calcAfterProcesses=1>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;
                <name> = <formula>;
                
</auxiliaries>
             end
             
             if k == 1
<auxiliaries isFlat=2; calcAfterProcesses=1>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;
                <name> = <formula>;
                
</auxiliaries>
             end
             
             %------------------------------------
             % STEP 7.3: output of "late" auxiliaries
             %------------------------------------
<auxiliaries isFlat=0; calcAfterProcesses=1; isOutput=1>             
             output_vector_<name>(k) = output_vector_<name>(k) + <name>;
</auxiliaries>
<auxiliaries isFlat=0; calcAfterProcesses=1; isUsedElsewhere=1>             
             auxiliary_vector_<name>(k) = <name>;
</auxiliaries>
             if k == kmax
<auxiliaries isFlat=1; calcAfterProcesses=1; isOutput=1>
                output_scalar_<name> = output_scalar_<name> + <name>;
</auxiliaries>
<auxiliaries isFlat=0; calcAfterProcesses=1; isUsedElsewhere=1>             
             auxiliary_scalar_<name> = <name>;
</auxiliaries>
             end
             if k == 1
<auxiliaries isFlat=2; calcAfterProcesses=1; isOutput=1>
                output_scalar_<name> = output_scalar_<name> + <name>;
</auxiliaries>
<auxiliaries isFlat=2; calcAfterProcesses=1; isUsedElsewhere=1>             
             auxiliary_scalar_<name> = <name>;
</auxiliaries>
             end

             %---------------------------------------
             % STEP 7.4: passing vertical velocity and diffusivity to the coupler
             %---------------------------------------

             % EXPLICIT MOVEMENT
             <tracers isFlat=0; vertSpeed/=0>
                vertical_speed_of_<name>(k)=(<vertSpeed>)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_<name>(k)=(<vertDiff>);        % leave as m2/s
             </tracers> 
    end
    
    %---------------------------------------
    % biological timestep has ended
    %---------------------------------------
    
    %---------------------------------------
    % vertical movement follows
    %---------------------------------------
    
    % calculate new total marked element concentrations
    for k = 1:kmax
          <celements>
             tracer_vector_<total>(k) = ...
             <containingTracers isFlat=0>
                max(0.0,tracer_vector_<ct>(k))*<ctAmount> + ...
             </containingTracers>
                0.0;
          </celements>
    end
    
    % vertical movement of tracers
    for m = 1:num_vmove_steps
       % first, move the age concentration of marked elements
       <tracers childOf/=none; isFlat=0; vertSpeed/=0; hasCeAged>
          tracer_vector_<ceAgedName> = vmove_explicit(vertical_speed_of_<name>, ...
                         tracer_vector_<ceAgedName>, ...
                         tracer_vector_<name>, tracer_vector_<ceTotalName>, ...
                         cellheights, timestep/num_vmove_steps*(24*3600));

       </tracers>
       % second, move the tracers (including marked tracers) themselves
       <tracers isFlat=0; vertSpeed/=0>
          tracer_vector_<name> = vmove_explicit(vertical_speed_of_<name>, ...
                                   tracer_vector_<name>, ...
                                   tracer_vector_<name>, tracer_vector_<name>, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
       </tracers>
       % third, calculate new total marked element concentrations
       for k = 1:kmax
             <celements>
                tracer_vector_<total>(k) = ...
                <containingTracers isFlat=0>
                   max(0.0,tracer_vector_<ct>(k))*<ctAmount> + ...
                </containingTracers>
                   0.0;
             </celements>       
       end
    end
    % vertical diffusion of tracers
    for m = 1:num_vmove_steps
       % first, diffuse the age concentration of marked elements
       <tracers childOf/=none; isFlat=0; vertSpeed/=0; hasCeAged>
          tracer_vector_<ceAgedName> = vdiff_explicit(vertical_diffusivity_of_<name>, ...
                                   tracer_vector_<ceAgedName>, ...
                                   tracer_vector_<name>, tracer_vector_<ceTotalName>, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));

       </tracers>
       % second, diffuse the tracers (including marked tracers) themselves
       <tracers isFlat=0; vertSpeed/=0>
          tracer_vector_<name> = vdiff_explicit(vertical_diffusivity_of_<name>, ...
                                   tracer_vector_<name>, ...
                                   tracer_vector_<name>, tracer_vector_<name>, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
       </tracers>
       % third, calculate new total marked element concentrations
       for k = 1:kmax
             <celements>
                tracer_vector_<total>(k) = ...
                <containingTracers isFlat=0>
                   max(0.0,tracer_vector_<ct>(k))*<ctAmount> + ...
                </containingTracers>
                   0.0;
             </celements>       
       end
    end

    % calculate total colored element concentrations at bottom
    <celements>
       tracer_scalar_<totalBottom> = ...
       <containingTracers isFlat=1>
          max(0.0,tracer_scalar_<name>)*<ctAmount> + ...
       </containingTracers>
          0.0;
    </celements>
