! For testing the new tags of versions 1.3.11.0 and 1.3.11.1

! NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE !
!                                                                            !
! NOTE        WORKS ONLY WITH VERSION 1.3.11 but not with 1.3.12        NOTE !
!                                                                            !
! NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE !

1<numTracers>1
1<numTracers childOf=none>1
1<numTracers childOf=no3>1
  1<numTracers +2>1
  1<numTracers vertLoc=sur>1
1<numTracers childOf=none;vertLoc=sur>1
1<numTracers childOf=none;vertLoc=sur;+3>1
1<numTracers childOf=none;vertLoc=sur;-5>1


numb. uncolored tracers:
 tag 'numChildOf=none': <numChildOf=none>
 result no colors:      9
 result with colors:    9
 
numb. uncolored tracers +2:
 tag 'numChildOf=none+2': <numChildOf=none+2>
 result no colors:        11
 result with colors:      11
  
should not work:
 tag 'numChildOf=abc': <numChildOf=abc>
 result no colors:     <numChildOf=abc> (not working)
 result with colors:   <numChildOf=abc> (not working)

numb. child-tracers of dia:
 tag 'numChildOf=dia': <numChildOf=dia>
 result no colors:      0
 result with colors:    7
  
numb. sediment uncolored tracers:
 tag 'numSedChildOf=none': <numSedChildOf=none>
 result no colors:         1
 result with colors:       1
  <numSedChildOf=none>

numb. water uncolored tracers:
 tag 'numWatChildOf=none': <numWatChildOf=none>
 result no colors:      5
 result with colors:    5

numb. surf uncolored tracers:
 tag 'numSurChildOf=none': <numSurChildOf=none>
 result no colors:      3
 result with colors:    3


  po4            
  NAME            | nT | nF | nC 
<tracers>
  <name> |  <numTracers> |  <numFlat> |  <numChildOf=none>
</tracers>