! Number of vars
numTracers:
  SOLL (no col): 9
  SOLL (wi col): 50
  IST:  <numTracers>
numFlatTracers:
  SOLL (no col): 4
  SOLL (wi col): 22
  IST:  <numFlatTracers>
num3DTracers:
  SOLL (no col): 5
  SOLL (wi col): 28
  IST:  <num3DTracers>
numRiverDepTracers:
  SOLL (no col): 5
  SOLL (wi col): 5
  IST:  <numRiverDepTracers>
numMovingTracers:
  SOLL (no col): 2
  SOLL (wi col): 16
  IST:  <numMovingTracers>
numSurTracers:
  SOLL (no col): 3
  SOLL (wi col): 15
  IST:  <numSurTracers>
  SOLL: SOLL+1
  IST:  <numSurTracers+1>
  SOLL: SOLL+11
  IST:  <numSurTracers+11>
  SOLL: SOLL+111
  IST:  <numSurTracers+111>
  SOLL: SOLL-22
  IST:  <numSurTracers-22>
  SOLL: SOLL
  IST:  <numSurTracers+0>
  SOLL: SOL
  IST:  <numSurTracers-0>
numSedTracers:
  SOLL (no col): 1
  SOLL (wi col): 1
  IST:  <numSedTracers>
numAtmosDepTracers:
  SOLL (no col): 5
  SOLL (wi col): 5
  IST:  <numAtmosDepTracers>

! loop celements and elents with their descriptions
<celements>
    <element>, <color>: <description>
</celements>

<tracers>
  <elements>
    <element>, <trimName>:
                    - <elementDescription> 
                    - <description> 
  </elements>
</tracers>



! CF-Convention tests for netCDF attributes
<tracers>
  <numTracers>
  ! <name>
    <trimName>.description='<description>'
    <trimName>.unit='<unit>'
    <trimName>.longname='<longname>'
    <trimName>.standardname='<standardname>'
</tracers>



! only uncolored tracers
<tracers childOf=none>
  <name>
</tracers>



! only uncolored tracers in the water
<tracers childOf=none; vertLoc=WAT>
  <name>
</tracers>



! all tracers in the sediment
<tracers vertLoc=SED>
  <name>
</tracers>


! only green painted tracers
<tracers color=green>
  <name>
</tracers>


! test tracerAbove and tracerBelow feature
<tracers tracerAbove/=none; vertLoc=SED>
  <trimName> is converted to <tracerAbove> when it is resuspended from the sediment into the water column.
</tracers>

<tracers tracerBelow/=none; vertLoc=WAT>
  <trimName> is converted to <tracerBelow> when it deposites from the water phase to the sediment.
</tracers>



! Testing of real extension (64bit reals = 8 byte = '_8')
read(lunamm,'(10f8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(10g8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(10a8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(f8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(ff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(fff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
read(lunamm,'(ffff8.4)',iostat=ios) ((srfflx_in(i,j), i=1,mmx), j=1,nmx)
cformat = '(i4,'//trim(cformat)//'f8.3)'
cformat = '(i4,'//trim(cformat)//'0f8.3)'
cformat = '(i4,'//trim(cformat)//'0ff8.3)'
cformat = '(i4,'//trim(cformat)//'fff8.3)'
cformat = '(i4,'//trim(cformat)//'ffff8.3)'
cformat = '(i4,'//trim(cformat)//'8.3)'
abc=4.5
def = 4.6
ghi =4.7



! uncolored tracers and their children
<tracers childOf=none>
  ! I am <name> and my children are
  <children>
  !  <childName>
  </children>
</tracers>


! uncolored tracers and their green OR grey children
<tracers childOf=none>
  ! I am <name> and my green or grey children are
  <children color=green OR color=grey>
  !  <childName>
  </children>
</tracers>


! uncolored tracers and their green N children
<tracers childOf=none>
  ! I am <name> and my green N children are
  <children contents=green_N>
  !  <childName>
  </children>
</tracers>


! test 'contents' condition
<tracers contents=O>
  ! I am <name> and I contain 'O'.
</tracers>



