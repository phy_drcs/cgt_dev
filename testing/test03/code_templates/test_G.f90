! normal
<tracers name=dia OR name=no3>
 <timeTendencies>
  <trimname>: <timeTendency>
 </timeTendencies>
</tracers>

! no invertSign
<tracers name=dia OR name=no3>
 <timeTendencies>
  <trimname>: <timeTendency invertSign=0>
 </timeTendencies>
</tracers>

! inverted sign
<tracers name=dia OR name=no3>
 <timeTendencies>
  <trimname>: <timeTendency invertSign=1>
 </timeTendencies>
</tracers>

! only LHS with positive sign
<tracers name=dia OR name=no3>
 <timeTendencies isEduct=1; isProduct=0>
  <trimname>: <timeTendency invertSign>
 </timeTendencies>
</tracers>

! only RHS
<tracers name=dia OR name=no3>
 <timeTendencies isEduct=0; isProduct=1>
  <trimname>: <timeTendency>
 </timeTendencies>
</tracers>

! none 1
<tracers name=dia OR name=no3>
 <timeTendencies isEduct=0; isProduct=0>
  <trimname>: <timeTendency>
 </timeTendencies>
</tracers>

! none 2
<tracers name=dia OR name=no3>
 <timeTendencies isEduct/=1; isProduct/=1>
  <trimname>: <timeTendency>
 </timeTendencies>
</tracers>

! default 1
<tracers name=dia OR name=no3>
 <timeTendencies isEduct=1>
  <trimname>: <timeTendency>
 </timeTendencies>
</tracers>

! default 2
<tracers name=dia OR name=no3>
 <timeTendencies isProduct=1>
  <trimname>: <timeTendency>
 </timeTendencies>
</tracers>