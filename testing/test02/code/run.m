%----------------------------------------------
% MATLAB 1-d model for testing ecosystem models
% hagen.radtke@io-warnemuende.de
%----------------------------------------------
global time_axis
global depths

disp('initialization');

%load timestep, initial date etc.
configure;

%initialize the date
current_date        =start_date;
current_output_date =start_date;
current_output_index=1; % time index in output matrix
max_output_index    =floor(repeated_runs*(end_date-start_date)/output_interval);
time_axis           =zeros(1,max_output_index);
for i=1:max_output_index
    time_axis(i)=(i-1)*output_interval+start_date;
end

disp('  loading physical forcing');

%load cell heights
cellheights = load('physics/cellheights.txt'); % cell heights [m]
depths      = cumsum(cellheights);             % bottom depths of cells [m]
kmax        = size(cellheights,1);             % number of vertical layers

add_constants;

%load physics
forcing_matrix_temperature   = load('physics/temperature.txt');  % temperature [deg_C]
forcing_matrix_salinity      = load('physics/salinity.txt');     % salinity [g/kg]
forcing_matrix_light_at_top  = load('physics/light_at_top.txt'); % downward flux of 
                                                                 % shortwave light at sea surface [W/m2]
forcing_matrix_bottom_stress = load('physics/bottom_stress.txt');% bottom stress [N/m2]                                                              
forcing_matrix_opacity_water = load('physics/opacity_water.txt');% clear-water opacity [1/m]
forcing_matrix_diffusivity   = load('physics/diffusivity.txt');  % turbulent vertical diffusivity [m2/s]

forcing_index_temperature = 1;
forcing_index_salinity = 1;
forcing_index_light_at_top = 1;
forcing_index_bottom_stress = 1;
forcing_index_opacity_water = 1;
forcing_index_diffusivity = 1;

disp('  loading biological initialization values');

%load constants
cgt_init_constants;

%load initial tracer concentrations
cgt_init_tracers;

% fill the output with zeros or NaNs
cgt_init_output;

output_vector_temperature   = zeros(1,kmax);
output_vector_salinity      = zeros(1,kmax);
output_vector_opacity       = zeros(1,kmax);
output_vector_light         = zeros(1,kmax);
output_vector_diffusivity   = zeros(1,kmax);
output_scalar_light_at_top  = 0.0;
output_scalar_zenith_angle  = 0.0;
output_scalar_bottom_stress = 0.0;
output_temperature   = zeros(kmax,max_output_index)/0.0;
output_salinity      = zeros(kmax,max_output_index)/0.0;
output_opacity       = zeros(kmax,max_output_index)/0.0;
output_light         = zeros(kmax,max_output_index)/0.0;
output_diffusivity   = zeros(kmax,max_output_index)/0.0;
output_light_at_top  = zeros(1   ,max_output_index)/0.0;
output_zenith_angle  = zeros(1   ,max_output_index)/0.0;
output_bottom_stress = zeros(1   ,max_output_index)/0.0;
output_count  = 0;

disp('starting the run');

% do the timestep
while current_date < repeated_runs*(end_date-start_date)+start_date
    % load the physics
    [forcing_vector_temperature  , forcing_index_temperature]   = load_forcing(forcing_matrix_temperature,current_date,start_date,end_date, kmax, forcing_index_temperature);
    [forcing_vector_salinity     , forcing_index_salinity]      = load_forcing(forcing_matrix_salinity,current_date,start_date,end_date, kmax, forcing_index_salinity);
    [forcing_vector_opacity_water, forcing_index_opacity_water] = load_forcing(forcing_matrix_opacity_water,current_date,start_date,end_date, kmax, forcing_index_opacity_water);
    [forcing_vector_diffusivity  , forcing_index_diffusivity]   = load_forcing(forcing_matrix_diffusivity,current_date,start_date,end_date, kmax, forcing_index_diffusivity);
    [forcing_scalar_light_at_top , forcing_index_light_at_top]  = load_forcing(forcing_matrix_light_at_top,current_date,start_date,end_date, kmax, forcing_index_light_at_top);
    [forcing_scalar_bottom_stress, forcing_index_bottom_stress] = load_forcing(forcing_matrix_bottom_stress,current_date,start_date,end_date, kmax, forcing_index_bottom_stress);
    
    % light calculation
    sun_pos = sun_position(current_date,location);
    forcing_scalar_zenith_angle = sun_pos.zenith*pi/180;  

    cgt_calc_opacity_bio;
    
    forcing_vector_opacity      = forcing_vector_opacity_bio + forcing_vector_opacity_water;
    forcing_vector_light        = zeros(1,kmax);
    if forcing_scalar_zenith_angle*180/pi < 90 % daytime
      zenith_angle_under_water = asin(sin(forcing_scalar_zenith_angle)/1.33);% consider refraction at sea surface       
      forcing_vector_light(1)     = forcing_scalar_light_at_top;
      for k=1:kmax
        light_path_length = depths(k)/cos(zenith_angle_under_water);
        if k<kmax
          forcing_vector_light(k+1) = forcing_vector_light(k)*exp(-forcing_vector_opacity(k)*light_path_length);
        end
        forcing_vector_light(k)   = forcing_vector_light(k)*exp(-forcing_vector_opacity(k)*light_path_length*0.5);
      end
    end
    
    % output of physics during this time step
    output_vector_temperature   = output_vector_temperature   + forcing_vector_temperature;
    output_vector_salinity      = output_vector_salinity      + forcing_vector_salinity;
    output_vector_opacity       = output_vector_opacity       + forcing_vector_opacity;
    output_vector_light         = output_vector_light         + forcing_vector_light;
    output_vector_diffusivity   = output_vector_diffusivity   + forcing_vector_diffusivity;
    output_scalar_light_at_top  = output_scalar_light_at_top  + forcing_scalar_light_at_top;
    output_scalar_zenith_angle  = output_scalar_zenith_angle  + forcing_scalar_zenith_angle;
    output_scalar_bottom_stress = output_scalar_bottom_stress + forcing_scalar_bottom_stress;
    output_count=output_count+1;
    
    % do the biology including vertical migration / particle sinking
    cgt_bio_timestep;
    
    % do the vertical mixing
    cgt_mixing_timestep;
    
    % check if output needs to be saved in final array
    if current_date*(1.0+1.0e-10) >= current_output_date + output_interval
        % display current date/time
        disp(datestr(current_date));
        % do the output of physics
        for k=1:kmax
            output_temperature(k,current_output_index) = output_vector_temperature(k) /output_count;
            output_salinity(k,current_output_index)    = output_vector_salinity(k)    /output_count;
            output_opacity(k,current_output_index)     = output_vector_opacity(k)     /output_count;
            output_light(k,current_output_index)       = output_vector_light(k)       /output_count;            
            output_diffusivity(k,current_output_index) = output_vector_diffusivity(k) /output_count;            
        end
        output_light_at_top(current_output_index)  = output_scalar_light_at_top   /output_count;
        output_bottom_stress(current_output_index) = output_scalar_bottom_stress  /output_count;
        output_zenith_angle(current_output_index)  = output_scalar_zenith_angle   /output_count;
        % reset temporary physics output values
        output_vector_temperature   = zeros(1,kmax);
        output_vector_salinity      = zeros(1,kmax);
        output_vector_opacity       = zeros(1,kmax);
        output_vector_light         = zeros(1,kmax);
        output_vector_diffusivity   = zeros(1,kmax);
        output_scalar_light_at_top  = 0.0;
        output_scalar_zenith_angle  = 0.0;
        output_scalar_bottom_stress = 0.0;
        %do the output of biology
        cgt_output_final;
        %reset output indexes
        output_count  = 0;
        current_output_index = current_output_index + 1;
        current_output_date=current_output_date+output_interval;
        
        % NEW
        i_count_l = 1;
        i_count_s = 1;
    end
    
    % update the current date/time
    current_date = current_date + timestep;
end
