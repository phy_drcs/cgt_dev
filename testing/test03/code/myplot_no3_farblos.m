%function [ output_plot ] = myplot( input_name )
% plot 1d or 2d variables
input_matrix=output_no3 - (output_no3_with_green_N + output_no3_with_darkgreen_N + output_no3_with_blue_N + output_no3_with_red_N + output_no3_with_pink_N);

if size(input_matrix,1)==size(depths,1)
  if sum(mylevels<0)
    contourf(time_axis',depths',input_matrix);
  else
    contourf(time_axis',depths',input_matrix,mylevels);
  end
  shading flat;
  colorbar;
  set(gca,'YDir','reverse');
  set(gca,'xtick',time_axis);
  datetick('x','keeplimits');
  xlabel('time');
  ylabel('depth [m]');
  title(cgt_get_description(myvar));
end

if size(input_matrix,1)==1
  plot(time_axis',input_matrix);
  datetick('x');
  xlabel('time');
  title(cgt_get_description(myvar));
end