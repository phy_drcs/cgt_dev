%--------------------------------------------------------------------------
% save tracers, auxiliary variables and process rates to final output array
%--------------------------------------------------------------------------
% in the water column
for k=1:kmax
    output_no3            (k,current_output_index)=output_vector_no3            (k)/output_count;
    output_po4            (k,current_output_index)=output_vector_po4            (k)/output_count;
    output_o2             (k,current_output_index)=output_vector_o2             (k)/output_count;
    output_dia            (k,current_output_index)=output_vector_dia            (k)/output_count;
    output_det            (k,current_output_index)=output_vector_det            (k)/output_count;
    output_no3_with_green_O(k,current_output_index)=output_vector_no3_with_green_O(k)/output_count;
    output_po4_with_green_O(k,current_output_index)=output_vector_po4_with_green_O(k)/output_count;
    output_o2_with_green_O(k,current_output_index)=output_vector_o2_with_green_O(k)/output_count;
    output_dia_with_green_O(k,current_output_index)=output_vector_dia_with_green_O(k)/output_count;
    output_det_with_green_O(k,current_output_index)=output_vector_det_with_green_O(k)/output_count;
    output_no3_with_green_N(k,current_output_index)=output_vector_no3_with_green_N(k)/output_count;
    output_dia_with_green_N(k,current_output_index)=output_vector_dia_with_green_N(k)/output_count;
    output_det_with_green_N(k,current_output_index)=output_vector_det_with_green_N(k)/output_count;
    output_no3_with_red_N (k,current_output_index)=output_vector_no3_with_red_N (k)/output_count;
    output_dia_with_red_N (k,current_output_index)=output_vector_dia_with_red_N (k)/output_count;
    output_det_with_red_N (k,current_output_index)=output_vector_det_with_red_N (k)/output_count;
    output_no3_with_darkgreen_N(k,current_output_index)=output_vector_no3_with_darkgreen_N(k)/output_count;
    output_dia_with_darkgreen_N(k,current_output_index)=output_vector_dia_with_darkgreen_N(k)/output_count;
    output_det_with_darkgreen_N(k,current_output_index)=output_vector_det_with_darkgreen_N(k)/output_count;
    output_no3_with_pink_N(k,current_output_index)=output_vector_no3_with_pink_N(k)/output_count;
    output_dia_with_pink_N(k,current_output_index)=output_vector_dia_with_pink_N(k)/output_count;
    output_det_with_pink_N(k,current_output_index)=output_vector_det_with_pink_N(k)/output_count;
    output_no3_with_blue_N(k,current_output_index)=output_vector_no3_with_blue_N(k)/output_count;
    output_dia_with_blue_N(k,current_output_index)=output_vector_dia_with_blue_N(k)/output_count;
    output_det_with_blue_N(k,current_output_index)=output_vector_det_with_blue_N(k)/output_count;
    output_no3_with_grey_N(k,current_output_index)=output_vector_no3_with_grey_N(k)/output_count;
    output_dia_with_grey_N(k,current_output_index)=output_vector_dia_with_grey_N(k)/output_count;
    output_det_with_grey_N(k,current_output_index)=output_vector_det_with_grey_N(k)/output_count;
end
% in the sediment
% at the sea surface

% now, set the temporary output vector to zero
  output_vector_no3             = zeros(1,kmax);
  output_vector_po4             = zeros(1,kmax);
  output_vector_o2              = zeros(1,kmax);
  output_vector_dia             = zeros(1,kmax);
  output_vector_det             = zeros(1,kmax);
  output_vector_no3_with_green_O = zeros(1,kmax);
  output_vector_po4_with_green_O = zeros(1,kmax);
  output_vector_o2_with_green_O = zeros(1,kmax);
  output_vector_dia_with_green_O = zeros(1,kmax);
  output_vector_det_with_green_O = zeros(1,kmax);
  output_vector_no3_with_green_N = zeros(1,kmax);
  output_vector_dia_with_green_N = zeros(1,kmax);
  output_vector_det_with_green_N = zeros(1,kmax);
  output_vector_no3_with_red_N  = zeros(1,kmax);
  output_vector_dia_with_red_N  = zeros(1,kmax);
  output_vector_det_with_red_N  = zeros(1,kmax);
  output_vector_no3_with_darkgreen_N = zeros(1,kmax);
  output_vector_dia_with_darkgreen_N = zeros(1,kmax);
  output_vector_det_with_darkgreen_N = zeros(1,kmax);
  output_vector_no3_with_pink_N = zeros(1,kmax);
  output_vector_dia_with_pink_N = zeros(1,kmax);
  output_vector_det_with_pink_N = zeros(1,kmax);
  output_vector_no3_with_blue_N = zeros(1,kmax);
  output_vector_dia_with_blue_N = zeros(1,kmax);
  output_vector_det_with_blue_N = zeros(1,kmax);
  output_vector_no3_with_grey_N = zeros(1,kmax);
  output_vector_dia_with_grey_N = zeros(1,kmax);
  output_vector_det_with_grey_N = zeros(1,kmax);
