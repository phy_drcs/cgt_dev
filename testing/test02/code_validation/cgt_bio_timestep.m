    if firstBioTimestep
        tracer_vector_no3_with_grey_N = tracer_vector_no3 - tracer_vector_no3_with_green_N - tracer_vector_no3_with_red_N - tracer_vector_no3_with_darkgreen_N - tracer_vector_no3_with_pink_N - tracer_vector_no3_with_blue_N;
        firstBioTimestep = false;
    end
    
    sum_dia_with_colored_N = tracer_vector_dia_with_green_N + tracer_vector_dia_with_darkgreen_N + tracer_vector_dia_with_blue_N + tracer_vector_dia_with_red_N + tracer_vector_dia_with_pink_N;
    sum_no3_with_colored_N = tracer_vector_no3_with_green_N + tracer_vector_no3_with_darkgreen_N + tracer_vector_no3_with_blue_N + tracer_vector_dia_with_red_N + tracer_vector_no3_with_pink_N;
    sum_det_with_colored_N = tracer_vector_det_with_green_N + tracer_vector_det_with_darkgreen_N + tracer_vector_det_with_blue_N + tracer_vector_det_with_red_N + tracer_vector_det_with_pink_N;
    
    for k = 1:kmax 
        if(sum_dia_with_colored_N(k) > tracer_vector_dia(k))
            tracer_vector_dia_with_green_N(k) = tracer_vector_dia(k) * tracer_vector_dia_with_green_N(k) / sum_dia_with_colored_N(k);
            tracer_vector_dia_with_darkgreen_N(k) = tracer_vector_dia(k) * tracer_vector_dia_with_darkgreen_N(k) / sum_dia_with_colored_N(k);
            tracer_vector_dia_with_blue_N(k) = tracer_vector_dia(k) * tracer_vector_dia_with_blue_N(k) / sum_dia_with_colored_N(k);
            tracer_vector_dia_with_red_N(k) = tracer_vector_dia(k) * tracer_vector_dia_with_red_N(k) / sum_dia_with_colored_N(k);
            tracer_vector_dia_with_pink_N(k) = tracer_vector_dia(k) * tracer_vector_dia_with_pink_N(k) / sum_dia_with_colored_N(k);
            disp(strcat('corrected dia: ', num2str(sum_dia_with_colored_N(k) - tracer_vector_dia(k)), ', k=', num2str(k)))
        end
        
        if(sum_det_with_colored_N(k) > tracer_vector_det(k))
            tracer_vector_det_with_green_N(k) = tracer_vector_det(k) * tracer_vector_det_with_green_N(k) / sum_det_with_colored_N(k);
            tracer_vector_det_with_darkgreen_N(k) = tracer_vector_det(k) * tracer_vector_det_with_darkgreen_N(k) / sum_det_with_colored_N(k);
            tracer_vector_det_with_blue_N(k) = tracer_vector_det(k) * tracer_vector_det_with_blue_N(k) / sum_det_with_colored_N(k);
            tracer_vector_det_with_red_N(k) = tracer_vector_det(k) * tracer_vector_det_with_red_N(k) / sum_det_with_colored_N(k);
            tracer_vector_det_with_pink_N(k) = tracer_vector_det(k) * tracer_vector_det_with_pink_N(k) / sum_det_with_colored_N(k);
            disp(strcat('corrected det: ', num2str(sum_det_with_colored_N(k) - tracer_vector_det(k)), ', k=', num2str(k)))
        end
        
        if(sum_no3_with_colored_N(k) > tracer_vector_no3(k))
            tracer_vector_no3_with_green_N(k) = tracer_vector_no3(k) * tracer_vector_no3_with_green_N(k) / sum_no3_with_colored_N(k);
            tracer_vector_no3_with_darkgreen_N(k) = tracer_vector_no3(k) * tracer_vector_no3_with_darkgreen_N(k) / sum_no3_with_colored_N(k);
            tracer_vector_no3_with_blue_N(k) = tracer_vector_no3(k) * tracer_vector_no3_with_blue_N(k) / sum_no3_with_colored_N(k);
            tracer_vector_no3_with_red_N(k) = tracer_vector_no3(k) * tracer_vector_no3_with_red_N(k) / sum_no3_with_colored_N(k);
            tracer_vector_no3_with_pink_N(k) = tracer_vector_no3(k) * tracer_vector_no3_with_pink_N(k) / sum_no3_with_colored_N(k);
            disp(strcat('corrected no3: ', num2str(sum_no3_with_colored_N(k) - tracer_vector_no3(k)), ', k=', num2str(k)))
        end
    end
    
    % calculate total element concentrations in the water column
    for k = 1:kmax
             tracer_vector_total_green_O  (k) = ...
                max(0.0,tracer_vector_no3_with_green_O(k))*3 + ...
                max(0.0,tracer_vector_po4_with_green_O(k))*4 + ...
                max(0.0,tracer_vector_o2_with_green_O(k))*2 + ...
                max(0.0,tracer_vector_dia_with_green_O(k))*110/16 + ...
                max(0.0,tracer_vector_det_with_green_O(k))*110/16 + ...
                0.0;
             tracer_vector_total_green_N  (k) = ...
                max(0.0,tracer_vector_no3_with_green_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_green_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_green_N(k))*1 + ...
                0.0;
             tracer_vector_total_red_N    (k) = ...
                max(0.0,tracer_vector_no3_with_red_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_red_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_red_N(k))*1 + ...
                0.0;
             tracer_vector_total_darkgreen_N(k) = ...
                max(0.0,tracer_vector_no3_with_darkgreen_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_darkgreen_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_darkgreen_N(k))*1 + ...
                0.0;
             tracer_vector_total_pink_N   (k) = ...
                max(0.0,tracer_vector_no3_with_pink_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_pink_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_pink_N(k))*1 + ...
                0.0;
             tracer_vector_total_blue_N   (k) = ...
                max(0.0,tracer_vector_no3_with_blue_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_blue_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_blue_N(k))*1 + ...
                0.0;
             tracer_vector_total_grey_N   (k) = ...
                max(0.0,tracer_vector_no3_with_grey_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_grey_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_grey_N(k))*1 + ...
                0.0;
    end

    % calculate total colored element concentrations at bottom
          tracer_scalar_total_green_O_at_bottom = ...
             0.0;
          tracer_scalar_total_green_N_at_bottom = ...
             0.0;
          tracer_scalar_total_red_N_at_bottom = ...
             0.0;
          tracer_scalar_total_darkgreen_N_at_bottom = ...
             0.0;
          tracer_scalar_total_pink_N_at_bottom = ...
             0.0;
          tracer_scalar_total_blue_N_at_bottom = ...
             0.0;
          tracer_scalar_total_grey_N_at_bottom = ...
             0.0;

    % Water column processes

    cgt_bottomdepth = 0.0;
    for k = 1:kmax
       cgt_bottomdepth = cgt_bottomdepth + cellheights(k);
          
             %------------------------------------
             % STEP 1: prepare abiotic parameters
             %------------------------------------
             cgt_temp       = forcing_vector_temperature(k);           % potential temperature     [Celsius]
             cgt_sali       = forcing_vector_salinity(k);              % salinity                  [g/kg]
             cgt_light      = forcing_vector_light(k);                 % light intensity           [W/m2]
             cgt_cellheight = cellheights(k);                          % cell height               [m]
             cgt_density    = density_water;                           % density                   [kg/m3]
             cgt_timestep   = timestep;                                % timestep                  [days]
             cgt_latitude   = location.latitude;                       % geographic latitude       [deg]
             if k == kmax 
                cgt_current_wave_stress=forcing_scalar_bottom_stress;  % bottom stress             [N/m2]
             end                     
             
             %------------------------------------
             % STEP 2: load tracer values
             %------------------------------------
             no3             = tracer_vector_no3            (k); % nitrate
             if k == 1
                above_no3             = tracer_vector_no3            (k);
             else
                above_no3             = tracer_vector_no3            (k-1);
             end
             po4             = tracer_vector_po4            (k); % phosphate
             if k == 1
                above_po4             = tracer_vector_po4            (k);
             else
                above_po4             = tracer_vector_po4            (k-1);
             end
             o2              = tracer_vector_o2             (k); % oxygen
             if k == 1
                above_o2              = tracer_vector_o2             (k);
             else
                above_o2              = tracer_vector_o2             (k-1);
             end
             dia             = tracer_vector_dia            (k); % diatoms
             if k == 1
                above_dia             = tracer_vector_dia            (k);
             else
                above_dia             = tracer_vector_dia            (k-1);
             end
             det             = tracer_vector_det            (k); % detritus
             if k == 1
                above_det             = tracer_vector_det            (k);
             else
                above_det             = tracer_vector_det            (k-1);
             end
             no3_with_green_O = tracer_vector_no3_with_green_O(k); % nitrate; containing 
             if k == 1
                above_no3_with_green_O = tracer_vector_no3_with_green_O(k);
             else
                above_no3_with_green_O = tracer_vector_no3_with_green_O(k-1);
             end
             po4_with_green_O = tracer_vector_po4_with_green_O(k); % phosphate; containing 
             if k == 1
                above_po4_with_green_O = tracer_vector_po4_with_green_O(k);
             else
                above_po4_with_green_O = tracer_vector_po4_with_green_O(k-1);
             end
             o2_with_green_O = tracer_vector_o2_with_green_O(k); % oxygen; containing 
             if k == 1
                above_o2_with_green_O = tracer_vector_o2_with_green_O(k);
             else
                above_o2_with_green_O = tracer_vector_o2_with_green_O(k-1);
             end
             dia_with_green_O = tracer_vector_dia_with_green_O(k); % diatoms; containing 
             if k == 1
                above_dia_with_green_O = tracer_vector_dia_with_green_O(k);
             else
                above_dia_with_green_O = tracer_vector_dia_with_green_O(k-1);
             end
             det_with_green_O = tracer_vector_det_with_green_O(k); % detritus; containing 
             if k == 1
                above_det_with_green_O = tracer_vector_det_with_green_O(k);
             else
                above_det_with_green_O = tracer_vector_det_with_green_O(k-1);
             end
             no3_with_green_N = tracer_vector_no3_with_green_N(k); % nitrate; containing nitrogen bound by diatomes
             if k == 1
                above_no3_with_green_N = tracer_vector_no3_with_green_N(k);
             else
                above_no3_with_green_N = tracer_vector_no3_with_green_N(k-1);
             end
             dia_with_green_N = tracer_vector_dia_with_green_N(k); % diatoms; containing nitrogen bound by diatomes
             if k == 1
                above_dia_with_green_N = tracer_vector_dia_with_green_N(k);
             else
                above_dia_with_green_N = tracer_vector_dia_with_green_N(k-1);
             end
             det_with_green_N = tracer_vector_det_with_green_N(k); % detritus; containing nitrogen bound by diatomes
             if k == 1
                above_det_with_green_N = tracer_vector_det_with_green_N(k);
             else
                above_det_with_green_N = tracer_vector_det_with_green_N(k-1);
             end
             no3_with_red_N  = tracer_vector_no3_with_red_N (k); % nitrate; containing nitrogen formerly bound by diatoms
             if k == 1
                above_no3_with_red_N  = tracer_vector_no3_with_red_N (k);
             else
                above_no3_with_red_N  = tracer_vector_no3_with_red_N (k-1);
             end
             dia_with_red_N  = tracer_vector_dia_with_red_N (k); % diatoms; containing nitrogen formerly bound by diatoms
             if k == 1
                above_dia_with_red_N  = tracer_vector_dia_with_red_N (k);
             else
                above_dia_with_red_N  = tracer_vector_dia_with_red_N (k-1);
             end
             det_with_red_N  = tracer_vector_det_with_red_N (k); % detritus; containing nitrogen formerly bound by diatoms
             if k == 1
                above_det_with_red_N  = tracer_vector_det_with_red_N (k);
             else
                above_det_with_red_N  = tracer_vector_det_with_red_N (k-1);
             end
             no3_with_darkgreen_N = tracer_vector_no3_with_darkgreen_N(k); % nitrate; containing nitrogen bound by diatoms a second time
             if k == 1
                above_no3_with_darkgreen_N = tracer_vector_no3_with_darkgreen_N(k);
             else
                above_no3_with_darkgreen_N = tracer_vector_no3_with_darkgreen_N(k-1);
             end
             dia_with_darkgreen_N = tracer_vector_dia_with_darkgreen_N(k); % diatoms; containing nitrogen bound by diatoms a second time
             if k == 1
                above_dia_with_darkgreen_N = tracer_vector_dia_with_darkgreen_N(k);
             else
                above_dia_with_darkgreen_N = tracer_vector_dia_with_darkgreen_N(k-1);
             end
             det_with_darkgreen_N = tracer_vector_det_with_darkgreen_N(k); % detritus; containing nitrogen bound by diatoms a second time
             if k == 1
                above_det_with_darkgreen_N = tracer_vector_det_with_darkgreen_N(k);
             else
                above_det_with_darkgreen_N = tracer_vector_det_with_darkgreen_N(k-1);
             end
             no3_with_pink_N = tracer_vector_no3_with_pink_N(k); % nitrate; containing nitrogen formerly bound by diatoms a second time
             if k == 1
                above_no3_with_pink_N = tracer_vector_no3_with_pink_N(k);
             else
                above_no3_with_pink_N = tracer_vector_no3_with_pink_N(k-1);
             end
             dia_with_pink_N = tracer_vector_dia_with_pink_N(k); % diatoms; containing nitrogen formerly bound by diatoms a second time
             if k == 1
                above_dia_with_pink_N = tracer_vector_dia_with_pink_N(k);
             else
                above_dia_with_pink_N = tracer_vector_dia_with_pink_N(k-1);
             end
             det_with_pink_N = tracer_vector_det_with_pink_N(k); % detritus; containing nitrogen formerly bound by diatoms a second time
             if k == 1
                above_det_with_pink_N = tracer_vector_det_with_pink_N(k);
             else
                above_det_with_pink_N = tracer_vector_det_with_pink_N(k-1);
             end
             no3_with_blue_N = tracer_vector_no3_with_blue_N(k); % nitrate; containing nitrogen bound by diatoms a third or more times
             if k == 1
                above_no3_with_blue_N = tracer_vector_no3_with_blue_N(k);
             else
                above_no3_with_blue_N = tracer_vector_no3_with_blue_N(k-1);
             end
             dia_with_blue_N = tracer_vector_dia_with_blue_N(k); % diatoms; containing nitrogen bound by diatoms a third or more times
             if k == 1
                above_dia_with_blue_N = tracer_vector_dia_with_blue_N(k);
             else
                above_dia_with_blue_N = tracer_vector_dia_with_blue_N(k-1);
             end
             det_with_blue_N = tracer_vector_det_with_blue_N(k); % detritus; containing nitrogen bound by diatoms a third or more times
             if k == 1
                above_det_with_blue_N = tracer_vector_det_with_blue_N(k);
             else
                above_det_with_blue_N = tracer_vector_det_with_blue_N(k-1);
             end
             no3_with_grey_N = tracer_vector_no3_with_grey_N(k); % nitrate; containing 
             if k == 1
                above_no3_with_grey_N = tracer_vector_no3_with_grey_N(k);
             else
                above_no3_with_grey_N = tracer_vector_no3_with_grey_N(k-1);
             end
             dia_with_grey_N = tracer_vector_dia_with_grey_N(k); % diatoms; containing 
             if k == 1
                above_dia_with_grey_N = tracer_vector_dia_with_grey_N(k);
             else
                above_dia_with_grey_N = tracer_vector_dia_with_grey_N(k-1);
             end
             det_with_grey_N = tracer_vector_det_with_grey_N(k); % detritus; containing 
             if k == 1
                above_det_with_grey_N = tracer_vector_det_with_grey_N(k);
             else
                above_det_with_grey_N = tracer_vector_det_with_grey_N(k-1);
             end

             no3                   = max(no3            ,0.0);
             above_no3             = max(above_no3            ,0.0);
             po4                   = max(po4            ,0.0);
             above_po4             = max(above_po4            ,0.0);
             o2                    = max(o2             ,0.0);
             above_o2              = max(above_o2             ,0.0);
             dia                   = max(dia            ,0.0);
             above_dia             = max(above_dia            ,0.0);
             det                   = max(det            ,0.0);
             above_det             = max(above_det            ,0.0);

             if k == kmax

             end

             %------------------------------------
             % STEP 4.1: calculate auxiliaries
             %------------------------------------
             % light limitation factor for diatoms growth [1] :
             light_lim_dia   = cgt_light/(light_min_dia+cgt_light);
             
             % no3 limitation for diatoms growth [1] :
             no3_lim_dia     = no3/(no3+no3_min_dia)         ;
             
             % po4 limitation for diatoms growth [1] :
             po4_lim_dia     = po4/(po4+po4_min_dia)         ;
             
             % oxygen saturation concentration [mol/kg] :
             oxysat          = (10.18e0+((5.306e-3-4.8725e-5*cgt_temp)*cgt_temp-0.2785e0)*cgt_temp+cgt_sali*((2.2258e-3+(4.39e-7*cgt_temp-4.645e-5)*cgt_temp)*cgt_temp-6.33e-2))*44.66e0*1e-6;
             

             if k == kmax
             end
             
             if k == 1
             end
             
             %------------------------------------
             % STEP 4.2: output of auxiliaries
             %------------------------------------

             if k == kmax
             end
             if k == 1
             end

             %------------------------------------
             % STEP 5: calculate process limitations
             %------------------------------------

             lim_no3_0            = theta(no3-0.0);
             lim_po4_1            = theta(po4-0.0);
             lim_o2_3             = theta(o2-0.0);
             lim_dia_2            = theta(dia-0.0);

             if k == kmax
             end

             if k == 1
             end

             %------------------------------------
             %-- POSITIVE-DEFINITE SCHEME --------
             %-- means the following steps will be repeated as often as nessecary
             %------------------------------------

             fraction_of_total_timestep = 1.0;   % how much of the original timestep is remaining
             total_rate_growth_dia               = 0.0;
             total_rate_respiration_dia          = 0.0;
             total_rate_mortality_dia            = 0.0;
             total_rate_surf_flux_o2_down          = 0.0;
             total_rate_surf_flux_o2_up          = 0.0;
             total_rate_respiration_dia_green_O          = 0.0;
             total_rate_mortality_dia_green_O          = 0.0;
             total_rate_surf_flux_o2_up_green_O          = 0.0;
             total_rate_growth_dia_green_N          = 0.0;
             total_rate_respiration_dia_green_N          = 0.0;
             total_rate_mortality_dia_green_N          = 0.0;
             total_rate_growth_dia_red_N          = 0.0;
             total_rate_respiration_dia_red_N          = 0.0;
             total_rate_mortality_dia_red_N          = 0.0;
             total_rate_growth_dia_darkgreen_N          = 0.0;
             total_rate_respiration_dia_darkgreen_N          = 0.0;
             total_rate_mortality_dia_darkgreen_N          = 0.0;
             total_rate_growth_dia_pink_N          = 0.0;
             total_rate_respiration_dia_pink_N          = 0.0;
             total_rate_mortality_dia_pink_N          = 0.0;
             total_rate_growth_dia_blue_N          = 0.0;
             total_rate_respiration_dia_blue_N          = 0.0;
             total_rate_mortality_dia_blue_N          = 0.0;
             total_rate_growth_dia_grey_N          = 0.0;
             total_rate_respiration_dia_grey_N          = 0.0;
             total_rate_mortality_dia_grey_N          = 0.0;
             number_of_loop = 1;

             while cgt_timestep > 0.0

                %------------------------------------
                % STEP 6.1: calculate process rates
                %------------------------------------
                % growth of diatoms :
                growth_dia      = (growth_max_dia * dia * min(light_lim_dia,min(no3_lim_dia,po4_lim_dia)))*lim_no3_0*lim_po4_1;
                growth_dia      = max(growth_dia     ,0.0);

                % respiration of diatoms :
                respiration_dia = (dia * resp_rate_dia)*lim_dia_2*lim_o2_3;
                respiration_dia = max(respiration_dia,0.0);

                % mortality of diatoms :
                mortality_dia   = (mort_rate_dia * dia)*lim_dia_2;
                mortality_dia   = max(mortality_dia  ,0.0);

                % respiration of diatoms; sub-process for green oxygen :
                respiration_dia_green_O = respiration_dia * ((1.0)*(110/16)*max(0.0,min(1.0,dia_with_green_O/max(0.00000000001,dia)))+(8.625)*(2)*max(0.0,min(1.0,o2_with_green_O/max(0.00000000001,o2)))) / ((1.0)*(110/16)+(8.625)*(2));
                respiration_dia_green_O = max(respiration_dia_green_O,0.0);

                % mortality of diatoms; sub-process for green oxygen :
                mortality_dia_green_O = mortality_dia * ((1.0)*(110/16)*max(0.0,min(1.0,dia_with_green_O/max(0.00000000001,dia)))) / ((1.0)*(110/16));
                mortality_dia_green_O = max(mortality_dia_green_O,0.0);

                % growth of diatoms; sub-process for green nitrogen :
                growth_dia_green_N = growth_dia * ((1.0)*(1)*max(0.0,min(1.0,no3_with_green_N/max(0.00000000001,no3)))) / ((1.0)*(1));
                growth_dia_green_N = max(growth_dia_green_N,0.0);

                % respiration of diatoms; sub-process for green nitrogen :
                respiration_dia_green_N = respiration_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_green_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                respiration_dia_green_N = max(respiration_dia_green_N,0.0);

                % mortality of diatoms; sub-process for green nitrogen :
                mortality_dia_green_N = mortality_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_green_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                mortality_dia_green_N = max(mortality_dia_green_N,0.0);

                % growth of diatoms; sub-process for red nitrogen :
                growth_dia_red_N = growth_dia * ((1.0)*(1)*max(0.0,min(1.0,no3_with_red_N/max(0.00000000001,no3)))) / ((1.0)*(1));
                growth_dia_red_N = max(growth_dia_red_N,0.0);

                % respiration of diatoms; sub-process for red nitrogen :
                respiration_dia_red_N = respiration_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_red_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                respiration_dia_red_N = max(respiration_dia_red_N,0.0);

                % mortality of diatoms; sub-process for red nitrogen :
                mortality_dia_red_N = mortality_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_red_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                mortality_dia_red_N = max(mortality_dia_red_N,0.0);

                % growth of diatoms; sub-process for darkgreen nitrogen :
                growth_dia_darkgreen_N = growth_dia * ((1.0)*(1)*max(0.0,min(1.0,no3_with_darkgreen_N/max(0.00000000001,no3)))) / ((1.0)*(1));
                growth_dia_darkgreen_N = max(growth_dia_darkgreen_N,0.0);

                % respiration of diatoms; sub-process for darkgreen nitrogen :
                respiration_dia_darkgreen_N = respiration_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_darkgreen_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                respiration_dia_darkgreen_N = max(respiration_dia_darkgreen_N,0.0);

                % mortality of diatoms; sub-process for darkgreen nitrogen :
                mortality_dia_darkgreen_N = mortality_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_darkgreen_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                mortality_dia_darkgreen_N = max(mortality_dia_darkgreen_N,0.0);

                % growth of diatoms; sub-process for pink nitrogen :
                growth_dia_pink_N = growth_dia * ((1.0)*(1)*max(0.0,min(1.0,no3_with_pink_N/max(0.00000000001,no3)))) / ((1.0)*(1));
                growth_dia_pink_N = max(growth_dia_pink_N,0.0);

                % respiration of diatoms; sub-process for pink nitrogen :
                respiration_dia_pink_N = respiration_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_pink_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                respiration_dia_pink_N = max(respiration_dia_pink_N,0.0);

                % mortality of diatoms; sub-process for pink nitrogen :
                mortality_dia_pink_N = mortality_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_pink_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                mortality_dia_pink_N = max(mortality_dia_pink_N,0.0);

                % growth of diatoms; sub-process for blue nitrogen :
                growth_dia_blue_N = growth_dia * ((1.0)*(1)*max(0.0,min(1.0,no3_with_blue_N/max(0.00000000001,no3)))) / ((1.0)*(1));
                growth_dia_blue_N = max(growth_dia_blue_N,0.0);

                % respiration of diatoms; sub-process for blue nitrogen :
                respiration_dia_blue_N = respiration_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_blue_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                respiration_dia_blue_N = max(respiration_dia_blue_N,0.0);

                % mortality of diatoms; sub-process for blue nitrogen :
                mortality_dia_blue_N = mortality_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_blue_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                mortality_dia_blue_N = max(mortality_dia_blue_N,0.0);

                % growth of diatoms; sub-process for grey nitrogen :
                growth_dia_grey_N = growth_dia * ((1.0)*(1)*max(0.0,min(1.0,no3_with_grey_N/max(0.00000000001,no3)))) / ((1.0)*(1));
                growth_dia_grey_N = max(growth_dia_grey_N,0.0);

                % respiration of diatoms; sub-process for grey nitrogen :
                respiration_dia_grey_N = respiration_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_grey_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                respiration_dia_grey_N = max(respiration_dia_grey_N,0.0);

                % mortality of diatoms; sub-process for grey nitrogen :
                mortality_dia_grey_N = mortality_dia * ((1.0)*(1)*max(0.0,min(1.0,dia_with_grey_N/max(0.00000000001,dia)))) / ((1.0)*(1));
                mortality_dia_grey_N = max(mortality_dia_grey_N,0.0);


                if k == kmax
                end
             
                if k == 1
                   % downward oxygen flux through the surface :
                   surf_flux_o2_down = piston_vel_oxy*oxysat*cgt_density;
                   surf_flux_o2_down = max(surf_flux_o2_down,0.0);
                
                   % upward oxygen flux through the surface :
                   surf_flux_o2_up = (piston_vel_oxy*o2*cgt_density)*lim_o2_3;
                   surf_flux_o2_up = max(surf_flux_o2_up,0.0);
                
                   % upward oxygen flux through the surface; sub-process for green oxygen :
                   surf_flux_o2_up_green_O = surf_flux_o2_up * ((1.0)*(2)*max(0.0,min(1.0,o2_with_green_O/max(0.00000000001,o2)))) / ((1.0)*(2));
                   surf_flux_o2_up_green_O = max(surf_flux_o2_up_green_O,0.0);
                
                end

                %------------------------------------
                % STEP 6.2: calculate possible euler-forward change (in a full timestep)
                %------------------------------------

                change_of_no3             = 0.0;
                change_of_po4             = 0.0;
                change_of_o2              = 0.0;
                change_of_dia             = 0.0;
                change_of_det             = 0.0;
                change_of_no3_with_green_O = 0.0;
                change_of_po4_with_green_O = 0.0;
                change_of_o2_with_green_O = 0.0;
                change_of_dia_with_green_O = 0.0;
                change_of_det_with_green_O = 0.0;
                change_of_no3_with_green_N = 0.0;
                change_of_dia_with_green_N = 0.0;
                change_of_det_with_green_N = 0.0;
                change_of_no3_with_red_N  = 0.0;
                change_of_dia_with_red_N  = 0.0;
                change_of_det_with_red_N  = 0.0;
                change_of_no3_with_darkgreen_N = 0.0;
                change_of_dia_with_darkgreen_N = 0.0;
                change_of_det_with_darkgreen_N = 0.0;
                change_of_no3_with_pink_N = 0.0;
                change_of_dia_with_pink_N = 0.0;
                change_of_det_with_pink_N = 0.0;
                change_of_no3_with_blue_N = 0.0;
                change_of_dia_with_blue_N = 0.0;
                change_of_det_with_blue_N = 0.0;
                change_of_no3_with_grey_N = 0.0;
                change_of_dia_with_grey_N = 0.0;
                change_of_det_with_grey_N = 0.0;

             
                change_of_no3             = change_of_no3             + cgt_timestep*(0.0 ...
                   + respiration_dia              ... % respiration of diatoms
                   - growth_dia                   ... % growth of diatoms
                );
             
                change_of_po4             = change_of_po4             + cgt_timestep*(0.0 ...
                   + (respiration_dia)*(1/16)     ... % respiration of diatoms
                   - (growth_dia)*(1/16)          ... % growth of diatoms
                );
             
                change_of_o2              = change_of_o2              + cgt_timestep*(0.0 ...
                   + (growth_dia)*(8.625)         ... % growth of diatoms
                   - (respiration_dia)*(8.625)    ... % respiration of diatoms
                );
             
                change_of_dia             = change_of_dia             + cgt_timestep*(0.0 ...
                   + growth_dia                   ... % growth of diatoms
                   - respiration_dia              ... % respiration of diatoms
                   - mortality_dia                ... % mortality of diatoms
                );
             
                change_of_det             = change_of_det             + cgt_timestep*(0.0 ...
                   + mortality_dia                ... % mortality of diatoms
                );
             
                change_of_no3_with_green_O = change_of_no3_with_green_O + cgt_timestep*(0.0 ...
                   + respiration_dia_green_O      ... % respiration of diatoms; sub-process for green oxygen
                   - growth_dia*max(0.0,min(1.0,no3_with_green_O/max(0.00000000001,no3))) ... % growth of diatoms
                );
             
                change_of_po4_with_green_O = change_of_po4_with_green_O + cgt_timestep*(0.0 ...
                   + (respiration_dia_green_O)*(1/16) ... % respiration of diatoms; sub-process for green oxygen
                   - (growth_dia)*(1/16)*max(0.0,min(1.0,po4_with_green_O/max(0.00000000001,po4))) ... % growth of diatoms
                );
             
                change_of_o2_with_green_O = change_of_o2_with_green_O + cgt_timestep*(0.0 ...
                   + (growth_dia)*(8.625)         ... % growth of diatoms
                   - (respiration_dia)*(8.625)*max(0.0,min(1.0,o2_with_green_O/max(0.00000000001,o2))) ... % respiration of diatoms
                );
             
                change_of_dia_with_green_O = change_of_dia_with_green_O + cgt_timestep*(0.0 ...
                   + growth_dia                   ... % growth of diatoms
                   - respiration_dia*max(0.0,min(1.0,dia_with_green_O/max(0.00000000001,dia))) ... % respiration of diatoms
                   - mortality_dia*max(0.0,min(1.0,dia_with_green_O/max(0.00000000001,dia))) ... % mortality of diatoms
                );
             
                change_of_det_with_green_O = change_of_det_with_green_O + cgt_timestep*(0.0 ...
                   + mortality_dia_green_O        ... % mortality of diatoms; sub-process for green oxygen
                );
             
                change_of_no3_with_green_N = change_of_no3_with_green_N + cgt_timestep*(0.0 ...
                   - growth_dia*max(0.0,min(1.0,no3_with_green_N/max(0.00000000001,no3))) ... % growth of diatoms
                );
             
                change_of_dia_with_green_N = change_of_dia_with_green_N + cgt_timestep*(0.0 ...
                   + growth_dia                   ... % growth of diatoms
                   + growth_dia_green_N           ... % growth of diatoms; sub-process for green nitrogen
                   - respiration_dia*max(0.0,min(1.0,dia_with_green_N/max(0.00000000001,dia))) ... % respiration of diatoms
                   - mortality_dia*max(0.0,min(1.0,dia_with_green_N/max(0.00000000001,dia))) ... % mortality of diatoms
                   - growth_dia_green_N           ... % growth of diatoms; sub-process for green nitrogen
                   - growth_dia_red_N             ... % growth of diatoms; sub-process for red nitrogen
                   - growth_dia_darkgreen_N       ... % growth of diatoms; sub-process for darkgreen nitrogen
                   - growth_dia_pink_N            ... % growth of diatoms; sub-process for pink nitrogen
                   - growth_dia_blue_N            ... % growth of diatoms; sub-process for blue nitrogen
                   - growth_dia_grey_N            ... % growth of diatoms; sub-process for grey nitrogen
                );
             
                change_of_det_with_green_N = change_of_det_with_green_N + cgt_timestep*(0.0 ...
                   + mortality_dia_green_N        ... % mortality of diatoms; sub-process for green nitrogen
                );
             
                change_of_no3_with_red_N  = change_of_no3_with_red_N  + cgt_timestep*(0.0 ...
                   + respiration_dia_green_N      ... % respiration of diatoms; sub-process for green nitrogen
                   + respiration_dia_red_N        ... % respiration of diatoms; sub-process for red nitrogen
                   - growth_dia*max(0.0,min(1.0,no3_with_red_N/max(0.00000000001,no3))) ... % growth of diatoms
                );
             
                change_of_dia_with_red_N  = change_of_dia_with_red_N  + cgt_timestep*(0.0 ...
                   - respiration_dia*max(0.0,min(1.0,dia_with_red_N/max(0.00000000001,dia))) ... % respiration of diatoms
                   - mortality_dia*max(0.0,min(1.0,dia_with_red_N/max(0.00000000001,dia))) ... % mortality of diatoms
                );
             
                change_of_det_with_red_N  = change_of_det_with_red_N  + cgt_timestep*(0.0 ...
                   + mortality_dia_red_N          ... % mortality of diatoms; sub-process for red nitrogen
                );
             
                change_of_no3_with_darkgreen_N = change_of_no3_with_darkgreen_N + cgt_timestep*(0.0 ...
                   - growth_dia*max(0.0,min(1.0,no3_with_darkgreen_N/max(0.00000000001,no3))) ... % growth of diatoms
                );
             
                change_of_dia_with_darkgreen_N = change_of_dia_with_darkgreen_N + cgt_timestep*(0.0 ...
                   + growth_dia                   ... % growth of diatoms
                   + growth_dia_red_N             ... % growth of diatoms; sub-process for red nitrogen
                   + growth_dia_darkgreen_N       ... % growth of diatoms; sub-process for darkgreen nitrogen
                   - respiration_dia*max(0.0,min(1.0,dia_with_darkgreen_N/max(0.00000000001,dia))) ... % respiration of diatoms
                   - mortality_dia*max(0.0,min(1.0,dia_with_darkgreen_N/max(0.00000000001,dia))) ... % mortality of diatoms
                );
             
                change_of_det_with_darkgreen_N = change_of_det_with_darkgreen_N + cgt_timestep*(0.0 ...
                   + mortality_dia_darkgreen_N    ... % mortality of diatoms; sub-process for darkgreen nitrogen
                );
             
                change_of_no3_with_pink_N = change_of_no3_with_pink_N + cgt_timestep*(0.0 ...
                   + respiration_dia_darkgreen_N  ... % respiration of diatoms; sub-process for darkgreen nitrogen
                   + respiration_dia_pink_N       ... % respiration of diatoms; sub-process for pink nitrogen
                   - growth_dia*max(0.0,min(1.0,no3_with_pink_N/max(0.00000000001,no3))) ... % growth of diatoms
                );
             
                change_of_dia_with_pink_N = change_of_dia_with_pink_N + cgt_timestep*(0.0 ...
                   - respiration_dia*max(0.0,min(1.0,dia_with_pink_N/max(0.00000000001,dia))) ... % respiration of diatoms
                   - mortality_dia*max(0.0,min(1.0,dia_with_pink_N/max(0.00000000001,dia))) ... % mortality of diatoms
                );
             
                change_of_det_with_pink_N = change_of_det_with_pink_N + cgt_timestep*(0.0 ...
                   + mortality_dia_pink_N         ... % mortality of diatoms; sub-process for pink nitrogen
                );
             
                change_of_no3_with_blue_N = change_of_no3_with_blue_N + cgt_timestep*(0.0 ...
                   + respiration_dia_blue_N       ... % respiration of diatoms; sub-process for blue nitrogen
                   - growth_dia*max(0.0,min(1.0,no3_with_blue_N/max(0.00000000001,no3))) ... % growth of diatoms
                );
             
                change_of_dia_with_blue_N = change_of_dia_with_blue_N + cgt_timestep*(0.0 ...
                   + growth_dia_pink_N            ... % growth of diatoms; sub-process for pink nitrogen
                   + growth_dia_blue_N            ... % growth of diatoms; sub-process for blue nitrogen
                   - respiration_dia*max(0.0,min(1.0,dia_with_blue_N/max(0.00000000001,dia))) ... % respiration of diatoms
                   - mortality_dia*max(0.0,min(1.0,dia_with_blue_N/max(0.00000000001,dia))) ... % mortality of diatoms
                );
             
                change_of_det_with_blue_N = change_of_det_with_blue_N + cgt_timestep*(0.0 ...
                   + mortality_dia_blue_N         ... % mortality of diatoms; sub-process for blue nitrogen
                );
             
                change_of_no3_with_grey_N = change_of_no3_with_grey_N + cgt_timestep*(0.0 ...
                   + respiration_dia_grey_N       ... % respiration of diatoms; sub-process for grey nitrogen
                   - growth_dia*max(0.0,min(1.0,no3_with_grey_N/max(0.00000000001,no3))) ... % growth of diatoms
                );
             
                change_of_dia_with_grey_N = change_of_dia_with_grey_N + cgt_timestep*(0.0 ...
                   + growth_dia_grey_N            ... % growth of diatoms; sub-process for grey nitrogen
                   - respiration_dia*max(0.0,min(1.0,dia_with_grey_N/max(0.00000000001,dia))) ... % respiration of diatoms
                   - mortality_dia*max(0.0,min(1.0,dia_with_grey_N/max(0.00000000001,dia))) ... % mortality of diatoms
                );
             
                change_of_det_with_grey_N = change_of_det_with_grey_N + cgt_timestep*(0.0 ...
                   + mortality_dia_grey_N         ... % mortality of diatoms; sub-process for grey nitrogen
                );

                if k == 1

                   change_of_no3             = change_of_no3             + cgt_timestep*(0.0 ...
                   );

                   change_of_po4             = change_of_po4             + cgt_timestep*(0.0 ...
                   );

                   change_of_o2              = change_of_o2              + cgt_timestep*(0.0 ...
                      + surf_flux_o2_down/(cgt_cellheight*cgt_density) ... % downward oxygen flux through the surface
                      - surf_flux_o2_up/(cgt_cellheight*cgt_density) ... % upward oxygen flux through the surface
                   );

                   change_of_dia             = change_of_dia             + cgt_timestep*(0.0 ...
                   );

                   change_of_det             = change_of_det             + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_green_O = change_of_no3_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_po4_with_green_O = change_of_po4_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_o2_with_green_O = change_of_o2_with_green_O + cgt_timestep*(0.0 ...
                      - surf_flux_o2_up/(cgt_cellheight*cgt_density)*max(0.0,min(1.0,o2_with_green_O/max(0.00000000001,o2))) ... % upward oxygen flux through the surface
                   );

                   change_of_dia_with_green_O = change_of_dia_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_green_O = change_of_det_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_green_N = change_of_no3_with_green_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_green_N = change_of_dia_with_green_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_green_N = change_of_det_with_green_N + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_red_N  = change_of_no3_with_red_N  + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_red_N  = change_of_dia_with_red_N  + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_red_N  = change_of_det_with_red_N  + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_darkgreen_N = change_of_no3_with_darkgreen_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_darkgreen_N = change_of_dia_with_darkgreen_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_darkgreen_N = change_of_det_with_darkgreen_N + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_pink_N = change_of_no3_with_pink_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_pink_N = change_of_dia_with_pink_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_pink_N = change_of_det_with_pink_N + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_blue_N = change_of_no3_with_blue_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_blue_N = change_of_dia_with_blue_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_blue_N = change_of_det_with_blue_N + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_grey_N = change_of_no3_with_grey_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_grey_N = change_of_dia_with_grey_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_grey_N = change_of_det_with_grey_N + cgt_timestep*(0.0 ...
                   );
                end

                if k == kmax

                   change_of_no3             = change_of_no3             + cgt_timestep*(0.0 ...
                   );

                   change_of_po4             = change_of_po4             + cgt_timestep*(0.0 ...
                   );

                   change_of_o2              = change_of_o2              + cgt_timestep*(0.0 ...
                   );

                   change_of_dia             = change_of_dia             + cgt_timestep*(0.0 ...
                   );

                   change_of_det             = change_of_det             + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_green_O = change_of_no3_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_po4_with_green_O = change_of_po4_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_o2_with_green_O = change_of_o2_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_green_O = change_of_dia_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_green_O = change_of_det_with_green_O + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_green_N = change_of_no3_with_green_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_green_N = change_of_dia_with_green_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_green_N = change_of_det_with_green_N + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_red_N  = change_of_no3_with_red_N  + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_red_N  = change_of_dia_with_red_N  + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_red_N  = change_of_det_with_red_N  + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_darkgreen_N = change_of_no3_with_darkgreen_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_darkgreen_N = change_of_dia_with_darkgreen_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_darkgreen_N = change_of_det_with_darkgreen_N + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_pink_N = change_of_no3_with_pink_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_pink_N = change_of_dia_with_pink_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_pink_N = change_of_det_with_pink_N + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_blue_N = change_of_no3_with_blue_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_blue_N = change_of_dia_with_blue_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_blue_N = change_of_det_with_blue_N + cgt_timestep*(0.0 ...
                   );

                   change_of_no3_with_grey_N = change_of_no3_with_grey_N + cgt_timestep*(0.0 ...
                   );

                   change_of_dia_with_grey_N = change_of_dia_with_grey_N + cgt_timestep*(0.0 ...
                   );

                   change_of_det_with_grey_N = change_of_det_with_grey_N + cgt_timestep*(0.0 ...
                   );
                end           

                %------------------------------------
                % STEP 6.2.5: diagnostic output by Daniel Neumann
                %------------------------------------
                tracer_vector_no3_uncolored = tracer_vector_no3(k) - tracer_vector_no3_with_red_N(k) - tracer_vector_no3_with_pink_N(k) - tracer_vector_no3_with_blue_N(k);
                change_of_no3_uncolored = change_of_no3 - change_of_no3_with_red_N - change_of_no3_with_pink_N - change_of_no3_with_blue_N;
                tracer_vector_dia_uncolored = tracer_vector_dia(k) - tracer_vector_dia_with_green_N(k) - tracer_vector_dia_with_darkgreen_N(k) - tracer_vector_dia_with_blue_N(k);
                change_of_dia_uncolored = change_of_dia - change_of_dia_with_green_N - change_of_dia_with_darkgreen_N - change_of_dia_with_blue_N;
                
                if ( (k == 1) && (current_date >= detail_date_start) && (current_date <= detail_date_stop) )
                    disp('~~~~~~~~~~~ DIAGNOSTIC OUTPUT ~~~~~~~~~~~')
                    disp(strcat(' Date: ', datestr(current_date), ', timestep left: ', num2str(fraction_of_total_timestep), ', number_of_loop', num2str(number_of_loop), ', cgt_timestep:', num2str(cgt_timestep)))
%~ <tracers>
                    %~ tracer_name = strcat('ZERO_BIOLOGY', '_with_noColor');
                    %~ if( (strcmp(tracer_name(1:3), 'no3') && (strcmp(tracer_name(10:12), 'noC') || strcmp(tracer_name(10:12), 'red') || strcmp(tracer_name(10:12), 'pin') || strcmp(tracer_name(10:12), 'blu'))) ...
                     %~ || (strcmp(tracer_name(1:3), 'dia') && (strcmp(tracer_name(10:12), 'noC') || strcmp(tracer_name(10:13), 'gree') || strcmp(tracer_name(10:12), 'dar') || strcmp(tracer_name(10:12), 'blu'))) )
                        %~ disp(' tracer: ZERO_BIOLOGY')
                        %~ disp(strcat('  change_of_*: ', num2str(change_of_ZERO_BIOLOGY(1))))
                        %~ disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_ZERO_BIOLOGY(k))))
                    %~ end
%~ </tracers>
                    disp(' tracer: uncolored no3')
                    disp(strcat('  change_of_*: ', num2str(change_of_no3_uncolored)))
                    disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_no3_uncolored)))
                    disp(' tracer: uncolored dia')
                    disp(strcat('  change_of_*: ', num2str(change_of_dia_uncolored)))
                    disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_dia_uncolored)))
                end
                
                %------------------------------------
                % STEP 6.3: calculate maximum fraction of the timestep before some tracer gets exhausted
                %------------------------------------

                timestep_fraction = 1.0;
                which_tracer_exhausted = -1;

                % find the tracer which is exhausted after the shortest period of time

                % in the water column
             
                % check if tracer no3             was exhausted from the beginning and is still consumed
                if (tracer_vector_no3            (k) <= 0.0) && (change_of_no3             < 0.0)
                   disp(strcat('~~ exhaused but consumded: no3            , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 1;
                end
                % check if tracer no3             was present, but got exhausted
                if (tracer_vector_no3            (k) > 0.0) && (tracer_vector_no3            (k) + change_of_no3             < 0.0)
                   timestep_fraction_new = tracer_vector_no3            (k) / (0.0 - change_of_no3            );
                   disp(strcat('~~ present but got exhausted: no3            , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 1;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer po4             was exhausted from the beginning and is still consumed
                if (tracer_vector_po4            (k) <= 0.0) && (change_of_po4             < 0.0)
                   disp(strcat('~~ exhaused but consumded: po4            , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 2;
                end
                % check if tracer po4             was present, but got exhausted
                if (tracer_vector_po4            (k) > 0.0) && (tracer_vector_po4            (k) + change_of_po4             < 0.0)
                   timestep_fraction_new = tracer_vector_po4            (k) / (0.0 - change_of_po4            );
                   disp(strcat('~~ present but got exhausted: po4            , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 2;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer o2              was exhausted from the beginning and is still consumed
                if (tracer_vector_o2             (k) <= 0.0) && (change_of_o2              < 0.0)
                   disp(strcat('~~ exhaused but consumded: o2             , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 3;
                end
                % check if tracer o2              was present, but got exhausted
                if (tracer_vector_o2             (k) > 0.0) && (tracer_vector_o2             (k) + change_of_o2              < 0.0)
                   timestep_fraction_new = tracer_vector_o2             (k) / (0.0 - change_of_o2             );
                   disp(strcat('~~ present but got exhausted: o2             , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 3;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer dia             was exhausted from the beginning and is still consumed
                if (tracer_vector_dia            (k) <= 0.0) && (change_of_dia             < 0.0)
                   disp(strcat('~~ exhaused but consumded: dia            , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 4;
                end
                % check if tracer dia             was present, but got exhausted
                if (tracer_vector_dia            (k) > 0.0) && (tracer_vector_dia            (k) + change_of_dia             < 0.0)
                   timestep_fraction_new = tracer_vector_dia            (k) / (0.0 - change_of_dia            );
                   disp(strcat('~~ present but got exhausted: dia            , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 4;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer det             was exhausted from the beginning and is still consumed
                if (tracer_vector_det            (k) <= 0.0) && (change_of_det             < 0.0)
                   disp(strcat('~~ exhaused but consumded: det            , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 5;
                end
                % check if tracer det             was present, but got exhausted
                if (tracer_vector_det            (k) > 0.0) && (tracer_vector_det            (k) + change_of_det             < 0.0)
                   timestep_fraction_new = tracer_vector_det            (k) / (0.0 - change_of_det            );
                   disp(strcat('~~ present but got exhausted: det            , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 5;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer no3_with_green_O was exhausted from the beginning and is still consumed
                if (tracer_vector_no3_with_green_O(k) <= 0.0) && (change_of_no3_with_green_O < 0.0)
                   disp(strcat('~~ exhaused but consumded: no3_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 6;
                end
                % check if tracer no3_with_green_O was present, but got exhausted
                if (tracer_vector_no3_with_green_O(k) > 0.0) && (tracer_vector_no3_with_green_O(k) + change_of_no3_with_green_O < 0.0)
                   timestep_fraction_new = tracer_vector_no3_with_green_O(k) / (0.0 - change_of_no3_with_green_O);
                   disp(strcat('~~ present but got exhausted: no3_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 6;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer po4_with_green_O was exhausted from the beginning and is still consumed
                if (tracer_vector_po4_with_green_O(k) <= 0.0) && (change_of_po4_with_green_O < 0.0)
                   disp(strcat('~~ exhaused but consumded: po4_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 7;
                end
                % check if tracer po4_with_green_O was present, but got exhausted
                if (tracer_vector_po4_with_green_O(k) > 0.0) && (tracer_vector_po4_with_green_O(k) + change_of_po4_with_green_O < 0.0)
                   timestep_fraction_new = tracer_vector_po4_with_green_O(k) / (0.0 - change_of_po4_with_green_O);
                   disp(strcat('~~ present but got exhausted: po4_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 7;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer o2_with_green_O was exhausted from the beginning and is still consumed
                if (tracer_vector_o2_with_green_O(k) <= 0.0) && (change_of_o2_with_green_O < 0.0)
                   disp(strcat('~~ exhaused but consumded: o2_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 8;
                end
                % check if tracer o2_with_green_O was present, but got exhausted
                if (tracer_vector_o2_with_green_O(k) > 0.0) && (tracer_vector_o2_with_green_O(k) + change_of_o2_with_green_O < 0.0)
                   timestep_fraction_new = tracer_vector_o2_with_green_O(k) / (0.0 - change_of_o2_with_green_O);
                   disp(strcat('~~ present but got exhausted: o2_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 8;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer dia_with_green_O was exhausted from the beginning and is still consumed
                if (tracer_vector_dia_with_green_O(k) <= 0.0) && (change_of_dia_with_green_O < 0.0)
                   disp(strcat('~~ exhaused but consumded: dia_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 9;
                end
                % check if tracer dia_with_green_O was present, but got exhausted
                if (tracer_vector_dia_with_green_O(k) > 0.0) && (tracer_vector_dia_with_green_O(k) + change_of_dia_with_green_O < 0.0)
                   timestep_fraction_new = tracer_vector_dia_with_green_O(k) / (0.0 - change_of_dia_with_green_O);
                   disp(strcat('~~ present but got exhausted: dia_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 9;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer det_with_green_O was exhausted from the beginning and is still consumed
                if (tracer_vector_det_with_green_O(k) <= 0.0) && (change_of_det_with_green_O < 0.0)
                   disp(strcat('~~ exhaused but consumded: det_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 10;
                end
                % check if tracer det_with_green_O was present, but got exhausted
                if (tracer_vector_det_with_green_O(k) > 0.0) && (tracer_vector_det_with_green_O(k) + change_of_det_with_green_O < 0.0)
                   timestep_fraction_new = tracer_vector_det_with_green_O(k) / (0.0 - change_of_det_with_green_O);
                   disp(strcat('~~ present but got exhausted: det_with_green_O, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 10;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer no3_with_green_N was exhausted from the beginning and is still consumed
                if (tracer_vector_no3_with_green_N(k) <= 0.0) && (change_of_no3_with_green_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: no3_with_green_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 11;
                end
                % check if tracer no3_with_green_N was present, but got exhausted
                if (tracer_vector_no3_with_green_N(k) > 0.0) && (tracer_vector_no3_with_green_N(k) + change_of_no3_with_green_N < 0.0)
                   timestep_fraction_new = tracer_vector_no3_with_green_N(k) / (0.0 - change_of_no3_with_green_N);
                   disp(strcat('~~ present but got exhausted: no3_with_green_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 11;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer dia_with_green_N was exhausted from the beginning and is still consumed
                if (tracer_vector_dia_with_green_N(k) <= 0.0) && (change_of_dia_with_green_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: dia_with_green_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 12;
                end
                % check if tracer dia_with_green_N was present, but got exhausted
                if (tracer_vector_dia_with_green_N(k) > 0.0) && (tracer_vector_dia_with_green_N(k) + change_of_dia_with_green_N < 0.0)
                   timestep_fraction_new = tracer_vector_dia_with_green_N(k) / (0.0 - change_of_dia_with_green_N);
                   disp(strcat('~~ present but got exhausted: dia_with_green_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 12;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer det_with_green_N was exhausted from the beginning and is still consumed
                if (tracer_vector_det_with_green_N(k) <= 0.0) && (change_of_det_with_green_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: det_with_green_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 13;
                end
                % check if tracer det_with_green_N was present, but got exhausted
                if (tracer_vector_det_with_green_N(k) > 0.0) && (tracer_vector_det_with_green_N(k) + change_of_det_with_green_N < 0.0)
                   timestep_fraction_new = tracer_vector_det_with_green_N(k) / (0.0 - change_of_det_with_green_N);
                   disp(strcat('~~ present but got exhausted: det_with_green_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 13;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer no3_with_red_N  was exhausted from the beginning and is still consumed
                if (tracer_vector_no3_with_red_N (k) <= 0.0) && (change_of_no3_with_red_N  < 0.0)
                   disp(strcat('~~ exhaused but consumded: no3_with_red_N , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 14;
                end
                % check if tracer no3_with_red_N  was present, but got exhausted
                if (tracer_vector_no3_with_red_N (k) > 0.0) && (tracer_vector_no3_with_red_N (k) + change_of_no3_with_red_N  < 0.0)
                   timestep_fraction_new = tracer_vector_no3_with_red_N (k) / (0.0 - change_of_no3_with_red_N );
                   disp(strcat('~~ present but got exhausted: no3_with_red_N , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 14;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer dia_with_red_N  was exhausted from the beginning and is still consumed
                if (tracer_vector_dia_with_red_N (k) <= 0.0) && (change_of_dia_with_red_N  < 0.0)
                   disp(strcat('~~ exhaused but consumded: dia_with_red_N , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 15;
                end
                % check if tracer dia_with_red_N  was present, but got exhausted
                if (tracer_vector_dia_with_red_N (k) > 0.0) && (tracer_vector_dia_with_red_N (k) + change_of_dia_with_red_N  < 0.0)
                   timestep_fraction_new = tracer_vector_dia_with_red_N (k) / (0.0 - change_of_dia_with_red_N );
                   disp(strcat('~~ present but got exhausted: dia_with_red_N , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 15;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer det_with_red_N  was exhausted from the beginning and is still consumed
                if (tracer_vector_det_with_red_N (k) <= 0.0) && (change_of_det_with_red_N  < 0.0)
                   disp(strcat('~~ exhaused but consumded: det_with_red_N , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 16;
                end
                % check if tracer det_with_red_N  was present, but got exhausted
                if (tracer_vector_det_with_red_N (k) > 0.0) && (tracer_vector_det_with_red_N (k) + change_of_det_with_red_N  < 0.0)
                   timestep_fraction_new = tracer_vector_det_with_red_N (k) / (0.0 - change_of_det_with_red_N );
                   disp(strcat('~~ present but got exhausted: det_with_red_N , k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 16;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer no3_with_darkgreen_N was exhausted from the beginning and is still consumed
                if (tracer_vector_no3_with_darkgreen_N(k) <= 0.0) && (change_of_no3_with_darkgreen_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: no3_with_darkgreen_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 17;
                end
                % check if tracer no3_with_darkgreen_N was present, but got exhausted
                if (tracer_vector_no3_with_darkgreen_N(k) > 0.0) && (tracer_vector_no3_with_darkgreen_N(k) + change_of_no3_with_darkgreen_N < 0.0)
                   timestep_fraction_new = tracer_vector_no3_with_darkgreen_N(k) / (0.0 - change_of_no3_with_darkgreen_N);
                   disp(strcat('~~ present but got exhausted: no3_with_darkgreen_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 17;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer dia_with_darkgreen_N was exhausted from the beginning and is still consumed
                if (tracer_vector_dia_with_darkgreen_N(k) <= 0.0) && (change_of_dia_with_darkgreen_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: dia_with_darkgreen_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 18;
                end
                % check if tracer dia_with_darkgreen_N was present, but got exhausted
                if (tracer_vector_dia_with_darkgreen_N(k) > 0.0) && (tracer_vector_dia_with_darkgreen_N(k) + change_of_dia_with_darkgreen_N < 0.0)
                   timestep_fraction_new = tracer_vector_dia_with_darkgreen_N(k) / (0.0 - change_of_dia_with_darkgreen_N);
                   disp(strcat('~~ present but got exhausted: dia_with_darkgreen_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 18;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer det_with_darkgreen_N was exhausted from the beginning and is still consumed
                if (tracer_vector_det_with_darkgreen_N(k) <= 0.0) && (change_of_det_with_darkgreen_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: det_with_darkgreen_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 19;
                end
                % check if tracer det_with_darkgreen_N was present, but got exhausted
                if (tracer_vector_det_with_darkgreen_N(k) > 0.0) && (tracer_vector_det_with_darkgreen_N(k) + change_of_det_with_darkgreen_N < 0.0)
                   timestep_fraction_new = tracer_vector_det_with_darkgreen_N(k) / (0.0 - change_of_det_with_darkgreen_N);
                   disp(strcat('~~ present but got exhausted: det_with_darkgreen_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 19;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer no3_with_pink_N was exhausted from the beginning and is still consumed
                if (tracer_vector_no3_with_pink_N(k) <= 0.0) && (change_of_no3_with_pink_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: no3_with_pink_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 20;
                end
                % check if tracer no3_with_pink_N was present, but got exhausted
                if (tracer_vector_no3_with_pink_N(k) > 0.0) && (tracer_vector_no3_with_pink_N(k) + change_of_no3_with_pink_N < 0.0)
                   timestep_fraction_new = tracer_vector_no3_with_pink_N(k) / (0.0 - change_of_no3_with_pink_N);
                   disp(strcat('~~ present but got exhausted: no3_with_pink_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 20;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer dia_with_pink_N was exhausted from the beginning and is still consumed
                if (tracer_vector_dia_with_pink_N(k) <= 0.0) && (change_of_dia_with_pink_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: dia_with_pink_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 21;
                end
                % check if tracer dia_with_pink_N was present, but got exhausted
                if (tracer_vector_dia_with_pink_N(k) > 0.0) && (tracer_vector_dia_with_pink_N(k) + change_of_dia_with_pink_N < 0.0)
                   timestep_fraction_new = tracer_vector_dia_with_pink_N(k) / (0.0 - change_of_dia_with_pink_N);
                   disp(strcat('~~ present but got exhausted: dia_with_pink_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 21;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer det_with_pink_N was exhausted from the beginning and is still consumed
                if (tracer_vector_det_with_pink_N(k) <= 0.0) && (change_of_det_with_pink_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: det_with_pink_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 22;
                end
                % check if tracer det_with_pink_N was present, but got exhausted
                if (tracer_vector_det_with_pink_N(k) > 0.0) && (tracer_vector_det_with_pink_N(k) + change_of_det_with_pink_N < 0.0)
                   timestep_fraction_new = tracer_vector_det_with_pink_N(k) / (0.0 - change_of_det_with_pink_N);
                   disp(strcat('~~ present but got exhausted: det_with_pink_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 22;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer no3_with_blue_N was exhausted from the beginning and is still consumed
                if (tracer_vector_no3_with_blue_N(k) <= 0.0) && (change_of_no3_with_blue_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: no3_with_blue_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 23;
                end
                % check if tracer no3_with_blue_N was present, but got exhausted
                if (tracer_vector_no3_with_blue_N(k) > 0.0) && (tracer_vector_no3_with_blue_N(k) + change_of_no3_with_blue_N < 0.0)
                   timestep_fraction_new = tracer_vector_no3_with_blue_N(k) / (0.0 - change_of_no3_with_blue_N);
                   disp(strcat('~~ present but got exhausted: no3_with_blue_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 23;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer dia_with_blue_N was exhausted from the beginning and is still consumed
                if (tracer_vector_dia_with_blue_N(k) <= 0.0) && (change_of_dia_with_blue_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: dia_with_blue_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 24;
                end
                % check if tracer dia_with_blue_N was present, but got exhausted
                if (tracer_vector_dia_with_blue_N(k) > 0.0) && (tracer_vector_dia_with_blue_N(k) + change_of_dia_with_blue_N < 0.0)
                   timestep_fraction_new = tracer_vector_dia_with_blue_N(k) / (0.0 - change_of_dia_with_blue_N);
                   disp(strcat('~~ present but got exhausted: dia_with_blue_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 24;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer det_with_blue_N was exhausted from the beginning and is still consumed
                if (tracer_vector_det_with_blue_N(k) <= 0.0) && (change_of_det_with_blue_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: det_with_blue_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 25;
                end
                % check if tracer det_with_blue_N was present, but got exhausted
                if (tracer_vector_det_with_blue_N(k) > 0.0) && (tracer_vector_det_with_blue_N(k) + change_of_det_with_blue_N < 0.0)
                   timestep_fraction_new = tracer_vector_det_with_blue_N(k) / (0.0 - change_of_det_with_blue_N);
                   disp(strcat('~~ present but got exhausted: det_with_blue_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 25;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer no3_with_grey_N was exhausted from the beginning and is still consumed
                if (tracer_vector_no3_with_grey_N(k) <= 0.0) && (change_of_no3_with_grey_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: no3_with_grey_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 26;
                end
                % check if tracer no3_with_grey_N was present, but got exhausted
                if (tracer_vector_no3_with_grey_N(k) > 0.0) && (tracer_vector_no3_with_grey_N(k) + change_of_no3_with_grey_N < 0.0)
                   timestep_fraction_new = tracer_vector_no3_with_grey_N(k) / (0.0 - change_of_no3_with_grey_N);
                   disp(strcat('~~ present but got exhausted: no3_with_grey_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 26;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer dia_with_grey_N was exhausted from the beginning and is still consumed
                if (tracer_vector_dia_with_grey_N(k) <= 0.0) && (change_of_dia_with_grey_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: dia_with_grey_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 27;
                end
                % check if tracer dia_with_grey_N was present, but got exhausted
                if (tracer_vector_dia_with_grey_N(k) > 0.0) && (tracer_vector_dia_with_grey_N(k) + change_of_dia_with_grey_N < 0.0)
                   timestep_fraction_new = tracer_vector_dia_with_grey_N(k) / (0.0 - change_of_dia_with_grey_N);
                   disp(strcat('~~ present but got exhausted: dia_with_grey_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 27;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
             
                % check if tracer det_with_grey_N was exhausted from the beginning and is still consumed
                if (tracer_vector_det_with_grey_N(k) <= 0.0) && (change_of_det_with_grey_N < 0.0)
                   disp(strcat('~~ exhaused but consumded: det_with_grey_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = 28;
                end
                % check if tracer det_with_grey_N was present, but got exhausted
                if (tracer_vector_det_with_grey_N(k) > 0.0) && (tracer_vector_det_with_grey_N(k) + change_of_det_with_grey_N < 0.0)
                   timestep_fraction_new = tracer_vector_det_with_grey_N(k) / (0.0 - change_of_det_with_grey_N);
                   disp(strcat('~~ present but got exhausted: det_with_grey_N, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   if timestep_fraction_new <= timestep_fraction
                      disp('~~~ really exhausted')
                      which_tracer_exhausted = 28;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
                
                if (k == 1) 
                    %~ disp(' tracer: uncolored no3')
                    %~ disp(strcat('  change_of_*: ', num2str(change_of_no3_uncolored)))
                    %~ disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_no3_uncolored)))
                    %~ disp(' tracer: uncolored dia')
                    %~ disp(strcat('  change_of_*: ', num2str(change_of_dia_uncolored)))
                    %~ disp(strcat('  tracer_vector_*: ', num2str(tracer_vector_dia_uncolored)))
                    
                     detail_change_of_no3_uncolored(k,current_output_index,i_count_l) = change_of_no3_uncolored;
                     detail_tracer_vector_no3_uncolored(k,current_output_index,i_count_l) = tracer_vector_no3_uncolored;
                     detail_change_of_dia_uncolored(k,current_output_index,i_count_l) = change_of_dia_uncolored;
                     detail_tracer_vector_dia_uncolored(k,current_output_index,i_count_l) = change_of_dia_uncolored;
                     i_count_l = i_count_l + 1;
                end
                
                %~ % check if tracer ZERO_BIOLOGY was exhausted from the beginning and is still consumed
                %~ if (tracer_vector_no3_uncolored <= 0.0) && (change_of_no3_uncolored < 0.0)
                   %~ disp(strcat('~~ exhaused but consumded: uncolored NO3-, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   %~ timestep_fraction = 0.0;
                %~ end
                %~ % check if tracer ZERO_BIOLOGY was present, but got exhausted
                %~ if (tracer_vector_no3_uncolored > 0.0) && (tracer_vector_no3_uncolored + change_of_no3_uncolored < 0.0)
                   %~ timestep_fraction_new = tracer_vector_no3_uncolored / (0.0 - change_of_no3_uncolored);
                   %~ disp(strcat('~~ present but got exhausted: uncolored NO3-, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   %~ if timestep_fraction_new <= timestep_fraction
                      %~ disp('~~~ really exhausted')
                      %~ timestep_fraction = timestep_fraction_new;
                   %~ end
                %~ end
                
                %~ % check if tracer ZERO_BIOLOGY was exhausted from the beginning and is still consumed
                %~ if (tracer_vector_dia_uncolored <= 0.0) && (change_of_dia_uncolored < 0.0)
                   %~ disp(strcat('~~ exhaused but consumded: uncolored dia, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   %~ timestep_fraction = 0.0;
                %~ end
                %~ % check if tracer ZERO_BIOLOGY was present, but got exhausted
                %~ if (tracer_vector_dia_uncolored > 0.0) && (tracer_vector_dia_uncolored + change_of_dia_uncolored < 0.0)
                   %~ timestep_fraction_new = tracer_vector_dia_uncolored / (0.0 - change_of_dia_uncolored);
                   %~ disp(strcat('~~ present but got exhausted: uncolored dia, k=', num2str(k), ', cgt_timestep=', num2str(cgt_timestep), ', date=', datestr(current_date)))
                   %~ if timestep_fraction_new <= timestep_fraction
                      %~ disp('~~~ really exhausted')
                      %~ timestep_fraction = timestep_fraction_new;
                   %~ end
                %~ end
          
                % in the bottom layer
                if k == kmax
                end          

                % now, update the limitations: rates of the processes limited by this tracer become zero in the future

                if 1 == which_tracer_exhausted
                   lim_no3_0            = 0.0;
                end
                if 2 == which_tracer_exhausted
                   lim_po4_1            = 0.0;
                end
                if 3 == which_tracer_exhausted
                   lim_o2_3             = 0.0;
                end
                if 4 == which_tracer_exhausted
                   lim_dia_2            = 0.0;
                end
                if 5 == which_tracer_exhausted
                end

                %------------------------------------
                % STEP 6.4: apply a Euler-forward timestep with the fraction of the time
                %------------------------------------ 

                % in the water column
             
                % tracer no3             (nitrate):
                tracer_vector_no3            (k) = tracer_vector_no3            (k) + change_of_no3             * timestep_fraction;
             
                % tracer po4             (phosphate):
                tracer_vector_po4            (k) = tracer_vector_po4            (k) + change_of_po4             * timestep_fraction;
             
                % tracer o2              (oxygen):
                tracer_vector_o2             (k) = tracer_vector_o2             (k) + change_of_o2              * timestep_fraction;
             
                % tracer dia             (diatoms):
                tracer_vector_dia            (k) = tracer_vector_dia            (k) + change_of_dia             * timestep_fraction;
             
                % tracer det             (detritus):
                tracer_vector_det            (k) = tracer_vector_det            (k) + change_of_det             * timestep_fraction;
             
                % tracer no3_with_green_O (nitrate; containing ):
                tracer_vector_no3_with_green_O(k) = tracer_vector_no3_with_green_O(k) + change_of_no3_with_green_O * timestep_fraction;
             
                % tracer po4_with_green_O (phosphate; containing ):
                tracer_vector_po4_with_green_O(k) = tracer_vector_po4_with_green_O(k) + change_of_po4_with_green_O * timestep_fraction;
             
                % tracer o2_with_green_O (oxygen; containing ):
                tracer_vector_o2_with_green_O(k) = tracer_vector_o2_with_green_O(k) + change_of_o2_with_green_O * timestep_fraction;
             
                % tracer dia_with_green_O (diatoms; containing ):
                tracer_vector_dia_with_green_O(k) = tracer_vector_dia_with_green_O(k) + change_of_dia_with_green_O * timestep_fraction;
             
                % tracer det_with_green_O (detritus; containing ):
                tracer_vector_det_with_green_O(k) = tracer_vector_det_with_green_O(k) + change_of_det_with_green_O * timestep_fraction;
             
                % tracer no3_with_green_N (nitrate; containing nitrogen bound by diatomes):
                tracer_vector_no3_with_green_N(k) = tracer_vector_no3_with_green_N(k) + change_of_no3_with_green_N * timestep_fraction;
             
                % tracer dia_with_green_N (diatoms; containing nitrogen bound by diatomes):
                tracer_vector_dia_with_green_N(k) = tracer_vector_dia_with_green_N(k) + change_of_dia_with_green_N * timestep_fraction;
             
                % tracer det_with_green_N (detritus; containing nitrogen bound by diatomes):
                tracer_vector_det_with_green_N(k) = tracer_vector_det_with_green_N(k) + change_of_det_with_green_N * timestep_fraction;
             
                % tracer no3_with_red_N  (nitrate; containing nitrogen formerly bound by diatoms):
                tracer_vector_no3_with_red_N (k) = tracer_vector_no3_with_red_N (k) + change_of_no3_with_red_N  * timestep_fraction;
             
                % tracer dia_with_red_N  (diatoms; containing nitrogen formerly bound by diatoms):
                tracer_vector_dia_with_red_N (k) = tracer_vector_dia_with_red_N (k) + change_of_dia_with_red_N  * timestep_fraction;
             
                % tracer det_with_red_N  (detritus; containing nitrogen formerly bound by diatoms):
                tracer_vector_det_with_red_N (k) = tracer_vector_det_with_red_N (k) + change_of_det_with_red_N  * timestep_fraction;
             
                % tracer no3_with_darkgreen_N (nitrate; containing nitrogen bound by diatoms a second time):
                tracer_vector_no3_with_darkgreen_N(k) = tracer_vector_no3_with_darkgreen_N(k) + change_of_no3_with_darkgreen_N * timestep_fraction;
             
                % tracer dia_with_darkgreen_N (diatoms; containing nitrogen bound by diatoms a second time):
                tracer_vector_dia_with_darkgreen_N(k) = tracer_vector_dia_with_darkgreen_N(k) + change_of_dia_with_darkgreen_N * timestep_fraction;
             
                % tracer det_with_darkgreen_N (detritus; containing nitrogen bound by diatoms a second time):
                tracer_vector_det_with_darkgreen_N(k) = tracer_vector_det_with_darkgreen_N(k) + change_of_det_with_darkgreen_N * timestep_fraction;
             
                % tracer no3_with_pink_N (nitrate; containing nitrogen formerly bound by diatoms a second time):
                tracer_vector_no3_with_pink_N(k) = tracer_vector_no3_with_pink_N(k) + change_of_no3_with_pink_N * timestep_fraction;
             
                % tracer dia_with_pink_N (diatoms; containing nitrogen formerly bound by diatoms a second time):
                tracer_vector_dia_with_pink_N(k) = tracer_vector_dia_with_pink_N(k) + change_of_dia_with_pink_N * timestep_fraction;
             
                % tracer det_with_pink_N (detritus; containing nitrogen formerly bound by diatoms a second time):
                tracer_vector_det_with_pink_N(k) = tracer_vector_det_with_pink_N(k) + change_of_det_with_pink_N * timestep_fraction;
             
                % tracer no3_with_blue_N (nitrate; containing nitrogen bound by diatoms a third or more times):
                tracer_vector_no3_with_blue_N(k) = tracer_vector_no3_with_blue_N(k) + change_of_no3_with_blue_N * timestep_fraction;
             
                % tracer dia_with_blue_N (diatoms; containing nitrogen bound by diatoms a third or more times):
                tracer_vector_dia_with_blue_N(k) = tracer_vector_dia_with_blue_N(k) + change_of_dia_with_blue_N * timestep_fraction;
             
                % tracer det_with_blue_N (detritus; containing nitrogen bound by diatoms a third or more times):
                tracer_vector_det_with_blue_N(k) = tracer_vector_det_with_blue_N(k) + change_of_det_with_blue_N * timestep_fraction;
             
                % tracer no3_with_grey_N (nitrate; containing ):
                tracer_vector_no3_with_grey_N(k) = tracer_vector_no3_with_grey_N(k) + change_of_no3_with_grey_N * timestep_fraction;
             
                % tracer dia_with_grey_N (diatoms; containing ):
                tracer_vector_dia_with_grey_N(k) = tracer_vector_dia_with_grey_N(k) + change_of_dia_with_grey_N * timestep_fraction;
             
                % tracer det_with_grey_N (detritus; containing ):
                tracer_vector_det_with_grey_N(k) = tracer_vector_det_with_grey_N(k) + change_of_det_with_grey_N * timestep_fraction;
          
                % in the bottom layer
                if k == kmax
                end           

                %------------------------------------
                % STEP 6.5: output of process rates
                %------------------------------------
                if k == kmax
                end
                if k == 1
                end
             
                %------------------------------------
                % STEP 6.6: set timestep to remaining timestep only
                %------------------------------------

                cgt_timestep = cgt_timestep * (1.0 - timestep_fraction);                         % remaining timestep
                fraction_of_total_timestep = fraction_of_total_timestep * (1.0 - timestep_fraction); % how much of the original timestep is remaining


                if number_of_loop > 100
                   error('aborted positive-definite scheme: more than 100 iterations');
                end
                number_of_loop=number_of_loop+1;

             end
             
             loop_count(k,current_output_index,i_count_s) = number_of_loop;
             i_count_s = i_count_s + 1;
             %------------------------------------
             %-- END OF POSITIVE-DEFINITE SCHEME -
             %------------------------------------  
             
             %------------------------------------
             % STEP 7.1: output of new tracer concentrations
             %------------------------------------
             output_vector_no3            (k) = output_vector_no3            (k) + no3            ;
             output_vector_po4            (k) = output_vector_po4            (k) + po4            ;
             output_vector_o2             (k) = output_vector_o2             (k) + o2             ;
             output_vector_dia            (k) = output_vector_dia            (k) + dia            ;
             output_vector_det            (k) = output_vector_det            (k) + det            ;
             output_vector_no3_with_green_O(k) = output_vector_no3_with_green_O(k) + no3_with_green_O;
             output_vector_po4_with_green_O(k) = output_vector_po4_with_green_O(k) + po4_with_green_O;
             output_vector_o2_with_green_O(k) = output_vector_o2_with_green_O(k) + o2_with_green_O;
             output_vector_dia_with_green_O(k) = output_vector_dia_with_green_O(k) + dia_with_green_O;
             output_vector_det_with_green_O(k) = output_vector_det_with_green_O(k) + det_with_green_O;
             output_vector_no3_with_green_N(k) = output_vector_no3_with_green_N(k) + no3_with_green_N;
             output_vector_dia_with_green_N(k) = output_vector_dia_with_green_N(k) + dia_with_green_N;
             output_vector_det_with_green_N(k) = output_vector_det_with_green_N(k) + det_with_green_N;
             output_vector_no3_with_red_N (k) = output_vector_no3_with_red_N (k) + no3_with_red_N ;
             output_vector_dia_with_red_N (k) = output_vector_dia_with_red_N (k) + dia_with_red_N ;
             output_vector_det_with_red_N (k) = output_vector_det_with_red_N (k) + det_with_red_N ;
             output_vector_no3_with_darkgreen_N(k) = output_vector_no3_with_darkgreen_N(k) + no3_with_darkgreen_N;
             output_vector_dia_with_darkgreen_N(k) = output_vector_dia_with_darkgreen_N(k) + dia_with_darkgreen_N;
             output_vector_det_with_darkgreen_N(k) = output_vector_det_with_darkgreen_N(k) + det_with_darkgreen_N;
             output_vector_no3_with_pink_N(k) = output_vector_no3_with_pink_N(k) + no3_with_pink_N;
             output_vector_dia_with_pink_N(k) = output_vector_dia_with_pink_N(k) + dia_with_pink_N;
             output_vector_det_with_pink_N(k) = output_vector_det_with_pink_N(k) + det_with_pink_N;
             output_vector_no3_with_blue_N(k) = output_vector_no3_with_blue_N(k) + no3_with_blue_N;
             output_vector_dia_with_blue_N(k) = output_vector_dia_with_blue_N(k) + dia_with_blue_N;
             output_vector_det_with_blue_N(k) = output_vector_det_with_blue_N(k) + det_with_blue_N;
             output_vector_no3_with_grey_N(k) = output_vector_no3_with_grey_N(k) + no3_with_grey_N;
             output_vector_dia_with_grey_N(k) = output_vector_dia_with_grey_N(k) + dia_with_grey_N;
             output_vector_det_with_grey_N(k) = output_vector_det_with_grey_N(k) + det_with_grey_N;
             if k == kmax
             end
             if k==1
             end
             
             %------------------------------------
             % STEP 7.2: calculate "late" auxiliaries
             %------------------------------------

             if k == kmax
             end
             
             if k == 1
             end
             
             %------------------------------------
             % STEP 7.3: output of "late" auxiliaries
             %------------------------------------
             if k == kmax
             end
             if k == 1
             end

             %---------------------------------------
             % STEP 7.4: passing vertical velocity and diffusivity to the coupler
             %---------------------------------------

             % EXPLICIT MOVEMENT
                vertical_speed_of_dia            (k)=(-0.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_dia            (k)=(0.0);        % leave as m2/s
                vertical_speed_of_det            (k)=(-4.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_det            (k)=(0.0);        % leave as m2/s
                vertical_speed_of_dia_with_green_O(k)=(-0.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_dia_with_green_O(k)=(0.0);        % leave as m2/s
                vertical_speed_of_det_with_green_O(k)=(-4.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_det_with_green_O(k)=(0.0);        % leave as m2/s
                vertical_speed_of_dia_with_green_N(k)=(-0.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_dia_with_green_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_det_with_green_N(k)=(-4.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_det_with_green_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_dia_with_red_N (k)=(-0.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_dia_with_red_N (k)=(0.0);        % leave as m2/s
                vertical_speed_of_det_with_red_N (k)=(-4.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_det_with_red_N (k)=(0.0);        % leave as m2/s
                vertical_speed_of_dia_with_darkgreen_N(k)=(-0.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_dia_with_darkgreen_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_det_with_darkgreen_N(k)=(-4.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_det_with_darkgreen_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_dia_with_pink_N(k)=(-0.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_dia_with_pink_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_det_with_pink_N(k)=(-4.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_det_with_pink_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_dia_with_blue_N(k)=(-0.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_dia_with_blue_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_det_with_blue_N(k)=(-4.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_det_with_blue_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_dia_with_grey_N(k)=(-0.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_dia_with_grey_N(k)=(0.0);        % leave as m2/s
                vertical_speed_of_det_with_grey_N(k)=(-4.5)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_det_with_grey_N(k)=(0.0);        % leave as m2/s
    end
    
    %---------------------------------------
    % biological timestep has ended
    %---------------------------------------
    
    %---------------------------------------
    % vertical movement follows
    %---------------------------------------
    
    % calculate new total marked element concentrations
    for k = 1:kmax
             tracer_vector_total_green_O  (k) = ...
                max(0.0,tracer_vector_no3_with_green_O(k))*3 + ...
                max(0.0,tracer_vector_po4_with_green_O(k))*4 + ...
                max(0.0,tracer_vector_o2_with_green_O(k))*2 + ...
                max(0.0,tracer_vector_dia_with_green_O(k))*110/16 + ...
                max(0.0,tracer_vector_det_with_green_O(k))*110/16 + ...
                0.0;
             tracer_vector_total_green_N  (k) = ...
                max(0.0,tracer_vector_no3_with_green_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_green_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_green_N(k))*1 + ...
                0.0;
             tracer_vector_total_red_N    (k) = ...
                max(0.0,tracer_vector_no3_with_red_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_red_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_red_N(k))*1 + ...
                0.0;
             tracer_vector_total_darkgreen_N(k) = ...
                max(0.0,tracer_vector_no3_with_darkgreen_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_darkgreen_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_darkgreen_N(k))*1 + ...
                0.0;
             tracer_vector_total_pink_N   (k) = ...
                max(0.0,tracer_vector_no3_with_pink_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_pink_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_pink_N(k))*1 + ...
                0.0;
             tracer_vector_total_blue_N   (k) = ...
                max(0.0,tracer_vector_no3_with_blue_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_blue_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_blue_N(k))*1 + ...
                0.0;
             tracer_vector_total_grey_N   (k) = ...
                max(0.0,tracer_vector_no3_with_grey_N(k))*1 + ...
                max(0.0,tracer_vector_dia_with_grey_N(k))*1 + ...
                max(0.0,tracer_vector_det_with_grey_N(k))*1 + ...
                0.0;
    end
    
    % vertical movement of tracers
    for m = 1:num_vmove_steps
       % first, move the age concentration of marked elements
       % second, move the tracers (including marked tracers) themselves
          tracer_vector_dia             = vmove_explicit(vertical_speed_of_dia            , ...
                                   tracer_vector_dia            , ...
                                   tracer_vector_dia            , tracer_vector_dia            , ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det             = vmove_explicit(vertical_speed_of_det            , ...
                                   tracer_vector_det            , ...
                                   tracer_vector_det            , tracer_vector_det            , ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_green_O = vmove_explicit(vertical_speed_of_dia_with_green_O, ...
                                   tracer_vector_dia_with_green_O, ...
                                   tracer_vector_dia_with_green_O, tracer_vector_dia_with_green_O, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_green_O = vmove_explicit(vertical_speed_of_det_with_green_O, ...
                                   tracer_vector_det_with_green_O, ...
                                   tracer_vector_det_with_green_O, tracer_vector_det_with_green_O, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_green_N = vmove_explicit(vertical_speed_of_dia_with_green_N, ...
                                   tracer_vector_dia_with_green_N, ...
                                   tracer_vector_dia_with_green_N, tracer_vector_dia_with_green_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_green_N = vmove_explicit(vertical_speed_of_det_with_green_N, ...
                                   tracer_vector_det_with_green_N, ...
                                   tracer_vector_det_with_green_N, tracer_vector_det_with_green_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_red_N  = vmove_explicit(vertical_speed_of_dia_with_red_N , ...
                                   tracer_vector_dia_with_red_N , ...
                                   tracer_vector_dia_with_red_N , tracer_vector_dia_with_red_N , ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_red_N  = vmove_explicit(vertical_speed_of_det_with_red_N , ...
                                   tracer_vector_det_with_red_N , ...
                                   tracer_vector_det_with_red_N , tracer_vector_det_with_red_N , ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_darkgreen_N = vmove_explicit(vertical_speed_of_dia_with_darkgreen_N, ...
                                   tracer_vector_dia_with_darkgreen_N, ...
                                   tracer_vector_dia_with_darkgreen_N, tracer_vector_dia_with_darkgreen_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_darkgreen_N = vmove_explicit(vertical_speed_of_det_with_darkgreen_N, ...
                                   tracer_vector_det_with_darkgreen_N, ...
                                   tracer_vector_det_with_darkgreen_N, tracer_vector_det_with_darkgreen_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_pink_N = vmove_explicit(vertical_speed_of_dia_with_pink_N, ...
                                   tracer_vector_dia_with_pink_N, ...
                                   tracer_vector_dia_with_pink_N, tracer_vector_dia_with_pink_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_pink_N = vmove_explicit(vertical_speed_of_det_with_pink_N, ...
                                   tracer_vector_det_with_pink_N, ...
                                   tracer_vector_det_with_pink_N, tracer_vector_det_with_pink_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_blue_N = vmove_explicit(vertical_speed_of_dia_with_blue_N, ...
                                   tracer_vector_dia_with_blue_N, ...
                                   tracer_vector_dia_with_blue_N, tracer_vector_dia_with_blue_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_blue_N = vmove_explicit(vertical_speed_of_det_with_blue_N, ...
                                   tracer_vector_det_with_blue_N, ...
                                   tracer_vector_det_with_blue_N, tracer_vector_det_with_blue_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_grey_N = vmove_explicit(vertical_speed_of_dia_with_grey_N, ...
                                   tracer_vector_dia_with_grey_N, ...
                                   tracer_vector_dia_with_grey_N, tracer_vector_dia_with_grey_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_grey_N = vmove_explicit(vertical_speed_of_det_with_grey_N, ...
                                   tracer_vector_det_with_grey_N, ...
                                   tracer_vector_det_with_grey_N, tracer_vector_det_with_grey_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
       % third, calculate new total marked element concentrations
       for k = 1:kmax
                tracer_vector_total_green_O  (k) = ...
                   max(0.0,tracer_vector_no3_with_green_O(k))*3 + ...
                   max(0.0,tracer_vector_po4_with_green_O(k))*4 + ...
                   max(0.0,tracer_vector_o2_with_green_O(k))*2 + ...
                   max(0.0,tracer_vector_dia_with_green_O(k))*110/16 + ...
                   max(0.0,tracer_vector_det_with_green_O(k))*110/16 + ...
                   0.0;
                tracer_vector_total_green_N  (k) = ...
                   max(0.0,tracer_vector_no3_with_green_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_green_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_green_N(k))*1 + ...
                   0.0;
                tracer_vector_total_red_N    (k) = ...
                   max(0.0,tracer_vector_no3_with_red_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_red_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_red_N(k))*1 + ...
                   0.0;
                tracer_vector_total_darkgreen_N(k) = ...
                   max(0.0,tracer_vector_no3_with_darkgreen_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_darkgreen_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_darkgreen_N(k))*1 + ...
                   0.0;
                tracer_vector_total_pink_N   (k) = ...
                   max(0.0,tracer_vector_no3_with_pink_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_pink_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_pink_N(k))*1 + ...
                   0.0;
                tracer_vector_total_blue_N   (k) = ...
                   max(0.0,tracer_vector_no3_with_blue_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_blue_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_blue_N(k))*1 + ...
                   0.0;
                tracer_vector_total_grey_N   (k) = ...
                   max(0.0,tracer_vector_no3_with_grey_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_grey_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_grey_N(k))*1 + ...
                   0.0;
       end
    end
    % vertical diffusion of tracers
    for m = 1:num_vmove_steps
       % first, diffuse the age concentration of marked elements
       % second, diffuse the tracers (including marked tracers) themselves
          tracer_vector_dia             = vdiff_explicit(vertical_diffusivity_of_dia            , ...
                                   tracer_vector_dia            , ...
                                   tracer_vector_dia            , tracer_vector_dia            , ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det             = vdiff_explicit(vertical_diffusivity_of_det            , ...
                                   tracer_vector_det            , ...
                                   tracer_vector_det            , tracer_vector_det            , ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_green_O = vdiff_explicit(vertical_diffusivity_of_dia_with_green_O, ...
                                   tracer_vector_dia_with_green_O, ...
                                   tracer_vector_dia_with_green_O, tracer_vector_dia_with_green_O, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_green_O = vdiff_explicit(vertical_diffusivity_of_det_with_green_O, ...
                                   tracer_vector_det_with_green_O, ...
                                   tracer_vector_det_with_green_O, tracer_vector_det_with_green_O, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_green_N = vdiff_explicit(vertical_diffusivity_of_dia_with_green_N, ...
                                   tracer_vector_dia_with_green_N, ...
                                   tracer_vector_dia_with_green_N, tracer_vector_dia_with_green_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_green_N = vdiff_explicit(vertical_diffusivity_of_det_with_green_N, ...
                                   tracer_vector_det_with_green_N, ...
                                   tracer_vector_det_with_green_N, tracer_vector_det_with_green_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_red_N  = vdiff_explicit(vertical_diffusivity_of_dia_with_red_N , ...
                                   tracer_vector_dia_with_red_N , ...
                                   tracer_vector_dia_with_red_N , tracer_vector_dia_with_red_N , ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_red_N  = vdiff_explicit(vertical_diffusivity_of_det_with_red_N , ...
                                   tracer_vector_det_with_red_N , ...
                                   tracer_vector_det_with_red_N , tracer_vector_det_with_red_N , ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_darkgreen_N = vdiff_explicit(vertical_diffusivity_of_dia_with_darkgreen_N, ...
                                   tracer_vector_dia_with_darkgreen_N, ...
                                   tracer_vector_dia_with_darkgreen_N, tracer_vector_dia_with_darkgreen_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_darkgreen_N = vdiff_explicit(vertical_diffusivity_of_det_with_darkgreen_N, ...
                                   tracer_vector_det_with_darkgreen_N, ...
                                   tracer_vector_det_with_darkgreen_N, tracer_vector_det_with_darkgreen_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_pink_N = vdiff_explicit(vertical_diffusivity_of_dia_with_pink_N, ...
                                   tracer_vector_dia_with_pink_N, ...
                                   tracer_vector_dia_with_pink_N, tracer_vector_dia_with_pink_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_pink_N = vdiff_explicit(vertical_diffusivity_of_det_with_pink_N, ...
                                   tracer_vector_det_with_pink_N, ...
                                   tracer_vector_det_with_pink_N, tracer_vector_det_with_pink_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_blue_N = vdiff_explicit(vertical_diffusivity_of_dia_with_blue_N, ...
                                   tracer_vector_dia_with_blue_N, ...
                                   tracer_vector_dia_with_blue_N, tracer_vector_dia_with_blue_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_blue_N = vdiff_explicit(vertical_diffusivity_of_det_with_blue_N, ...
                                   tracer_vector_det_with_blue_N, ...
                                   tracer_vector_det_with_blue_N, tracer_vector_det_with_blue_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_dia_with_grey_N = vdiff_explicit(vertical_diffusivity_of_dia_with_grey_N, ...
                                   tracer_vector_dia_with_grey_N, ...
                                   tracer_vector_dia_with_grey_N, tracer_vector_dia_with_grey_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
          tracer_vector_det_with_grey_N = vdiff_explicit(vertical_diffusivity_of_det_with_grey_N, ...
                                   tracer_vector_det_with_grey_N, ...
                                   tracer_vector_det_with_grey_N, tracer_vector_det_with_grey_N, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
       % third, calculate new total marked element concentrations
       for k = 1:kmax
                tracer_vector_total_green_O  (k) = ...
                   max(0.0,tracer_vector_no3_with_green_O(k))*3 + ...
                   max(0.0,tracer_vector_po4_with_green_O(k))*4 + ...
                   max(0.0,tracer_vector_o2_with_green_O(k))*2 + ...
                   max(0.0,tracer_vector_dia_with_green_O(k))*110/16 + ...
                   max(0.0,tracer_vector_det_with_green_O(k))*110/16 + ...
                   0.0;
                tracer_vector_total_green_N  (k) = ...
                   max(0.0,tracer_vector_no3_with_green_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_green_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_green_N(k))*1 + ...
                   0.0;
                tracer_vector_total_red_N    (k) = ...
                   max(0.0,tracer_vector_no3_with_red_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_red_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_red_N(k))*1 + ...
                   0.0;
                tracer_vector_total_darkgreen_N(k) = ...
                   max(0.0,tracer_vector_no3_with_darkgreen_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_darkgreen_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_darkgreen_N(k))*1 + ...
                   0.0;
                tracer_vector_total_pink_N   (k) = ...
                   max(0.0,tracer_vector_no3_with_pink_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_pink_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_pink_N(k))*1 + ...
                   0.0;
                tracer_vector_total_blue_N   (k) = ...
                   max(0.0,tracer_vector_no3_with_blue_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_blue_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_blue_N(k))*1 + ...
                   0.0;
                tracer_vector_total_grey_N   (k) = ...
                   max(0.0,tracer_vector_no3_with_grey_N(k))*1 + ...
                   max(0.0,tracer_vector_dia_with_grey_N(k))*1 + ...
                   max(0.0,tracer_vector_det_with_grey_N(k))*1 + ...
                   0.0;
       end
    end

    % calculate total colored element concentrations at bottom
       tracer_scalar_total_green_O_at_bottom = ...
          0.0;
       tracer_scalar_total_green_N_at_bottom = ...
          0.0;
       tracer_scalar_total_red_N_at_bottom = ...
          0.0;
       tracer_scalar_total_darkgreen_N_at_bottom = ...
          0.0;
       tracer_scalar_total_pink_N_at_bottom = ...
          0.0;
       tracer_scalar_total_blue_N_at_bottom = ...
          0.0;
       tracer_scalar_total_grey_N_at_bottom = ...
          0.0;
