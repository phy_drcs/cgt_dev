%function [ output_plot ] = myplot( input_name )
% plot 1d or 2d variables
input_matrix=eval(strcat('output_',myvar));

if size(input_matrix,1)==size(depths,1)
  if sum(mylevels<0)
    contourf(time_axis',depths',input_matrix);
  else
    contourf(time_axis',depths',input_matrix,mylevels);
  end
  shading flat;
  colorbar;
  set(gca,'YDir','reverse');
  set(gca,'xtick',time_axis);
  datetick('x','keeplimits');
  xlabel('time');
  ylabel('depth [m]');
  title(cgt_get_description(myvar));
end

if size(input_matrix,1)==1
  plot(time_axis',input_matrix);
  datetick('x');
  xlabel('time');
  title(cgt_get_description(myvar));
end