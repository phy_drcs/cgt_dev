program cgt_edit;

{$MODE Delphi}

uses
  Forms, Interfaces, Dialogs,
  Unit1 in 'Unit1.pas' {Form1},
  Unit2 in 'Unit2.pas' {Form2},
  Unit3 in 'Unit3.pas' {Form3},
  Unit4 in 'unit4.pas' {Form4},
  Unit5 in 'Unit5.pas' {Form5},
  Unit6 in 'unit6.pas' {Form6},
  Unit7 in 'Unit7.pas' {Form7},
  Unit8 in 'Unit8.pas' {Form8},
  unitCustomTags in 'unitcustomtags.pas' {frmCustomTags};

{$R *.res}

var
  requestedHelp: Boolean;
  i: Integer;

begin
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TfrmCustomTags, frmCustomTags);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TForm8, Form8);
  if Application.ParamCount > 0 then
  begin
    // if "-h" or "--help" is given, write usage text
    requestedHelp:=false;
    for i:=0 to Application.ParamCount-1 do
      if (lowercase(Application.Params[i+1])='-h') or (lowercase(Application.Params[i+1])='--help') then
        requestedHelp:=true;
    if requestedHelp then
    begin
      showMessage('Usage: cgt_edit    (running without parameters will start up graphical user interface)'+chr(13)+
                  'Usage: cgt_edit [/my/model/modelinfos.txt [/reference/model/modelinfos.txt]]');
      Halt;
    end
    else
    begin
      if application.ParamCount>2 then
      begin
        showMessage('cgt_edit can be run with 0, 1 or 2 parameters only. Use "cgt_edit --help" for help.');
        Halt;
      end
      else if application.ParamCount>=1 then
      begin
        Unit1.GivenModelinfos:=Application.Params[1];
        if application.ParamCount=2 then
          Unit1.GivenModelinfosRef:=Application.Params[2];
      end;
    end;
  end;
  Application.Run;
end.
